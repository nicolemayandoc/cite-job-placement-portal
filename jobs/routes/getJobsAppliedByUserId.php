<?php
require_once '../../config.php';
header("Content-Type: application/json");


if ($_SERVER["REQUEST_METHOD"] !== "GET") {
  echo json_encode([
    "message" => "Only get requests are supported."
  ]);
  exit();
}

$userId = "";

if (isset($_GET["id"]) && !empty($_GET["id"])) {
  $userId = $_GET["id"];
}

$query = "SELECT * FROM tbl_applicants
JOIN tbl_jobs
ON tbl_applicants.jobid = tbl_jobs.id
JOIN tbl_company
ON tbl_applicants.companyid = tbl_company.id
WHERE tbl_applicants.applicantsid = ?";
$stmt = $con->prepare($query);
$stmt->bind_param("i", $userId);
$stmt->execute();

$result = $stmt->get_result();
$jobs = array();

while ($row = $result->fetch_assoc())
  array_push($jobs, $row);

echo json_encode([
  "message" => "Retrieved jobs applied by user.",
  "jobs" => $jobs
]);
