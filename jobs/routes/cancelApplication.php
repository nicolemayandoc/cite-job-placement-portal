<?php
require_once '../../config.php';
require_once '../../functions.php';
require_once '../../session.php';
header("Content-Type: application/json");


if ($_SERVER["REQUEST_METHOD"] !== "GET") {
  echo json_encode([
    "message" => "Only get requests are supported."
  ]);
  exit();
}

$jobId = "";

if (isset($_GET["id"]) && !empty($_GET["id"])) {
  $jobId = $_GET["id"];
}

$userId = "";

if (isset($_GET["userId"]) && !empty($_GET["userId"])) {
  $userId = $_GET["userId"];
}

$query = "DELETE FROM tbl_applicants
WHERE tbl_applicants.jobid = ?
AND tbl_applicants.applicantsid = ?";

$stmt = $con->prepare($query);
$stmt->bind_param("ii", $jobId, $userId);
$stmt->execute();

header("Location: /$__name__/jobs/applied.php");
