<?php
session_start();
require_once '../config.php';
require_once '../functions.php';
require_once '../session.php';

$report = false;
$apply = false;

if (form("id")) {
    if (!$islogin) {
        navigate("../auth/?a=already");
    }

    $id = mysqli_value($con, "id");



    $result_query = mysqli_query($con, "
    SELECT
        tbl_jobs.id,
        tbl_company.id as 'c_id',
        tbl_company.c_name,
        tbl_company.c_address,
        tbl_company.c_cnum,
        tbl_company.c_description,
        tbl_company.c_banner,
        tbl_company.c_logo,
        tbl_accounts.firstname,
        tbl_accounts.lastname,
        tbl_jobs.j_name,
        tbl_jobs.j_age,
        tbl_jobs.j_min,
        tbl_jobs.j_max,
        tbl_jobs.j_currency_symbol,
        tbl_jobs.j_description,
        tbl_jobs.j_highlights,
        tbl_jobs.j_created_at
    FROM
        tbl_jobs
    INNER JOIN tbl_company ON tbl_company.userid = tbl_jobs.userid
    INNER JOIN tbl_accounts ON tbl_accounts.id = tbl_jobs.userid

    WHERE
    tbl_jobs.id = $id
    ");

    if (hasResult($result_query)) {
        $data = mysqli_fetch_assoc($result_query);
    } else {
        navigate("./");
    }

    if (form("apply")) {
        //check if the user is fully verified.
        $apply = true;
        if ($u_verification_state == 2) {
            $fully_verified = true;

            $job_id = $data["id"];
            $company_id = $data["c_id"];

            $check_application = mysqli_query($con, "SELECT * FROM `tbl_applicants` WHERE `companyid` = $company_id AND `applicantsid` = $u_id AND `jobid` = $job_id ");
            if (hasResult($check_application)) {
                $already_submited = true;
            } else {
                $submit_application = mysqli_query($con, "
                INSERT INTO `tbl_applicants`(
                    `companyid`,
                    `applicantsid`,
                    `jobid`
                )
                VALUES(
                    $company_id,
                    $u_id,
                    $job_id
                )
                ");
                $already_submited = false;
            }
        } else {
            $fully_verified = false;
        }
    } else {
        $apply = false;
    }
} elseif (form("report") && is_numeric(value("report"))) {
    $report = true;
    $reported_company_id = value("report");
} else {
    navigate("./");
}

$returnUrl = "";

if (isset($_GET["returnUrl"]) && !empty($_GET["returnUrl"])) {
    $returnUrl = $_GET["returnUrl"];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../assets/logo.png">
    <title>CITE Job Portal - JOBS</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous" defer></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11" defer></script>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="./view.css">
    <link rel="stylesheet" href="../verify.css">
    <link rel="stylesheet" href="../header.css">

    <!-- javascript -->
    <script src="./js/report_company.js" defer></script>
    <!-- Import Tailwind - a framework for writing CSS faster -->
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body>
    <div class="main">
        <?php include '../header.php' ?>
        <div class="body">

            <?php if ($apply) { ?>
                <?php if ($fully_verified) { ?>
                    <?php if ($already_submited) { ?>
                        <div class="box box_success">

                            <img src="../assets/success2.png" width="300px">
                            <p>
                                Your application for this position has been submitted and is now under review. Expect to hear back from the Company soon.
                            </p>
                            <a href="./applied.php">BACK</a>
                        </div>
                    <?php } else { ?>
                        <div class="box box_success">
                            <img src="../assets/success2.png" width="300px">
                            <p class="flex items-center flex-col">
                                Your application for the position has been successfully submitted. The company will be in touch with you soon with updates.
                                <a href="./applied.php">BACK</a>
                            </p>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <div class="box not_verified">
                        <img src="../assets/fail.png" width="300px">
                        <p>
                            Sorry you cannot apply for this job, Your account is not fully verified.
                        </p>
                        <a href="../profile/?page=general_information&sub=verified">GET VERIFED</a>
                    </div>
                <?php } ?>
            <?php } elseif ($report) { ?>
                <h3 class="text-green-700 mb-2 max-w-7xl mx-auto w-full">
                    <a href="?page=hire&sub=company" class="text-green-700 hover:text-green-600 active:text-red-700">Jobs</a>
                    >
                    <a href="?page=hire&sub=company" class="text-green-700 hover:text-green-600 active:text-red-700">View Jobs</a>
                    >
                    <a class="text-black">Report Company</a>
                </h3>
                <div>
                    <div class="flex flex-col sm:w-3/4 lg:w-2/3 mx-auto py-10">
                        <div class="bg-white p-6 rounded-lg shadow-lg">
                            <div class="text-lg font-bold mb-4 text-red-500">REPORT COMPANY</div>

                            <form class="form frm_report_company" method="post">
                                <div class="mb-4">
                                    <label for="tb_company_id" class="block text-gray-700 font-medium mb-2">
                                        Company ID
                                    </label>
                                    <input type="text" name="tb_company_id" id="tb_company_id" class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-indigo-500" value="<?= $reported_company_id ?>" readonly>
                                </div>
                                <div class="mb-4">
                                    <label for="tb_description" class="block text-gray-700 font-medium mb-2">
                                        Description
                                    </label>
                                    <textarea name="tb_description" id="tb_description" class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-indigo-500" rows="5"></textarea>
                                </div>
                                <div class="text-left">
                                    <button type="submit" class="bg-indigo-500 text-white py-2 px-2 rounded hover:bg-indigo-600">
                                        SUBMIT
                                    </button>

                                </div>
                            </form>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="drop-shadow-xl">
                        <?php if (empty($returnUrl)): ?>
                            <h3 class="text-green-700 mb-2 max-w-7xl mx-auto w-full">
                                <a href="?page=hire&sub=company" class="text-green-700 hover:text-green-600 active:text-red-700">Jobs</a>
                                >
                                <a class="text-black">View Jobs</a>
                            </h3>
                        <?php else: ?>
                            <h3 class="text-green-700 mb-2 max-w-7xl mx-auto w-full">
                                << <a href="<?= $returnUrl ?>" class="text-green-700 hover:text-green-600 active:text-red-700">Back to Previous</a>
                            </h3>
                        <?php endif ?>

                        <img src="../assets/images/<?= $data["c_banner"] ?>" class="max-w-7xl mx-auto w-full h-48 object-cover mb-3 rounded-b-md" />
                    </div>
                    <div class="max-w-7xl mx-auto w-full bg-white rounded-md px-6 py-4 grid sm:grid-cols-[1fr_auto] gap-2 mb-3">
                        <div class="grid sm:grid-cols-[8rem_1fr] ">
                            <img src="../assets/images/<?= $data["c_logo"] ?>" alt="Company logo" class="h-24 rounded-md" />
                            <div>
                                <p class="text-blue-600 font-semibold text-3xl uppercase"><?= $data["j_name"] ?></p>
                                <p class="text-blue-600 uppercase font-medium"><?= $data["c_name"] ?></p>
                                <div class="md:flex gap-3">
                                    <p class="uppercase">
                                        <i class="fa fa-map-marker text-red-500"></i>
                                        <?= $data["c_address"] ?>
                                    </p>
                                    <p class="uppercase">
                                        <i class="fa fa-money text-green-500"></i>
                                        <?= $data["j_currency_symbol"] . " " . number_format($data['j_min']) . " - " . $data["j_currency_symbol"] . " " . number_format($data['j_max']) ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="flex items-center">
                            <?php if ($u_type === "3"): ?>
                                <?php if (hasSubmittedJobApplication($con, $data["id"], $u_id)): ?>
                                    <a href="/<?= $__name__ ?>/jobs/routes/cancelApplication.php?id=<?= $data["id"] ?>&userId=<?= $u_id ?>"
   class="bg-gray-300 text-gray-700 px-4 py-2 rounded-md"
   onclick="event.preventDefault();
            Swal.fire({
              title: 'Are you sure?',
              text: 'Do you want to withdraw your application?',
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, Withdraw it!'
            }).then((result) => {
              if (result.isConfirmed) {
                window.location.href = '/<?= $__name__ ?>/jobs/routes/cancelApplication.php?id=<?= $data["id"] ?>&userId=<?= $u_id ?>';
              }
            });"
>
    <i class="fa fa-file-text-o"></i>
    WITHDRAW APPLY
</a>

                                <?php else: ?>
                                
<a href="?id=<?= $data["id"] ?>&apply=<?= $data["id"] ?>"
   class="bg-blue-500 text-white px-4 py-2 rounded-md"
   onclick="event.preventDefault(); 
   Swal.fire({
              title: 'NOTE:',
              icon: 'info',
              html:
                  'Only hired candidates that only recieve SMS notifications' +
                  '<br>' +
                  'If you do not recieve any SMS notifacttion within 7 days, Please assume that you have been declined.',
              showCloseButton: true,
           
              focusConfirm: false,
              confirmButtonText:
                  '<i class=&quot;fa fa-thumbs-up&quot;></i> Okay!',
              confirmButtonAriaLabel: 'Thumbs up, great!',
           
             }).then((result) => {
        if (result.isConfirmed) {
            window.location.href = '<?= "?id=" . $data["id"] . "&apply=" . $data["id"] ?>';
        }
    });">
    <i class="fa fa-file-text-o"></i>
    APPLY NOW
</a>
                                <?php endif ?>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="max-w-7xl mx-auto w-full grid md:grid-cols-[2fr_1fr] gap-3 items-start">
                        <div class="rounded-md px-6 py-4 bg-white">
                            <section>
                                <div class="sm:flex justify-between items-baseline">
                                    <h3 class="px-3 mb-1 font-medium">Job Highlights</h3>
                                    <p class="px-3 sm:px-0 text-gray-400 text-sm">
                                        Posted on
                                        <?= date("j F Y", strtotime($data["j_created_at"])) ?>
                                    </p>
                                </div>
                                <hr>
                                <p class="px-3 mb-3 pt-1">
                                    <?php if (isset($data["j_highlights"]) && !empty($data["j_highlights"])) : ?>
                                        <?= $data["j_highlights"] ?>
                                    <?php else : ?>
                                        <span class="text-gray-500">Not available.</span>
                                    <?php endif ?>
                                </p>
                            </section>
                            <section>
                                <h3 class="px-3 mb-1 font-medium">Job Description</h3>
                                <hr>
                                <p class="px-3 mb-3 pt-1">
                                    <?= $data["j_description"] ?>
                                </p>
                            </section>
                        </div>
                        <div class="rounded-md px-6 py-4 bg-white">
                            <h3 class="px-3 mb-1 font-medium">About the company</h3>
                            <hr>
                            <p class="px-3 mb-3 pt-1">
                                <?php if (isset($data["c_description"]) && !empty($data["c_description"])) : ?>
                                    <?= $data["c_description"] ?>
                                <?php else : ?>
                                    <span class="text-gray-500">Not available.</span>
                                <?php endif ?>
                            </p>
                            <div class=" px-1 py-10 bg-white">
                                <h3 class="px-3 mb-1 font-medium">About the owner</h3>
                                <hr>
                                <p class="px-3 mb-3 pt-1 mt-3">
                                    <label for="tb_code">Owner name: </label>

                                    <?php if (isset($data["c_name"]) && !empty($data["c_name"])) : ?>
                                        <?= $data["c_name"] ?>
                                    <?php else : ?>
                                        <span class="text-gray-500">Not available.</span>
                                    <?php endif ?>

                                </p>
                                <p class="px-3 mb-3">
                                    <label for="tb_code">Business number: </label>

                                    <?php if (isset($data["c_cnum"]) && !empty($data["c_cnum"])) : ?>
                                        <?= $data["c_cnum"] ?>
                                    <?php else : ?>
                                        <span class="text-gray-500">Not available.</span>
                                    <?php endif ?>
                                </p>
                                <div class="flex justify-end">
                                    <a href="?report=<?= $data["c_id"] ?>" class="bg-amber-400 px-3 py-2 rounded-md">
                                        <i class="fa fa-exclamation-triangle"></i>
                                        REPORT
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</body>

</html>