<?php
session_start();
require_once('../config.php');
require_once '../functions.php';
require_once('../session.php');

$islogin = false;
if(isset($_SESSION["islogin"]))
    $islogin = true;


if (!$islogin) {
  header("Location: /$__name__");
  exit();
}

if (intval($_SESSION["data"]["type"]) !== 3) {
  header("Location: /$__name__");
  exit();
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>CITE Job Portal</title>
    <link rel="icon" href="../assets/logo.png" >
    <!-- Google font: Poppins -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap"
      rel="stylesheet"
    />
    <link rel="stylesheet" href="../dashboard/admin/style.css">
    <link rel="stylesheet" href="../header.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11" defer></script>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- CSS utility and design framework -->
    <script src="https://cdn.tailwindcss.com"></script>
    <!-- Import Vue - a declarative UI framework -->
  <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
  </head>
  <body>
    <div id="app" class="font-[Poppins] min-h-[100svh] grid grid-rows-[auto_1fr]">
      <?php include '../header.php' ?>
      <main class="bg-gray-50 w-full">
        <div class="px-8 pt-8 max-w-6xl mx-auto">
          <div class="flex justify-between mb-6 items-center">
            <h2 class="font-semibold text-2xl">Jobs You've Applied For</h2>
            <input v-model="searchTerm" type="text" placeholder=" Search ..." class="border border-gray-300 px-3 py-2 rounded-md w-full sm:w-[12rem] outline-none text-gray-800 focus:ring focus:ring-blue-300/40 focus:border-blue-500" />
          </div>
          <div
            v-if="visibleJobs.length === 0"
            class="text-gray-500 text-center"
          >No Jobs Found.</div>
          <section
            v-else
            class="grid sm:grid-cols-2 gap-8"
          >            
            <a
              :href="`/${this.siteName}/jobs/view.php?id=${job.jobid}&returnUrl=${this.returnUrl}`"
              v-for="job in visibleJobs"
              :key="job.id"
              class="bg-white hover:bg-gray-100 duration-200 transition rounded-md shadow-md px-6 py-4"
            >
              <div class="sm:grid grid-cols-[auto_1fr] gap-3 items-center mb-3">
                <img
                  alt="Company logo"
                  :src="`../assets/images/${job.c_logo}`"
                  class="w-24 aspect-square object-contain rounded-full border border-gray-300"
                />
                <div class="">
                  <h3 class="text-lg sm:text-2xl font-medium text-blue-600">{{ job.j_name }}</h3>
                  <p>{{ job.c_name }}</p>
                </div>
              </div>
              <p class="uppercase">
                  <i class="fa fa-map-marker text-red-500"></i>
                  {{ job.c_address }}
              </p>
              <p class="uppercase mb-3">
                  <i class="fa fa-money text-green-500"></i>
                  {{ job.j_currency_symbol }} {{ job.j_min.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") }}
                  -
                  {{ job.j_currency_symbol }} {{ job.j_max.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") }}
              </p>
              <p class="text-gray-500">Applied: {{ job.created_at }}</p>
            </a>
          </section>
          <div class="flex justify-between mt-3">
            <div>
              <button
                type="button"
                v-if="hasPrevPage"
                @click="currentPage--"
                class="hover:bg-gray-300 duration-200 transition px-3 py-1 rounded-md"
              >
                Prev
              </button>
            </div>
            <div>
              <button
                type="button"
                v-if="hasNextJobsPage"
                @click="currentPage++"
                class="hover:bg-gray-300 duration-200 transition px-3 py-1 rounded-md"
              >
                Next
              </button>
            </div>
          </div>
        </div>
      </main>
    </div>
  </body>
  <script>
    const { createApp } = Vue

    createApp({
      data() {
        return {
          siteName: "<?= $__name__ ?>",
          userId: "<?= $u_id ?>",
          message: "Well, hello there!",
          isLoading: true,
          isError: false,
          jobs: [],
          pageSize: 4,
          currentPage: 0,
          searchTerm: "",
          returnUrl: encodeURI(location.href)
        }
      },
      async mounted() {
        try {
          const response = await fetch(`/${this.siteName}/jobs/routes/getJobsAppliedByUserId.php?id=${this.userId}`)
          const { jobs } = await response.json();
          this.jobs = jobs
        } catch {
          this.isError = true
        } finally {
          this.isLoading = false
        }
      },
      watch: {
        searchTerm() {
          this.currentPage = 0
        }
      },
      computed: {
        filteredJobs() {
          return this.jobs
            .filter((job) =>
              job.j_name
              .toLowerCase()
              .includes(this.searchTerm.toLowerCase())
            )
        },
        visibleJobs() {
          return this.filteredJobs.slice(this.currentPage * this.pageSize, this.currentPage * this.pageSize + this.pageSize)
        },
        hasNextJobsPage() {
          if (this.filteredJobs.length === 0) return false

          if (this.currentPage !== Math.ceil(this.filteredJobs.length / this.pageSize) - 1)
            return true

          return false
        },
        hasPrevPage() {
          if (this.currentPage > 0)
            return true

          return false
        },
      }
    }).mount("#app")
  </script>
</html>
