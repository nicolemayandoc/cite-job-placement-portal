<?php
session_start();
require_once '../config.php';
require_once '../functions.php';
require_once '../session.php';

function getPublicJobs($conn, $offset, $limit) {
  $jobs = array();
  $query = "SELECT
    tbl_jobs.id,
    tbl_company.c_name,
    tbl_company.c_address,
    tbl_company.c_cnum,
    tbl_accounts.firstname,
    tbl_accounts.lastname,
    tbl_jobs.j_name,
    tbl_jobs.j_age,
    tbl_jobs.j_min,
    tbl_jobs.j_max,
    tbl_jobs.j_currency_symbol,
    tbl_jobs.j_description,
    tbl_jobs.j_created_at
  FROM tbl_jobs
  INNER JOIN tbl_company
  ON tbl_company.userid = tbl_jobs.userid
  INNER JOIN tbl_accounts
  ON tbl_accounts.id = tbl_jobs.userid
  LIMIT ?
  OFFSET ?";

  $stmt = $conn->prepare($query);
  $stmt->bind_param("ii", $limit, $offset);
  $stmt->execute();
  $result = $stmt->get_result();

  while ($row = $result->fetch_assoc())
    $jobs[$row["id"]] = $row;

  return $jobs;
}

function getTotalPublicJobs($conn) {
  $query = "SELECT
    tbl_jobs.id,
    tbl_company.c_name,
    tbl_company.c_address,
    tbl_company.c_cnum,
    tbl_accounts.firstname,
    tbl_accounts.lastname,
    tbl_jobs.j_name,
    tbl_jobs.j_age,
    tbl_jobs.j_min,
    tbl_jobs.j_max,
    tbl_jobs.j_currency_symbol,
    tbl_jobs.j_description,
    tbl_jobs.j_created_at
  FROM tbl_jobs
  INNER JOIN tbl_company
  ON tbl_company.userid = tbl_jobs.userid
  INNER JOIN tbl_accounts
  ON tbl_accounts.id = tbl_jobs.userid";

  $stmt = $conn->prepare($query);
  $stmt->execute();
  $result = $stmt->get_result();

  return $result->num_rows;
}

function getFeaturedJobs($conn, $offset, $limit, $birthDate) {
  $jobs = array();
  $userCurrentAge = birthDateToAge($birthDate);

  $query = "SELECT
    tbl_jobs.id,
    tbl_company.c_name,
    tbl_company.c_address,
    tbl_company.c_cnum,
    tbl_accounts.firstname,
    tbl_accounts.lastname,
    tbl_jobs.j_name,
    tbl_jobs.j_age,
    tbl_jobs.j_min,
    tbl_jobs.j_max,
    tbl_jobs.j_currency_symbol,
    tbl_jobs.j_description,
    tbl_jobs.j_created_at
  FROM tbl_jobs
  INNER JOIN tbl_company
  ON tbl_company.userid = tbl_jobs.userid
  INNER JOIN tbl_accounts
  ON tbl_accounts.id = tbl_jobs.userid
  WHERE tbl_jobs.j_age <= ?
  AND tbl_jobs.j_age_max >= ?
  LIMIT ?
  OFFSET ?
  ";

  $stmt = $conn->prepare($query);
  $stmt->bind_param("iiii", $userCurrentAge, $userCurrentAge, $limit, $offset);
  $stmt->execute();
  $result = $stmt->get_result();

  while ($row = $result->fetch_assoc())
    $jobs[$row["id"]] = $row;

  return $jobs;
}

function getTotalFeaturedJobs($conn, $birthDate) {
  $userCurrentAge = birthDateToAge($birthDate);

  $query = "SELECT
    tbl_jobs.id,
    tbl_company.c_name,
    tbl_company.c_address,
    tbl_company.c_cnum,
    tbl_accounts.firstname,
    tbl_accounts.lastname,
    tbl_jobs.j_name,
    tbl_jobs.j_age,
    tbl_jobs.j_min,
    tbl_jobs.j_max,
    tbl_jobs.j_currency_symbol,
    tbl_jobs.j_description,
    tbl_jobs.j_created_at
  FROM tbl_jobs
  INNER JOIN tbl_company
  ON tbl_company.userid = tbl_jobs.userid
  INNER JOIN tbl_accounts
  ON tbl_accounts.id = tbl_jobs.userid
  WHERE tbl_jobs.j_age <= ?
  AND tbl_jobs.j_age_max >= ?";

  $stmt = $conn->prepare($query);
  $stmt->bind_param("ii", $userCurrentAge, $userCurrentAge);
  $stmt->execute();
  $result = $stmt->get_result();

  return $result->num_rows;
}

function getJobsFromSearch($conn, $offset, $limit, $searchTerm) {
  $jobs = array();
  $formattedSearchTerm = "%$searchTerm%";

  $query = "SELECT
    tbl_jobs.id,
    tbl_company.c_name,
    tbl_company.c_address,
    tbl_company.c_cnum,
    tbl_accounts.firstname,
    tbl_accounts.lastname,
    tbl_jobs.j_name,
    tbl_jobs.j_age,
    tbl_jobs.j_min,
    tbl_jobs.j_max,
    tbl_jobs.j_currency_symbol,
    tbl_jobs.j_description,
    tbl_jobs.j_created_at
  FROM tbl_jobs
  INNER JOIN tbl_company
  ON tbl_company.userid = tbl_jobs.userid
  INNER JOIN tbl_accounts
  ON tbl_accounts.id = tbl_jobs.userid
  WHERE tbl_company.c_name LIKE ?
  OR tbl_jobs.j_name LIKE ?
  OR tbl_company.c_address LIKE ?
  LIMIT ?
  OFFSET ?
  ";

  $stmt = $conn->prepare($query);
  $stmt->bind_param(
    "sssii",
    $formattedSearchTerm,
    $formattedSearchTerm,
    $formattedSearchTerm,
    $limit,
    $offset
  );
  $stmt->execute();
  $result = $stmt->get_result();

  while ($row = $result->fetch_assoc())
    $jobs[$row["id"]] = $row;

  return $jobs;
}

function getTotalJobsFromSearch($conn, $searchTerm) {
  $formattedSearchTerm = "%$searchTerm%";

  $query = "SELECT
    tbl_jobs.id,
    tbl_company.c_name,
    tbl_company.c_address,
    tbl_company.c_cnum,
    tbl_accounts.firstname,
    tbl_accounts.lastname,
    tbl_jobs.j_name,
    tbl_jobs.j_age,
    tbl_jobs.j_min,
    tbl_jobs.j_max,
    tbl_jobs.j_currency_symbol,
    tbl_jobs.j_description,
    tbl_jobs.j_created_at
  FROM tbl_jobs
  INNER JOIN tbl_company
  ON tbl_company.userid = tbl_jobs.userid
  INNER JOIN tbl_accounts
  ON tbl_accounts.id = tbl_jobs.userid
  WHERE tbl_company.c_name LIKE ?
  OR tbl_jobs.j_name LIKE ?
  OR tbl_company.c_address LIKE ?";

  $stmt = $conn->prepare($query);
  $stmt->bind_param(
    "sss",
    $formattedSearchTerm,
    $formattedSearchTerm,
    $formattedSearchTerm
  );
  $stmt->execute();
  $result = $stmt->get_result();

  return $result->num_rows;
}

$pageSize = 5;
$limit = $pageSize + 1;
$pageNumber = 0;

if (
  isset($_GET["page"]) &&
  is_numeric($_GET["page"]) &&
  intval($_GET["page"]) > 0
  )
  $pageNumber = $_GET["page"] - 1;

$offset = $pageNumber * $pageSize;

$onSearch = false;
$jobs = array();
$totalJobs = 0;

if (isset($_GET["search"])) {
  $onSearch = true;
  $searchTerm = mysqli_value($con,"search");
  $jobs = getJobsFromSearch($con, $offset, $limit, $searchTerm);
  $totalJobs = getTotalJobsFromSearch($con, $searchTerm);
} else {
  $isAdminUser =
    isset($_SESSION["data"]) &&
    intval($_SESSION["data"]["type"]) === 3;

  if($islogin && $isAdminUser)
  {
    $jobs = getFeaturedJobs($con, $offset, $limit, $u_bday);
    $totalJobs = getTotalFeaturedJobs($con, $u_bday);
  }
  else 
  {
    $jobs = getPublicJobs($con, $offset, $limit);
    $totalJobs = getTotalPublicJobs($con);
  }
}

$hasNextPage = false;
$nextPage = -1;
if (count($jobs) > $pageSize) {
  $hasNextPage = true;
  $nextPage = $pageNumber + 2;
  // Remove the last item, since that is
  // just our next page marker.
  array_pop($jobs);
}

$hasPrevPage = false;
$prevPage = -1;
if ($pageNumber > 0) {
  $hasPrevPage = true;
  $prevPage = $pageNumber;
}

$pageCount = ceil($totalJobs / $pageSize);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="../assets/logo.png" >
  <title>CITE Job Portal - JOBS</title>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous" defer></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11" defer></script>
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="../verify.css">
  <link rel="stylesheet" href="../header.css">
  <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>
  
  <div class="main">
    <?php include '../header.php' ?>
    <div class="body_search">
      <div class="container_search">
        <h1 class="text-2xl py-12 font-bold text-center sm:text-3xl sm:py-16 max-w-2xl px-6">
  Discover Your Dream Job with Ease - Start Your Search Today!
</h1>
<form action="" class="flex items-center justify-center mt-8 sm:mt-10" method="get">
  <div class="relative w-full sm:w-3/4">
    <i class="fa fa-search absolute inset-y-0 left-0 ml-3 py-2 text-gray-600"></i>
    <input type="text" name="search" id="search" value="<?= ($onSearch) ? value("search") : "" ?>" class="w-full pl-10 pr-2 py-1 border-2 border-gray-300 rounded-lg text-gray-600 placeholder-gray-500" placeholder="Job title, Company Name and City" required>
  </div>
  <button type="submit" class="ml-4 bg-gray-800 text-white py-3 px-6 rounded-lg hover:bg-gray-900 transition-colors duration-300">
    SEARCH
  </button>
</form>

      </div>
      <div class="content">
        <div class="container recommended pt-8 mx-auto">
          <div class="container_title text-center">
            <?php if ($onSearch): ?>
              Result - <?= $totalJobs ?>
            <?php else: ?>
              <?= ($islogin) ? "Find Your Perfect Career Match with Our Recommended Jobs." : "Featured Jobs" ?>
            <?php endif ?>
          </div>
          <div class="container_body max-w-5xl mx-auto px-3 w-full">
            <?php if(count($jobs) > 0): ?>
              <?php foreach ($jobs as $job): ?>
                <div class="bg-white px-8 py-6 md:px-16 md:py-10 rounded-md border border-gray-300 transition hover:shadow-xl duration-300">
                  <div class="grid sm:grid-cols-[1fr_18rem_10rem] gap-3">
                    <div>
                      <div class="mb-4">
                        <p class="text-xl font-semibold mb-3"><?= $job["j_name"]?></p>
                        <p class="text-[0.9rem] text-gray-500 underline">
                          <?= $job["c_name"]?>                        
                        </p>
                      </div>
                      <p class="text-gray-500 text-[0.9rem]">
                        Posted: <?= date("m-d-Y",strtotime($job["j_created_at"]))?>
                      </p>
                    </div>
                    <div>
                      <div class="mb-4">
                        <p class="text-lg font-medium">
                          <?= $job["j_currency_symbol"]." ".number_format($job['j_min'])." - ".$job["j_currency_symbol"]." ".number_format($job['j_max']) ?>  
                        </p>
                      </div>
                      <p class="text-[0.9rem] text-gray-500 underline">
                        <?= $job["c_address"] ?>
                      </p>
                      <p>
                        
                      </p>
                    </div>
                    <div>
                      <a
                        href="view.php?id=<?= $job["id"]?>"
                        class="inline-block text-center py-2 rounded-md w-full bg-green-500 hover:bg-green-600 transition duration-200"
                      >
                        View
                      </a>
                    </div>
                  </div>
                </div>
              <?php endforeach ?>
            <?php else: ?>
              <div class="not_found">
                <img src="../assets/empty.png" >
                <p>No Result</p>
              </div>
            <?php endif ?>
          </div>
          <div class="mt-6 mb-12 flex justify-between">
            <div>
              <?php if ($hasPrevPage): ?>
                <?php if ($onSearch): ?>
                  <a 
                    href="
                      <?= "{$_SERVER['PHP_SELF']}?search={$_GET["search"]}&page={$prevPage}" ?>
                    "
                    class="block font-medium text-white text-center px-4 py-2 bg-[#4c8d9b] hover:bg-[#539aa9] transition duration-200"
                  >
                    Prev
                  </a>
                <?php else: ?>
                  <a 
                    href="
                      <?= "{$_SERVER['PHP_SELF']}?page={$prevPage}" ?>
                    "
                    class="block font-medium text-white text-center px-4 py-2 bg-[#4c8d9b] hover:bg-[#539aa9] transition duration-200"
                  >
                    Prev
                  </a>
                <?php endif ?>
              <?php endif ?>
            </div>
            <div class="flex gap-2">
              <?php
                for ($i = 0; $i < $pageCount; $i++):
                  $currentPage = $i + 1;
              ?>
                <?php if ($onSearch): ?>
                  <a
                    <?php if ($i === $pageNumber): ?>
                      class="font-medium bg-[#4c8d9b] text-white hover:bg-[#539aa9] flex justify-center items-center w-6 transition duration-200"
                    <?php else: ?>
                      class="border border-gray-300 font-medium text-[#4c8d9b] flex justify-center items-center w-6 hover:bg-zinc-100 transition duration-200"
                    <?php endif ?>
                    href="
                      <?php echo "{$_SERVER['PHP_SELF']}?search={$_GET["search"]}&page={$currentPage}" ?>
                    "
                  >
                    <?= $currentPage ?>
                  </a>
                <?php else: ?>
                  <a
                    <?php if ($i === $pageNumber): ?>
                      class="font-medium bg-[#4c8d9b] text-white hover:bg-[#539aa9] flex justify-center items-center w-6 transition duration-200"
                    <?php else: ?>
                      class="border border-gray-300 font-medium text-[#4c8d9b] flex justify-center items-center w-6 hover:bg-zinc-100 transition duration-200"
                    <?php endif ?>
                    href="
                      <?php echo "{$_SERVER['PHP_SELF']}?page={$currentPage}" ?>
                    "
                  >
                    <?= $currentPage ?>
                  </a>
                <?php endif ?>
              <?php endfor ?>
            </div>
            <div>
              <?php if ($hasNextPage): ?>
                <?php if ($onSearch): ?>
                  <a
                    href="
                      <?= "{$_SERVER['PHP_SELF']}?search={$_GET["search"]}&page={$nextPage}" ?>
                    "
                    class="block font-medium text-white text-center px-4 py-2 bg-[#4c8d9b] hover:bg-[#539aa9] transition duration-200"
                  >
                    Next
                  </a>
                <?php else: ?>
                  <a
                    href="
                      <?= "{$_SERVER['PHP_SELF']}?page={$nextPage}" ?>
                    "
                    class="block font-medium text-white text-center px-4 py-2 bg-[#4c8d9b] hover:bg-[#539aa9] transition duration-200"
                  >
                    Next
                  </a>
                <?php endif ?>
              <?php endif ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
