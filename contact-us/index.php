<?php
session_start();

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../vendor/autoload.php';
require_once '../config.php';
require_once '../functions.php';
require_once '../session.php';

$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

function getJobCount($conn) {
    $query = "SELECT COUNT(id) AS job_count FROM `tbl_jobs`";
    $result = $conn->query($query);

    $row = $result->fetch_assoc();
    return $row["job_count"];
}

function getApplicantCount($conn) {
    $query = "SELECT COUNT(id) AS applicant_count FROM `tbl_applicants`";
    $result = $conn->query($query);

    $row = $result->fetch_assoc();
    return $row["applicant_count"];
}

function getUserCount($conn) {
    $query = "SELECT COUNT(id) AS user_count FROM `tbl_accounts`";
    $result = $conn->query($query);

    $row = $result->fetch_assoc();
    return $row["user_count"];
}

function sendEmail($fromName, $fromEmail, $messageSubject, $messageBody) {
  $mail = new PHPMailer(true);
  global $didSend;
  try {
      //Server settings
      $mail->isSMTP();                                  // Send using SMTP
      $mail->Host       = 'smtp.gmail.com';             // Set the SMTP server to send through
      $mail->SMTPAuth   = true;                         // Enable SMTP authentication
      $mail->Username   = $_ENV['PHP_MAILER_SITE_MAILER'];    // SMTP username
      $mail->Password   = $_ENV['PHP_MAILER_SITE_MAILER_PASS'];     // SMTP password
      $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;  // Enable implicit TLS encryption
      $mail->Port       = 465;                          // TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

      //Recipients
      $mail->setFrom($_ENV["PHP_MAILER_SITE_MAILER"], "{$fromName} (Forwarded by: UPang Job Portal)");
      $mail->addAddress($_ENV["PHP_MAILER_RECEIVER"]);  // Add a recipient

      //Content
      $mail->Subject = "UPang Job Portal: You have a message from {$fromName} ({$fromEmail})";
      $mail->Body    = "
Subject: {$messageSubject}
Message:
{$messageBody}
      ";

      $mail->send();
      $didSend = true;
  } catch (Exception $e) {
      echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
  } 
}

function tryToSendEmail() {
  global $isFormInvalid;
  $fromName = "Anonymous User";
  $fromEmail = "";
  $messageSubject = "";
  $messageBody = "";

  if (!empty($_POST["name"]))
    $fromName = $_POST["name"];

  if (empty($_POST["email"])) {
    $isFormInvalid = true;
    return;
  }
  
  $fromEmail = $_POST["email"];

  if (empty($_POST["messageSubject"])) {
    $isFormInvalid = true;
    return;
  }
  
  $messageSubject = $_POST["messageSubject"];

  if (empty($_POST["messageBody"])) {
    $isFormInvalid = true;
    return;
  }

  $messageBody = $_POST["messageBody"];
  sendEmail($fromName, $fromEmail, $messageSubject, $messageBody);
}

$isFormInvalid = false;
$didSend = false;
if ($_SERVER["REQUEST_METHOD"] === "POST") {
  tryToSendEmail();
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../assets/logo.png">
    <title>CITE Job Portal</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;500;600;700&display=swap" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous" defer></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11" defer></script>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="./style.css">
    <link rel="stylesheet" href="../verify.css">
    <link rel="stylesheet" href="../header.css">
    <script src="https://cdn.tailwindcss.com"></script>
  </head>
  <body>
    <div class="main">
      <div class="font-[Poppins]">
        <?php include '../header.php' ?>
      </div>

      <main class="font-[Nunito] px-4 pt-2 pb-24 text-slate-800 max-w-6xl mx-auto">
        <p class="text-center text-[2.5rem] font-bold py-12 md:py-20">Get in touch with us</p>
        <div class="grid md:grid-cols-2 gap-8 mb-16">
          <div class="block md:hidden">
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23653.585371774036!2d120.33135264414898!3d16.04665538470221!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x339167fe6bba4d67%3A0xf54b516c2c5d10b6!2sPHINMA-University%20of%20Pangasinan!5e0!3m2!1sen!2sph!4v1676424382967!5m2!1sen!2sph"
              class="w-full aspect-[4/3]"
              style="border:0;"
              allowfullscreen=""
              loading="lazy"
              referrerpolicy="no-referrer-when-downgrade">
            </iframe>
          </div>
          <!-- Contact Form -->
          <div>
            <form class="grid gap-4" method="POST">
              <?php if ($isFormInvalid): ?>
                <div class="text-center text-red-500">Invalid form</div>
              <?php else: ?>
                <?php if ($didSend): ?>
                  <div class="text-center font-medium">
                    <p>We have received your message.</p>
                    <p>We will get back to you as soon as possible.</p>
                  </div>
                <?php endif ?>
              <?php endif ?>
              <div class="grid grid-cols-2 gap-3">
                <input
                  type="text"
                  class="bg-sky-100 px-4 py-3"
                  placeholder="Your Name (optional)"
                  name="name"
                />
                <input
                  type="email"
                  class="
                    bg-sky-100 px-4 py-3
                    <?php if ($isFormInvalid): ?>
                      <?php if (empty($_POST["email"])): ?>
                        outline outline-red-500
                      <?php endif ?>
                    <?php endif ?>
                  "
                  placeholder="Your Email"
                  name="email"
                  <?php if ($isFormInvalid && isset($_POST["email"]) && !empty($_POST["email"])): ?>
                    value="<?= $_POST["email"] ?>"
                  <?php endif ?>
                />
              </div>
              <input
                type="text"
                class="
                  bg-sky-100 px-4 py-3
                  <?php if ($isFormInvalid): ?>
                    <?php if (empty($_POST["messageSubject"])): ?>
                        outline outline-red-500
                      <?php endif ?>
                  <?php endif ?>
                "
                placeholder="Subject"
                name="messageSubject"
                <?php if ($isFormInvalid && isset($_POST["messageSubject"])): ?>
                  value="<?= $_POST["messageSubject"] ?>"
                <?php endif ?>
              />
              <textarea
                class="
                  resize-none w-full bg-sky-100 px-4 py-3 h-48
                  <?php if ($isFormInvalid): ?>
                    <?php if (!isset($_POST["messageBody"]) || empty($_POST["messageBody"])): ?>
                      outline outline-red-500
                    <?php endif ?>
                  <?php endif ?>
                "
                placeholder="Message"
                name="messageBody"
              ><?php if ($isFormInvalid && isset($_POST["messageBody"])): ?><?= $_POST["messageBody"] ?><?php endif ?></textarea>

              <button type="submit" class="bg-sky-500 text-white px-4 py-2 font-semibold text-lg hover:bg-green-400 duration-200">Send Message</button>
            </form>
          </div>
          <div class="hidden md:block">
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23653.585371774036!2d120.33135264414898!3d16.04665538470221!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x339167fe6bba4d67%3A0xf54b516c2c5d10b6!2sPHINMA-University%20of%20Pangasinan!5e0!3m2!1sen!2sph!4v1676424382967!5m2!1sen!2sph"
              class="w-full aspect-[4/3]"
              style="border:0;"
              allowfullscreen=""
              loading="lazy"
              referrerpolicy="no-referrer-when-downgrade">
            </iframe>
          </div>
        </div>
        <div class="grid sm:grid-cols-3 gap-5">
          <div class="grid items-center sm:grid-cols-[4rem_1fr] gap-4">
            <div class="flex justify-center">
              <span class="w-16 aspect-square bg-green-600 text-white inline-flex items-center justify-center">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-telephone-fill" viewBox="0 0 16 16">
                  <path fill-rule="evenodd" d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
                </svg>
              </span>
            </div>
            <div class="text-center sm:text-left">
              <p class="text-xl font-semibold">Call to ask any question.</p>
              <p class="text-2xl font-bold">+63 995-078-5660</p>
            </div>
          </div>
          <div class="grid items-center sm:grid-cols-[4rem_1fr] gap-5">
            <div class="flex justify-center">
              <div class="w-16 aspect-square bg-green-600 text-white flex items-center justify-center">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-envelope-open-fill" viewBox="0 0 16 16">
                  <path d="M8.941.435a2 2 0 0 0-1.882 0l-6 3.2A2 2 0 0 0 0 5.4v.314l6.709 3.932L8 8.928l1.291.718L16 5.714V5.4a2 2 0 0 0-1.059-1.765l-6-3.2ZM16 6.873l-5.693 3.337L16 13.372v-6.5Zm-.059 7.611L8 10.072.059 14.484A2 2 0 0 0 2 16h12a2 2 0 0 0 1.941-1.516ZM0 13.373l5.693-3.163L0 6.873v6.5Z"/>
                </svg>
              </div>
            </div>
            <div class="text-center sm:text-left ">
              <p class="text-xl font-semibold">Email to get free quote.</p>
              <p class="text-2xl font-bold">upangjobportal@gmail.com</p>
            </div>
          </div>
          <div class="grid items-center sm:grid-cols-[4rem_1fr] gap-5">
            <div class="flex justify-center">
              <div class="w-16 aspect-square bg-green-600 text-white flex items-center justify-center">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt-fill" viewBox="0 0 16 16">
                  <path d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z"/>
                </svg>
              </div>
            </div>
            <div class="text-center sm:text-left">
              <p class="text-xl font-semibold">Visit our office.</p>
              <p class="text-2xl font-bold">Arellano St, Dagupan City, 2400, Pangasinan</p>
            </div>
          </div>
        </div>
      </main>
    <?php include "../footer.php" ?>
    </div>
  </body>
</html>