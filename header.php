<?php if($islogin && $u_type == 3 &&  $u_verification_state < 2){?>
<div class="verify">
    <p>
        To apply for a job you must complete your registration
    </p>
    <a href="/<?= $__name__ ?>/profile/?page=general_information&sub=verified">
        GET STARTED
    </a>
</div>
<?php } ?>
<div class="header" style="
    z-index: 1000;
    background-color: #FFFFFF;
">
    <div class="text">
        <a href="/<?= $__name__ ?>" class="header_logo">
            <img src="/<?= $__name__ ?>/assets/logo.png" alt="logo">
            <p>CITE Job Portal</p>
        </a>
        <label for="navigation-toggle" class="toggle-nav-btn">
            <i class="fa fa-bars"></i>
        </label>
    </div>
    <input type="checkbox" id="navigation-toggle" class="toggle-nav">
    
    <div class="navigation">
        <?php if($islogin){?>
            <?php if($u_type == 1){?>
                <a href="/<?= $__name__ ?>/jobs">JOBS</a>
                <a href="/<?= $__name__ ?>/contact-us">CONTACT US</a>
                <a href="/<?= $__name__ ?>/dashboard/admin?page=accounts">ACCOUNTS</a>
                <a href="/<?= $__name__ ?>/login-history">LOGIN HISTORY</a>
                <a href="/<?= $__name__ ?>/dashboard/admin?page=profile">PROFILE</a>
                <a href="/<?= $__name__ ?>/dashboard/admin" class="nav_a">
                    DASHBOARD
                </a>
            <?php }elseif($u_type == 2){?>
                <a href="/<?= $__name__ ?>">HOME</a>
                <a href="/<?= $__name__ ?>/jobs">JOBS</a>
                <a href="/<?= $__name__ ?>/contact-us">CONTACT US</a>
                <a href="/<?= $__name__ ?>/dashboard/company">DASHBOARD</a>      
                <a href="/<?= $__name__ ?>/dashboard/company/?page=profile" class="nav_a">
                    PROFILE
                </a>
            <?php }elseif($u_type == 3){?>
                <a href="/<?= $__name__ ?>">HOME</a>
                <a href="/<?= $__name__ ?>/jobs">FIND JOBS</a>
                <a href="/<?= $__name__ ?>/jobs/applied.php">APPLIED JOBS</a>
                <a href="/<?= $__name__ ?>/contact-us">CONTACT US</a>
                <a href="/<?= $__name__ ?>/profile" class="nav_a">
                    Profile
                </a>
             
            <?php } ?>
        <?php }else{?>
            <a href="/<?= $__name__ ?>">HOME</a>
            <a href="/<?= $__name__ ?>/jobs">JOBS</a>
            <a href="/<?= $__name__ ?>/contact-us">CONTACT US</a>
            <a href="/<?= $__name__ ?>/auth?a=already" class="nav_a">
                LOGIN
            </a>
        <?php } ?>
    </div>

</div>
