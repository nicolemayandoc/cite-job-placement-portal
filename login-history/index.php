<?php
session_start();
require_once('../config.php');
require_once '../functions.php';
require_once('../session.php');

$pageSize = 10;
$limit = $pageSize + 1;
$pageNumber = 0;

if (isset($_GET["page"]) && is_numeric($_GET["page"]) && intval($_GET["page"]) > 0) {
  $pageNumber = $_GET["page"] - 1;
}

$offset = $pageNumber * $pageSize;

$query = "SELECT * FROM tbl_login_history
JOIN tbl_accounts
ON tbl_accounts.id = tbl_login_history.account_id
ORDER BY login_time DESC
LIMIT ? OFFSET ?";

$stmt = $con->prepare($query);
$stmt->bind_param("ii", $limit, $offset);
$stmt->execute();
$result = $stmt->get_result();

$loginHistory = array();
while ($row = $result->fetch_assoc())
  array_push($loginHistory, $row);

$query = "SELECT COUNT(tbl_login_history.id) AS id FROM tbl_login_history
JOIN tbl_accounts
ON tbl_accounts.id = tbl_login_history.account_id
ORDER BY login_time DESC";

$stmt = $con->prepare($query);
$stmt->execute();
$result = $stmt->get_result();

$totalCount = $result->fetch_assoc()["id"];
$pageCount = ceil($totalCount / $pageSize);

$hasNextPage = false;
$nextPage = -1;
if (count($loginHistory) > $pageSize) {
  $hasNextPage = true;
  $nextPage = $pageNumber + 2;
  // Remove the last item, since that is
  // just our next page marker.
  array_pop($loginHistory);
}

$hasPrevPage = false;
$prevPage = -1;
if ($pageNumber > 0) {
  $hasPrevPage = true;
  $prevPage = $pageNumber;
}

$islogin = false;
if(isset($_SESSION["islogin"]))
    $islogin = true;


if (!$islogin) {
  header("Location: /$__name__");
  exit();
}

if (intval($_SESSION["data"]["type"]) !== 1) {
  header("Location: /$__name__");
  exit();
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>CITE Job Portal</title>
    <link rel="icon" href="../assets/logo.png" >
    <!-- Google font: Poppins -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap"
      rel="stylesheet"
    />
    <link rel="stylesheet" href="../dashboard/admin/style.css">
    <link rel="stylesheet" href="../header.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11" defer></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- CSS utility and design framework -->
    <script src="https://cdn.tailwindcss.com"></script>
  </head>
  <body>
    <div id="app" class="font-[Poppins]">
      <?php include '../header.php' ?>
      <main class="max-w-6xl mx-auto px-8 pt-8">
        <h2 class="text-2xl font-semibold text-[#555555] mb-3">Login Activity</h2>
        <div>
          <div class="hidden lg:grid grid-cols-[12rem_12rem_12rem_12rem_1fr] px-3 py-2 text-white font-semibold" style="background-color: #4c8d9b">
            <div>Date</div>
            <div>Time</div>
            <div>First name</div>
            <div>Last name</div>
            <div>Browser</div>
          </div>
          <div class="mb-6 grid gap-3 lg:block">
            <?php foreach($loginHistory as $entry): ?>
              <!-- Desktop -->
              <div class="hidden lg:grid grid-cols-[12rem_12rem_12rem_12rem_1fr] border-b border-gray-300 text-cyan-800 px-3 py-2">
                <!-- Get date portion of the field -->
                <div><?= explode(" ", $entry["login_time"])[0] ?></div>
                <!-- Get time portion of the field -->
                <div><?= explode(" ", $entry["login_time"])[1] ?></div>
                <div><?= $entry["firstname"] ?></div>
                <div><?= $entry["lastname"] ?></div>
                <div><?= $entry["browser_name"] ?></div>
              </div>
              <!-- Mobile -->
              <div class="lg:hidden border border-gray-300 px-4 py-2 rounded shadow">
                <div>
                  <span class="font-semibold text-xl" style="color: #4c8d9b">
                    <?= $entry["firstname"] ?> <?= $entry["lastname"] ?>
                  </span>
                </div>
                <div>
                  <span class="font-semibold" style="color: #4c8d9b">Date:</span>
                  <?= explode(" ", $entry["login_time"])[0] ?>
                </div>
                <div>
                  <span class="font-semibold" style="color: #4c8d9b">Time:</span>
                  <?= explode(" ", $entry["login_time"])[1] ?>
                </div>
                <div>
                  <span class="font-semibold" style="color: #4c8d9b">Browser:</span>
                  <?= $entry["browser_name"] ?>
                </div>
              </div>
            <?php endforeach ?>
          </div>
          <div class="grid grid-cols-[4.5rem_auto_4.5rem] justify-between mb-4">
            <div>
            <?php if ($hasPrevPage): ?>
              <a href="
                <?php echo "{$_SERVER['PHP_SELF']}?page={$prevPage}" ?>
              "
                class="block font-medium text-white text-center px-4 py-2 bg-[#4c8d9b] hover:bg-[#539aa9] transition duration-200"
              >
                Prev
              </a>
            <?php endif ?>
            </div>
            <div class="flex gap-2">
              <?php
                for ($i = 0; $i < $pageCount; $i++):
                  $currentPage = $i + 1;
              ?>
                <a
                  <?php if ($i === $pageNumber): ?>
                    class="font-medium bg-[#4c8d9b] text-white hover:bg-[#539aa9] flex justify-center items-center w-6 transition duration-200"
                  <?php else: ?>
                    class="border border-gray-300 font-medium text-[#4c8d9b] flex justify-center items-center w-6 hover:bg-zinc-100 transition duration-200"
                  <?php endif ?>
                  href="
                    <?php echo "{$_SERVER['PHP_SELF']}?page={$currentPage}" ?>
                  "
                >
                  <?= $currentPage ?>
                </a>
              <?php endfor ?>
            </div>
            <div>
            <?php if ($hasNextPage): ?>
              <a href="
                <?php echo "{$_SERVER['PHP_SELF']}?page={$nextPage}" ?>
              "
                class="block font-medium text-white text-center px-4 py-2 bg-[#4c8d9b] hover:bg-[#539aa9] transition duration-200"
              >
                Next
              </a>
            <?php endif ?>
            </div>
          </div>
        </div>
      </main>
    </div>
  </body>
</html>
