<div class="header" style="
    z-index: 1000;
    background-color: #FFFFFF;
">
    <div class="text">
        <a href="/<?= $__name__ ?>" class="header_logo">
            <img src="/<?= $__name__ ?>/assets/logo.png" alt="logo">
            <p>CITE Job Portal</p>
        </a>
        <label for="navigation-toggle" class="toggle-nav-btn">
            <i class="fa fa-bars"></i>
        </label>
    </div>
    <input type="checkbox" id="navigation-toggle" class="toggle-nav">
    
    <div class="navigation">
        <a href="/<?= $__name__ ?>">HOME</a>
        <a href="/<?= $__name__ ?>/jobs">JOBS</a>
        <a href="/<?= $__name__ ?>/contact-us">CONTACT US</a>
        <a href="/<?= $__name__ ?>/dashboard/company">DASHBOARD</a>      
        <a href="/<?= $__name__ ?>/dashboard/company/?page=profile" class="nav_a">
            PROFILE
        </a>
    </div>

</div>
