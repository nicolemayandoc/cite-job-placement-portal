$(document).ready(() => {
  $(".frm_profile_password").on("submit", (e) => {
    e.preventDefault()

    var data = $(".frm_profile_password").serializeArray()
    $.ajax({
      url: "./routes/update_password.php",
      method: "post",
      data: data,
      success: (res) => {
        console.log(res)
        if (res.success) {
          Swal.fire("Success", `${res.message}`, "success")
          setTimeout(() => {
            window.location.href = "./?page=profile&sub=password"
          }, 1000)
        } else {
          Swal.fire("Failed", `${res.message}`, "error")
        }
      },
    })
  })
})
