<?php
session_start();
require_once '../../config.php';
require_once '../../functions.php';
require_once '../../session.php';

setlocale(LC_MONETARY, "en_US");

if ($islogin) {
  if ($u_type == 2) {
    $page = (form("page")) ? value("page") : "dashboard";

    $load_jobs = mysqli_query($con, "SELECT * FROM `tbl_jobs` WHERE userid = $u_id");
    $count_jobs = mysqli_num_rows($load_jobs);

    if (form("manage")) {
      $update = true;
      $id = mysqli_value($con, "manage");
      if (is_numeric($id)) {
        $manage_Job = mysqli_query($con, "SELECT * FROM `tbl_jobs` WHERE id = $id");
        if (hasResult($manage_Job)) {
          $data = mysqli_fetch_assoc($manage_Job);
        } else {
          $update = false;
        }
      } else {
        $update = false;
      }
    } else {
      $update = false;
    }

    if (form("filter") && value("sub") == "applicants") {
      $filter = strtolower(mysqli_value($con, "filter"));
      if ($filter == "pending") {
        $load_applicants = mysqli_query($con, "SELECT tbl_applicants.id, tbl_applicants.applicantsid, tbl_accounts.firstname, tbl_accounts.lastname, tbl_accounts.cnum, tbl_accounts.bday, tbl_accounts.address, tbl_accounts.age, tbl_resume.path AS 'resume', tbl_applicants.companyid, tbl_applicants.jobid, tbl_jobs.j_name, tbl_applicants.status, tbl_applicants.created_at FROM tbl_applicants INNER JOIN tbl_accounts ON tbl_accounts.id = tbl_applicants.applicantsid INNER JOIN tbl_resume ON tbl_resume.userid = tbl_applicants.applicantsid INNER JOIN tbl_jobs ON tbl_jobs.id = tbl_applicants.jobid WHERE tbl_applicants.companyid = $c_id AND  tbl_applicants.status = 1");
      } elseif ($filter == "hired") {
        $load_applicants = mysqli_query($con, "SELECT tbl_applicants.id, tbl_applicants.applicantsid, tbl_accounts.firstname, tbl_accounts.lastname, tbl_accounts.cnum, tbl_accounts.bday, tbl_accounts.address, tbl_accounts.age, tbl_resume.path AS 'resume', tbl_applicants.companyid, tbl_applicants.jobid, tbl_jobs.j_name, tbl_applicants.status, tbl_applicants.created_at FROM tbl_applicants INNER JOIN tbl_accounts ON tbl_accounts.id = tbl_applicants.applicantsid INNER JOIN tbl_resume ON tbl_resume.userid = tbl_applicants.applicantsid INNER JOIN tbl_jobs ON tbl_jobs.id = tbl_applicants.jobid WHERE tbl_applicants.companyid = $c_id AND  tbl_applicants.status = 2");
      } elseif ($filter == "declined") {
        $load_applicants = mysqli_query($con, "SELECT tbl_applicants.id, tbl_applicants.applicantsid, tbl_accounts.firstname, tbl_accounts.lastname, tbl_accounts.cnum, tbl_accounts.bday, tbl_accounts.address, tbl_accounts.age, tbl_resume.path AS 'resume', tbl_applicants.companyid, tbl_applicants.jobid, tbl_jobs.j_name, tbl_applicants.status, tbl_applicants.created_at FROM tbl_applicants INNER JOIN tbl_accounts ON tbl_accounts.id = tbl_applicants.applicantsid INNER JOIN tbl_resume ON tbl_resume.userid = tbl_applicants.applicantsid INNER JOIN tbl_jobs ON tbl_jobs.id = tbl_applicants.jobid WHERE tbl_applicants.companyid = $c_id AND  tbl_applicants.status = 3");
      } else {
        $load_applicants = mysqli_query($con, "SELECT tbl_applicants.id, tbl_applicants.applicantsid, tbl_accounts.firstname, tbl_accounts.lastname, tbl_accounts.cnum, tbl_accounts.bday, tbl_accounts.address, tbl_accounts.age, tbl_resume.path AS 'resume', tbl_applicants.companyid, tbl_applicants.jobid, tbl_jobs.j_name, tbl_applicants.status, tbl_applicants.created_at FROM tbl_applicants INNER JOIN tbl_accounts ON tbl_accounts.id = tbl_applicants.applicantsid INNER JOIN tbl_resume ON tbl_resume.userid = tbl_applicants.applicantsid INNER JOIN tbl_jobs ON tbl_jobs.id = tbl_applicants.jobid WHERE tbl_applicants.companyid = $c_id");
      }
    } else {
      $filter = "all";
      $load_applicants = mysqli_query($con, "SELECT tbl_applicants.id, tbl_applicants.applicantsid, tbl_accounts.firstname, tbl_accounts.lastname, tbl_accounts.cnum, tbl_accounts.bday, tbl_accounts.address, tbl_accounts.age, tbl_resume.path AS 'resume', tbl_applicants.companyid, tbl_applicants.jobid, tbl_jobs.j_name, tbl_applicants.status, tbl_applicants.created_at FROM tbl_applicants INNER JOIN tbl_accounts ON tbl_accounts.id = tbl_applicants.applicantsid INNER JOIN tbl_resume ON tbl_resume.userid = tbl_applicants.applicantsid INNER JOIN tbl_jobs ON tbl_jobs.id = tbl_applicants.jobid WHERE tbl_applicants.companyid = '$c_id'");
    }

    $count_applicants = mysqli_num_rows($load_applicants);
  } else {
    navigate("../../");
  }
} else {
  navigate("../../");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>DASHBOARD</title>
  <link rel="icon" href="../../assets/logo.png">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous" defer></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11" defer></script>
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="../../header.css">
  <!-- javascript -->
  <script src="js/index.js" defer></script>
  <script src="js/post_job.js" defer></script>
  <script src="js/edit_job.js" defer></script>
  <script src="js/manage_applicants.js" defer></script>
  <script src="js/update_general.js" defer></script>
  <script src="js/update_company.js" defer></script>
  <script src="js/update_password.js" defer></script>

  <!-- Declarative UI framework -->
  <script src="../../vue.global.js"></script>
  <!-- Import Luxon - library for calculating dates -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/luxon/3.1.0/luxon.min.js"></script>
  <!-- CSS utility framework and design system -->
  <script src="https://cdn.tailwindcss.com"></script>

  <style>
    .hide-markup {
      color: white !important;
    }

    .d-none {
      display: none !important;
    }
  </style>
</head>

<body>
  <div id="app" class="main">
    <?php include("../../header.php") ?>
    <div class="body" id="body_page_<?= $page ?>">
      <div class="profile_box" style="display:none">
        <div class="profile_box_header">
          <p class="profile_name">
            <?= $u_fname . " " . $u_lname ?>
          </p>
          <p class="profile_email">
            <?= $u_email ?>
          </p>
        </div>
        <div class="profile_box_body">
          <a href="./?page=profile">Account Information</a>
        </div>
        <div class="profile_box_footer">
          <a href="../../logout.php" class="btn_logout">Logout</a>
        </div>
      </div>
      <?php if ($page == "dashboard") { ?>
        <div class="flex flex-wrap gap-3 justify-center mt-10">
          <div class="flex flex-col sm:w-auto w-full">
            <div class="container flex flex-col items-center sm:w-[20rem] w-full">
              <div class="container_title bg-white rounded-t-lg border [border-color:_rgb(204,_204,_204)] py-4 text-center text-lg font-medium text-green-600 w-full">
                Company Logo
              </div>
              <div class="container_body body_image w-full bg-white border-x border-b [border-color:_rgb(204,_204,_204)]">
                <img src="../../assets/images/<?= $c_logo ?>" class="object-cover  company_logo_preview profile_picture">

                <br>
              </div>
            </div>
            <div>
              <div class="flex flex-col mt-5 z-10 h-[100%] w-[100%] gap-3 sm:w-[100%]">
                <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= ($page == "dashboard") ? 'bg-green-700 text-white' : "" ?>" href="/<?= $__name__ ?>/dashboard/company/">
                  <i class="fa fa-bar-chart"></i>
                  <p class="name">Dashboard</p>
                </a>
                <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "list") ? 'bg-green-700 text-white' : "" ?>" href="?page=hire&sub=list">
                  <i class="fa fa-list"></i>
                  <p class="name">List of Jobs</p>
                </a>
                <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "postajob") ? 'bg-green-700 text-white' : "" ?>" href="?page=hire&sub=postajob">
                  <i class="fa fa-plus-circle"></i>
                  <p class="name">Add Jobs</p>
                </a>
                <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "applicants") ? 'bg-green-700 text-white' : "" ?>" href="?page=hire&sub=applicants">
                  <i class="fa fa-users"></i>
                  <p class="name">List of Applicants</p>
                </a>
              </div>

            </div>
          </div>

          <div class="flex sm:mt-0 mt-10 flex-col gap-3 sm:w-[55%] w-full">
            <div class="container">
              <div class="container_title">

                <div class="max-w-4xl">
                  <h2>Hi, <?= $u_fname . " " . $u_lname ?> 👋</h2>
                  <div class="dashboard">
                    <div class="box">
                      <div class="box_name">
                        Open Jobs
                        <span class="font-bold">(<?= $count_jobs ?>)</span>
                      </div>
                      <a href="./?page=hire&sub=list">
                        VIEW
                      </a>
                    </div>
                    <div class="box">
                      <div class="box_name">
                        Applicants
                        <span class="font-bold">(<?= $count_applicants ?>)</span>
                      </div>
                      <a href="?page=hire&sub=applicants">
                        VIEW
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php } elseif ($page == "hire") { ?>
        <?php if (form("sub")) { ?>
          <div class="showcase" id="showcase_sub_<?= value("sub") ?>">
            <?php if (value("sub") == "list") { ?>
              <div class="flex flex-wrap gap-3 justify-center mt-10">
                <div class="flex flex-col sm:w-auto w-full">
                  <div class="container flex flex-col items-center sm:w-[20rem] w-full">
                    <div class="container_title text-center text-lg font-medium text-green-600 w-full">
                      Company Logo
                    </div>
                    <div class="container_body body_image w-full ">
                      <img src="../../assets/images/<?= $c_logo ?>" class="object-cover  company_logo_preview profile_picture">

                      <br>
                    </div>
                  </div>
                  <div>
                    <div class="flex flex-col mt-5 z-10 h-[100%] w-[100%] gap-3 sm:w-[100%]">
                      <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "dashboard") ? 'bg-green-700 text-white' : "" ?>" href="/<?= $__name__ ?>/dashboard/company/">
                        <i class="fa fa-bar-chart"></i>
                        <p class="name">Dashboard</p>
                      </a>
                      <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "list") ? 'bg-green-700 text-white' : "" ?>" href="?page=hire&sub=list">
                        <i class="fa fa-list"></i>
                        <p class="name">List of Jobs</p>
                      </a>
                      <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "postajob") ? 'bg-green-700 text-white' : "" ?>" href="?page=hire&sub=postajob">
                        <i class="fa fa-plus-circle"></i>
                        <p class="name">Add Jobs</p>
                      </a>
                      <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "applicants") ? 'bg-green-700 text-white' : "" ?>" href="?page=hire&sub=applicants">
                        <i class="fa fa-users"></i>
                        <p class="name">List of Applicants</p>
                      </a>
                    </div>

                  </div>
                </div>

                <div class="flex sm:mt-0 mt-10 flex-col gap-3 sm:w-[55%] w-full">

                  <div class="max-w-4xl">

                    <div class="container">
                      <div class="container_title sm:flex justify-between">
                        <div class="title mb-3 sm:mb-0">
                          Jobs
                        </div>
                        <div class="sm:flex gap-2">
                          <div>
                            <input v-model="searchTerm" type="text" placeholder=" Search ..." class="border border-gray-300 px-3 py-2 rounded-md w-[12rem] outline-none text-gray-800 focus:ring focus:ring-blue-300/40 focus:border-blue-500" />
                          </div>
                          <div class="flex gap-2">
                            <button type="button" @click="currentPage--" :disabled="!hasPrevPage" class="disabled:text-gray-200">
                              Prev
                            </button>
                            <button type="button" @click="currentPage++" :disabled="!hasNextJobsPage" class="disabled:text-gray-200">
                              Next
                            </button>
                          </div>
                        </div>
                      </div>
                      <div class="container_body">
                        <div v-if="isLoading" class="text-center py-2">
                          Loading ...
                        </div>
                        <div v-else-if="visibleJobs.length === 0" class="py-4">
                          <img src="./assets/empty.png" alt="empty" width="200" class="mx-auto">
                          <p class="text-center">No jobs found</p>
                        </div>
                        <a
                          v-else
                          v-for="job in visibleJobs"
                          :key="job.id"
                          :href="`?page=hire&sub=postajob&manage=${job.id}`"
                          class="box hide-markup justify-between"
                          :style=" { color: 'black !important' } ">
                          <div class="">
                            <p class="name">{{ job.j_name }}</p>
                            <p class="salary_range">
                              {{ job.j_currency_symbol }}
                              {{ job.j_min }}
                              -
                              {{ job.j_currency_symbol }}
                              {{ job.j_max }}
                            </p>
                            <p class="posted_at">
                              {{ getFormattedDate(job.j_created_at) }}
                            </p>
                          </div>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>


            <?php } elseif (value("sub") == "postajob") { ?>
              <div class="flex flex-wrap gap-3 justify-center mt-10">
                <div class="flex flex-col sm:w-auto w-full">
                  <div class="container flex flex-col items-center sm:w-[20rem] w-full">
                    <div class="container_title text-center text-lg font-medium text-green-600 w-full">
                      Company Logo
                    </div>
                    <div class="container_body body_image w-full ">
                      <img src="../../assets/images/<?= $c_logo ?>" class="object-cover company_logo_preview profile_picture">

                      <br>
                    </div>
                  </div>
                  <div>
                    <div class="flex flex-col mt-5 z-10 h-[100%] w-[100%] gap-3 sm:w-[100%]">
                      <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "dashboard") ? 'bg-green-700 text-white' : "" ?>" href="/<?= $__name__ ?>/dashboard/company/">
                        <i class="fa fa-bar-chart"></i>
                        <p class="name">Dashboard</p>
                      </a>
                      <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "list") ? 'bg-green-700 text-white' : "" ?>" href="?page=hire&sub=list">
                        <i class="fa fa-list"></i>
                        <p class="name">List of Jobs</p>
                      </a>
                      <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "postajob") ? 'bg-green-700 text-white' : "" ?>" href="?page=hire&sub=postajob">
                        <i class="fa fa-plus-circle"></i>
                        <p class="name">Add Jobs</p>
                      </a>
                      <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "applicants") ? 'bg-green-700 text-white' : "" ?>" href="?page=hire&sub=applicants">
                        <i class="fa fa-users"></i>
                        <p class="name">List of Applicants</p>
                      </a>
                    </div>

                  </div>
                </div>

                <div class="flex sm:mt-0 mt-10 flex-col    sm:w-[55%] w-full">
                  <div class="container max-w-4xl">
                    <div class="container_title c text-lg font-medium text-green-600 ">
                      <?= ($update) ? "Edit Job description" : "Post a Job" ?>
                    </div>
                    <div class="container_body">
                      <form method="post" class="<?= ($update) ? "edit_job" : "post_job" ?>">
                        <?php if ($update) { ?>
                          <div class="field" hidden>

                            <input type="hidden" name="id" value="<?= ($update) ? $data["id"] : "" ?>" required readonly>
                          </div>
                        <?php } ?>
                        <div>
                          <div class="sm:grid grid-cols-[1fr_6rem] gap-4 items-end">
                            <div class="sm:flex items-start mb-2 sm:mb-0">
  <label for="position_name" class="w-[180px] mt-3 font-medium text-gray-700">Job Title</label>
  <input type="text" name="position_name" id="position_name" class="block w-full px-4 py-2 mt-2 text-gray-700 placeholder-gray-400 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" placeholder="Enter job title" v-model="searchTerm">
</div>

                            <div>
                              <button type="button" class="px-5 py-[11px] bg-blue-600 text-white rounded-md hover:bg-blue-900 font-medium transition duration-200 disabled:bg-blue-300" :disabled="isSearchBtnDisabled" @click="onSearch">
                                Search
                              </button>
                            </div>
                          </div>
                          <div v-if="didSearch">
                            <div v-if="isLoading" class="mt-2">Searching ...</div>
                            <div v-else>
                              <div v-if="searchResults.length > 0">
                                <ul>
                                  <button type="button" v-for="job in visibleResults" :key="job.title" class="block w-full text-left px-2 py-1 border border-solid border-gray-300 hover:bg-zinc-50 rounded-md mt-2" @click="onSelectResult(job)">
                                    <p>{{ job.title }}</p>
                                    <p style="
                                  display: -webkit-box;
                                  -webkit-box-orient: vertical;
                                  -webkit-line-clamp: 1;
                                  overflow: hidden;
                                  ">{{ job.description }}</p>
                                  </button>
                                </ul>
                                <div class="flex justify-between">
                                  <div>
                                    <button type="button" v-if="isPrevBtnVisible" @click="pageNumber--" class="px-3 py-1 mt-2 border border-solid border-gray-300 rounded-md hover:bg-zinc-100 transition duration-200">
                                      Prev
                                    </button>
                                  </div>
                                  <div>
                                    <button type="button" v-if="isNextBtnVisible" @click="pageNumber++" class="px-3 py-1 mt-2 border border-solid border-gray-300 rounded-md hover:bg-zinc-100 transition duration-200">
                                      Next
                                    </button>
                                  </div>
                                </div>
                              </div>
                              <div v-else class="mt-2">No search results found.</div>
                            </div>
                          </div>
                        </div>
                        <div class="sm:flex items-start">
  <label for="position_age" class="w-[175px] mt-3 font-medium text-gray-700">Minimum Age</label>
  <input type="number" name="position_age" id="position_age" class="block w-full px-4 py-2 mt-2 text-gray-700 placeholder-gray-400 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" placeholder="Minimum age" value="<?= ($update) ? $data["j_age"] : "" ?>" maxlength="2" oninput="this.value=this.value.slice(0,2)">
</div>
<div class="sm:flex items-start">
  <label for="position_age_max" class="w-[175px] mt-5 font-medium text-gray-700">Maximum Age</label>
  <input type="number" name="position_age_max" id="position_age_max" class="block w-full px-4 py-2 mt-2 text-gray-700 placeholder-gray-400 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" placeholder="Maximum age" value="<?= ($update) ? $data["j_age_max"] : "" ?>" maxlength="2" oninput="this.value=this.value.slice(0,2)">
</div>



                        <?php if ($update) { ?>
                          <div class="sm:flex items-start mt-3">
  <label for="position_gender" class="w-[175px] mt-3 font-medium text-gray-700">Gender</label>
  <div class="relative w-full">
    <select name="position_gender" id="position_gender" class="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline-blue focus:border-blue-500">
      <option value="Male" <?= ($data["j_gender"] == "Male") ? "selected" : "" ?>>Male</option>
      <option value="Female" <?= ($data["j_gender"] == "Female") ? "selected" : "" ?>>Female</option>
      <option value="Both" <?= ($data["j_gender"] == "Both") ? "selected" : "" ?>>Disclosed</option>
    </select>
    <div class="absolute inset-y-0 right-0 flex items-center px-2 pointer-events-none">
    <svg class="fill-current h-4 w-4" viewBox="0 0 20 20"><path d="M6 8l4 4 4-4"></path></svg>
    </div>
  </div>
</div>


                        <?php } else { ?>
                          <div class="sm:flex items-start mt-3 ">
  <label for="position_gender" class="w-[180px] mt-3 font-medium text-gray-700">Gender</label>
  <div class="relative w-full">
    <select name="position_gender" id="position_gender" class="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline-blue focus:border-blue-500">
      <option value="Male">Male</option>
      <option value="Female">Female</option>
      <option value="Both">Disclosed</option>
    </select>
    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
      <svg class="fill-current h-4 w-4" viewBox="0 0 20 20"><path d="M6 8l4 4 4-4"></path></svg>
    </div>
  </div>
</div>


                          
                        <?php } ?>

                        <div class="sm:flex items-start">
  <label for="minimum_salary" class="w-[180px] mt-3 font-medium text-gray-700">Minimum Salary</label>
  <input type="number" id="minimum_salary" name="minimum_salary" class="block w-full px-4 py-2 mt-2 text-gray-700 placeholder-gray-400 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" placeholder="Enter minimum salary" value="<?= ($update) ? $data["j_min"] : "" ?>" min="10000" maxlength="5" oninput="this.value=this.value.slice(0,5)">
</div>

<div class="sm:flex items-start">
  <label for="maximum_salary" class="w-[180px] mt-3 font-medium text-gray-700">Maximum Salary</label>
  <input type="number" id="maximum_salary" name="maximum_salary" class="block w-full px-4 py-2 mt-2 text-gray-700 placeholder-gray-400 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" placeholder="Enter maximum salary" value="<?= ($update) ? $data["j_max"] : "" ?>" maxlength="11" oninput="this.value=this.value.slice(0,11)">
</div>
                        <!--
                    <div class="field">
                      <select name="currency_symbol" id="currency_symbol">
                        <option value="" <?= ($update) ? "" : "selected" ?> disabled>Currency symbol</option>
                        <option value="₱" <?= ($update && $data["j_currency_symbol"] == "₱") ? "selected" : "" ?> >PH Peso</option>
                        <option value="$" <?= ($update && $data["j_currency_symbol"] == "$") ? "selected" : "" ?> >US Dollars</option>
                      </select>
                    </div>
                    -->
                       <div class="sm:flex items-start">
  <label for="highlights" class="w-[180px] mt-3 font-medium text-gray-700">Job Highlights</label>
  <textarea id="highlights" name="highlights" class="block w-full px-4 py-2 mt-2 text-gray-700 placeholder-gray-400 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" placeholder="Enter job highlights"><?= ($update) ? $data["j_highlights"] : "" ?></textarea>
</div>

<div class="sm:flex items-start">
  <label for="description" class="w-[180px] mt-3 font-medium text-gray-700">Job Description</label>
  <textarea id="description" name="description" class="description block w-full px-4 py-2 mt-2 text-gray-700 placeholder-gray-400 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" placeholder="Enter job description" v-model="description"></textarea>
</div>

                        <button class="button btn_submit bg-blue-500 hover:bg-blue-600 text-white font-medium py-3 px-6 rounded-lg transition duration-300 ease-in-out transform hover:scale-15 hover:-translate-y-1 ">
                          SAVE
                        </button>
                      </form>
                      <?php if ($update) { ?>
                        <button data-id="<?= ($update) ? $data["id"] : "" ?>" class="btn_delete_job">
                          DELETE
                        </button>
                      <?php } ?>
                    </div>
                  </div>
                </div>


              </div>

            <?php } elseif (value("sub") == "applicants") { ?>

              <div class="flex flex-wrap gap-3 justify-center mt-10">
                <div class="flex flex-col sm:w-auto w-full">
                  <div class="container flex flex-col items-center sm:w-[20rem] w-full">
                    <div class="container_title text-center w-full text-lg font-medium text-green-600">
                      Company Logo
                    </div>
                    <div class="container_body body_image w-full ">
                      <img src="../../assets/images/<?= $c_logo ?>" class="object-cover company_logo_preview profile_picture">
                      <br>
                    </div>
                  </div>
                  <div>
                    <div class="flex flex-col mt-5 z-10 h-[100%] w-[100%] gap-3 sm:w-[100%]">
                      <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "dashboard") ? 'bg-green-700 text-white' : "" ?>" href="/<?= $__name__ ?>/dashboard/company/">
                        <i class="fa fa-bar-chart"></i>
                        <p class="name">Dashboard</p>
                      </a>
                      <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transition text-green-700 border border-green-700 <?= (value("sub") == "list") ? 'bg-green-700 text-white' : "" ?>" href="?page=hire&sub=list">
                        <i class="fa fa-list"></i>
                        <p class="name">List of Jobs</p>
                      </a>
                      <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transition text-green-700 border border-green-700 <?= (value("sub") == "postajob") ? 'bg-green-700 text-white' : "" ?>" href="?page=hire&sub=postajob">
                        <i class="fa fa-plus-circle"></i>
                        <p class="name">Add Jobs</p>
                      </a>
                      <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transition text-green-700 border border-green-700 <?= (value("sub") == "applicants") ? 'bg-green-700 text-white' : "" ?>" href="?page=hire&sub=applicants">
                        <i class="fa fa-users"></i>
                        <p class="name">List of Applicants</p>
                      </a>
                    </div>


                  </div>
                </div>
                <div class="flex sm:mt-0 mt-5 flex-col gap-3 sm:w-[55%] w-full">

                  <div class="max-w-4xl">

                    <div class="container">
                      <div class="[color:_green] p-4 border-b [border-color:_rgb(204,_204,_204)] lg:flex justify-between items-center">
                        <div>
                          Applicants
                        </div>
                        <div class="lg:flex gap-2">
                          <!-- Search filters and pagination buttons -->
                          <div class="sm:flex gap-2">
                            <div class="flex gap-2">
                              <button type="button" @click="currentPage--" :disabled="!hasPrevPage" class="disabled:text-gray-200">
                                Prev
                              </button>
                              <button type="button" @click="currentPage++" :disabled="!hasNextApplicantsPage" class="disabled:text-gray-200">
                                Next
                              </button>
                            </div>
                            <div>
                              <input v-model="searchTerm" type="text" placeholder=" Search ..." class="w-full max-w-[12rem] border border-gray-300 px-3 py-2 rounded-md w-[12rem] outline-none text-gray-800 focus:ring focus:ring-blue-300/40 focus:border-blue-500" />
                            </div>
                          </div>
                          <div class="gap-1 flex flex-wrap justify-center items-center mt-3 lg:mt-0">
  <button type="button" :class="selectedApplicantsTab === 'all' ? 'bg-green-600 text-white' : 'bg-white text-black border-gray-300'" @click="onClickApplicantsTab('all')" class="flex-1 border-r-2 border-gray-300 md:rounded-l-lg rounded-r-none w-full md:w-auto pr-2 py-2 px-4 text-sm">
    ALL 
  </button>
  <div class="h-6 border-2 border-gray-300"></div>

  <button type="button" :class="selectedApplicantsTab === 'pending' ? 'bg-green-600 text-white border border-gray-400 rounded-sm' : 'bg-white text-black border border-gray-300 rounded-sm'" @click="onClickApplicantsTab('pending')" class="flex-1 border-r-2 border-gray-300 md:w-auto py-2 px-4 text-sm">
    SHORT LISTED
  </button>
  <div class="h-6 border-2 border-gray-300"></div>

  <button type="button" :class="selectedApplicantsTab === 'hired' ? 'bg-green-500 text-white' : 'bg-white text-black border-gray-300'" @click="onClickApplicantsTab('hired')" class="flex-1 border-r-2 border-gray-300 w-full md:w-auto py-2 px-4 text-sm">
    HIRED
  </button>
  <div class="h-6 border-2 border-gray-300"></div>

  <button type="button" :class="selectedApplicantsTab === 'declined' ? 'bg-green-600 text-white' : 'bg-white text-black'" @click="onClickApplicantsTab('declined')" class="flex-1 md:rounded-r-lg rounded-l-none py-2 px-4 w-full md:w-auto text-sm">
    DECLINED
  </button>
</div>

                        </div>
                      </div>
                      <div class="container_body">
                        <div v-if="isLoading" class="text-center py-2">
                          Loading ...
                        </div>
                        <div v-else-if="visibleApplicants.length === 0" class="py-4">
                          <img src="./assets/empty.png" alt="empty" width="200" class="mx-auto">
                          <p class="text-center">No applicants found</p>
                        </div>
                        <div v-else v-for="applicant in visibleApplicants" :key="applicant.id" class="box d-none" :style=" { display: 'flex !important' }">
                          <div>
                            <p class="j_name">
                              <span class="label" style="color:black; font-weight: 500;">Job Title : </span>
                              {{ applicant.j_name }}
                            </p>
                            <p class="name">
                              <span class="label"  style="color:black; font-weight: 500;">Full name : </span>
                              {{ applicant.firstname }} {{ applicant.lastname }}
                            </p>
                            <p class="posted_at">
                              <span class="label" style="color:black; font-weight: 500;">Birthday : </span>
                              {{ getFormattedDate(applicant.bday) }}
                            </p>
                            <p class="posted_at">
                              <span class="label" style="color:black; font-weight: 500;">Age : </span>
                              {{ getAgeFromBirthDate(applicant.bday) }}
                            </p>
                            <p class="address">
                              <span class="label" style="color:black; font-weight: 500;">Address : </span>
                              {{ applicant.address }}
                            </p>
                            <p class="posted_at">
                              <span class="label" style="color:black; font-weight: 500;">Posted at : </span>
                              {{ getFormattedDate(applicant.created_at) }}
                            </p>
                          </div>
                         <div class="sm:flex sm:gap-2">
  <div v-if="!(applicant.status === 2 || applicant.status === 3)" class="flex flex-col gap-2 sm:flex-row">
    <button @click="onHireApplicant(applicant.id)" class="w-full sm:w-auto px-5 py-2 bg-green-700 text-white rounded-md hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-opacity-50">
      <i class="fa fa-check"></i>
      HIRE
    </button>
    <button @click="onDeclineApplicant(applicant.id)" class="w-full sm:w-auto px-4 py-2 bg-red-600 text-white rounded-md hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-red-500 focus:ring-opacity-50">
      <i class="fa fa-times"></i>
      DECLINE
    </button>
  </div>
  <div class="sm:flex gap-2 mt-2 sm:mt-0">
    <lightbox :pdf="`../../resume/${applicant.resume}`"></lightbox>
    <a
      :href="`../../resume/${applicant.resume}`"
      class="mt-2 sm:mt-0 inline-block w-full sm:w-auto px-4 py-3 text-center bg-blue-500 text-white rounded-md hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-opacity-50 text-center"
      download
    >
      <i class="fa fa-file-text-o"></i>
      DOWNLOAD RESUME
    </a>
  </div>
</div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>


        <?php } else {
              navigate("?page=hire&sub=list");
            } ?>
    </div>
  <?php } else {
          navigate("?page=hire&sub=list");
        } ?>
<?php } elseif ($page == "profile") { ?>
  <?php if (form("sub")) { ?>

    <div class="content">
      <div class="showcase" id="showcase_sub_<?= value("sub") ?>">
        <?php if (value("sub") == "general_information") { ?>
          <div class="flex gap-5 justify-center flex-wrap mt-5 ">
            <div class="flex flex-col w-full mb-4 sm:w-[20rem]">
              <div class="container flex flex-col items-center max-w-3xl mx-auto">
                <div class="container_title w-full text-center text-lg font-medium text-green-600">
                  Avatar
                </div>

                <div class="container_body body_image w-full">
                  <img src="../../assets/images/<?= $u_avatar ?>" class="avatar round-image">
                  <input type="file" name="input_upload_field" id="input_upload_field" class="input_upload_field" data-set="avatar" accept="image/*">
                  <input type="file" name="input_upload_field" id="change_avatar" class="input_upload_field" data-set="avatar" data-preview="avatar" accept="image/*">
                  <br>
                  <label for="change_avatar" class="btn_upload_picture mt-5 w-full">CHANGE AVATAR</label>
                </div>
              </div>
              <div>
                <div class="flex flex-col mt-5 z-10 h-[100%] w-[100%] gap-3 sm:w-[100%]">

                  <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "company_information") ? 'bg-green-700 text-white' : "" ?>" href="?page=profile&sub=company_information">
                    <i class="fa fa-building-o "></i>
                    <p class="name">Company Information</p>
                  </a>
                  <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "general_information") ? 'bg-green-700 text-white' : "" ?>" href="?page=profile&sub=general_information">
                    <i class="fa fa-user-circle-o"></i>
                    <p class="name">General Information</p>
                  </a>
                  <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "password") ? 'bg-green-700 text-white' : "" ?>" href="?page=profile&sub=password">
                    <i class="fa fa-key "></i>
                    <p class="name">Password</p>
                  </a>
                  <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "password") ? 'bg-green-700 text-white' : "" ?>" href="/<?= $__name__ ?>/logout.php">
                    <i class="fa fa-sign-out"></i>
                    <span class="name">Log Out</span>
                  </a>

                </div>

              </div>
            </div>
            <div class="container max-w-3xl">
              <div class="container_title text-lg font-medium text-green-600 ">
                Personal information & Account settings
              </div>
              <div class="container_body">
                <form method="post" class="frm_<?= $page ?>">
                  <div class="sm:grid grid-cols-[130px_1fr]">
                    <label for="tb_firstname" style="color: black; font-weight: 500;">First Name</label>
                    <input type="text" name="tb_firstname" id="tb_firstname" placeholder="e.g. John" value="<?= $u_fname ?>" class="w-full border border-gray-400 rounded py-2 px-4 mb-3 leading-tight focus:ring-1 focus:ring-green-800 focus:bg-white focus:border-green-400 capitalize">
                  </div>

                  <div class="sm:grid grid-cols-[130px_1fr]">
                    <label for="tb_lastname" style="color: black; font-weight: 500;">Last name</label>
                    <input type="text" name="tb_lastname" id="tb_lastname" placeholder="e.g. Doe" value="<?= $u_lname ?>" class="w-full border border-gray-400 rounded py-2 px-4 mb-3 leading-tight focus:ring-1 focus:ring-green-800 focus:bg-white focus:border-green-400 capitalize">
                  </div>
                  <div class="sm:grid grid-cols-[130px_1fr]">
                    <!-- <label for="tb_age">Age</label> -->
                    <!--
                          We shouldn't be storing ages as an integer, as it will get outdated very quickly.
                          For compatibility, let's keep this here with a default value of -1.
                        -->
                    <input type="hidden" name="tb_age" id="tb_age" placeholder="Age" value="-1">
                  </div>
                  <div class="sm:grid grid-cols-[130px_1fr]">
                    <label for="tb_bday" style="color: black; font-weight: 500;">Birthday</label>
                    <input type="date" name="tb_bday" id="tb_bday" value="<?= $u_bday ?>" maxlength="8" oninput="this.value=this.value.slice(0,10)" class="w-full border border-gray-400 rounded py-2 px-4 mb-3 leading-tight focus:ring-1 focus:ring-green-800 focus:bg-white focus:border-green-400">
                  </div>
                 
                  <div class="sm:grid grid-cols-[130px_1fr]">
                    <label for="tb_email" style="color: black; font-weight: 500;">Email Add.</label>
                    <input type="email" name="tb_email" id="tb_email" placeholder="e.g. johndoe@gmail.com" value="<?= $u_email ?>" class="w-full border border-gray-400 rounded py-2 px-4 mb-3 leading-tight focus:ring-1 focus:ring-green-800 focus:bg-white focus:border-green-400">
                  </div>
                  <div class="sm:grid grid-cols-[130px_1fr]">
                    <label for="tb_cnum" style="color: black; font-weight: 500;">Contact no.</label>
                 
                    <input
                      type="number"
                      name="tb_cnum"
                      id="tb_cnum"
                      placeholder="e.g. 9XXX-XXX-YYY"
                      maxlength="10"
                      oninput="this.value=this.value.slice(0,10)"
                      class="w-full border border-gray-400 rounded py-2 px-4 mb-3 leading-tight focus:ring-1 focus:ring-green-800 focus:bg-white focus:border-green-400"
                      value="<?= substr($u_cnum, 1) ?>"
                    > 
                  </div>
                  <div class="sm:grid grid-cols-[130px_1fr]">
                    <label for="tb_address" style="color: black; font-weight: 500;">Address</label>
                    <textarea name="tb_address" class="tb_address w-full border border-gray-400 rounded py-2 px-4 mb-3 leading-tight focus:ring-1 focus:ring-green-800 focus:bg-white focus:border-green-400" id="tb_address" placeholder="e.g. 39 Plaza Dr, Rockwell Center, Makati, 1200 Metro Manila "> <?= $u_address ?> </textarea>
                  </div>
                  <button class="btn bg-green-700 mt-3 text-white py-3 px-3 rounded-lg hover:bg-green-600 transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-10">
                    UPDATE
                  </button>
                </form>
              </div>
            </div>
          </div>
        <?php } elseif (value("sub") == "company_information") { ?>
          <div class="flex flex-wrap gap-5 justify-center mt-5">
            <div class="flex flex-col w-full mb-4 sm:w-[20rem]">
              <div class="container flex flex-col items-center max-w-3xl mx-auto">
                <div class="container_title text-center text-lg font-medium text-green-600  w-full">
                  Company Logo
                </div>
                <div class="container_body body_image w-full ">
                  <img src="../../assets/images/<?= $c_logo ?>" class="object-cover company_logo_preview profile_picture">
                  <input type="file" name="input_upload_field" id="change_company_logo" class="input_upload_field" data-set="company_logo" data-preview="company_logo_preview" accept="image/*">
                  <br>
                  <label for="change_company_logo" class="w-full mt-5 btn_upload_picture">CHANGE COMPANY LOGO</label>
                </div>
              </div>
              <div>
                <div class="flex flex-col mt-5 z-10 h-[100%] w-[100%] gap-3 sm:w-[100%]">

                  <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "company_information") ? 'bg-green-700 text-white' : "" ?>" href="?page=profile&sub=company_information">
                    <i class="fa fa-building-o "></i>
                    <p class="name">Company Information</p>
                  </a>
                  <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "general_information") ? 'bg-green-700 text-white' : "" ?>" href="?page=profile&sub=general_information">
                    <i class="fa fa-user-circle-o"></i>
                    <p class="name">General Information</p>
                  </a>
                  <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "password") ? 'bg-green-700 text-white' : "" ?>" href="?page=profile&sub=password">
                    <i class="fa fa-key"></i>
                    <p class="name">Password</p>
                  </a>
                  <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "password") ? 'bg-green-700 text-white' : "" ?>" href="/<?= $__name__ ?>/logout.php">
                    <i class="fa fa-sign-out"></i>
                    <span class="name">Log Out</span>
                  </a>
                </div>
              </div>
            </div>

            <div class="flex sm:mt-0 mt-10 flex-col gap-3 sm:w-1/2 w-full">
              <div class="container max-w-3xl">
                <div class="container_title text-lg font-medium text-green-600">
                  Company Banner
                </div>
                <div class="container_body body_image">
                  <img src="../../assets/images/<?= $c_banner ?>" class="object-cover rounded-md company_banner_preview profile_picture">
                  <input type="file" name="input_upload_field" id="change_company_banner" class="input_upload_field" data-set="company_banner" data-preview="company_banner_preview" accept="image/*">
                  <br>
                  <label for="change_company_banner" class="mt-3 btn_upload_picture">CHANGE COMPANY BANNER</label>
                </div>
              </div>
              <div class="container max-w-3xl">
                <div class="container_title text-lg font-medium text-green-600 ">
                  Company Information
                </div>
                <div class="container_body">
                  <form method="post" class="frm_<?= $page ?>_company">
                    <div class="sm:grid grid-cols-[150px_1fr]">
                      <label for="tb_name" style="color: black; font-weight: 500;">Company name</label>
                      <input type="text" name="tb_name" id="tb_name" placeholder="e.g. PHINMA Corporation" value="<?= $c_name ?>" class="w-full border border-gray-400 rounded py-2 px-4 mb-3 leading-tight focus:ring-1 focus:ring-green-800 focus:bg-white focus:border-green-400">
                    </div>
                    <div class="sm:grid grid-cols-[150px_1fr]">
                      <label for="tb_cnum" style="color: black; font-weight: 500;">Business number</label>
                      <input type="number" 
                      name="tb_cnum" 
                      id="tb_cnum" 
                      placeholder="e.g. 9XXX-XXX-YYY" 
                      value="<?= substr($c_cnum, 1) ?>" 
                      maxlength="10" 
                      oninput="this.value=this.value.slice(0,10)" 
                      class="w-full border border-gray-400 rounded py-2 px-4 mb-3 leading-tight focus:ring-1 focus:ring-green-200 focus:bg-white focus:border-green-400">
                
                    </div>

                    <div class="sm:grid grid-cols-[150px_1fr] ">
                      <label for="tb_address" style="color: black; font-weight: 500;">Address</label>
                      <textarea name="tb_address" class="tb_address w-full border border-gray-400 rounded py-2 px-4 mb-3 leading-tight focus:ring-1 focus:ring-green-800 focus:bg-white focus:border-green-400" id="tb_address" placeholder="e.g. 39 Plaza Dr, Rockwell Center, Makati, 1200 Metro Manila "><?= $c_address ?></textarea>
                    </div>
                    <div class="sm:grid grid-cols-[150px_1fr]">
                      <label for="tb_address" style="color: black; font-weight: 500;">Description</label>
                      <textarea name="tb_description" class="tb_description w-full border border-gray-400 rounded py-2 px-4 mb-3 leading-tight focus:ring-1 focus:ring-green-800 focus:bg-white focus:border-green-400" id="tb_description" placeholder="Describe your company"><?= $c_description ?></textarea>
                    </div>
                    <button class="button btn_submit bg-green-800 text-white py-3 px-4 rounded-lg hover:bg-green-700 transition duration-200 ease-in-out transform hover:-translate-y-1 hover:scale-10">

                      UPDATE
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        <?php } elseif (value("sub") == "password") { ?>

          <div class="flex gap-5 justify-center flex-wrap mt-5">
            <div class="flex flex-col w-full mb-4 sm:w-[20rem]">
              <div class="container flex flex-col items-center max-w-3xl mx-auto">
                <div class="container_title w-full text-center text-lg font-medium text-green-600">
                  Avatar
                </div>
                <div class="container_body body_image w-full">
                  <img src="../../assets/images/<?= $u_avatar ?>" class="avatar round-image">
                  <input type="file" name="input_upload_field" id="input_upload_field" class="input_upload_field" data-set="avatar" accept="image/*">
                  <input type="file" name="input_upload_field" id="change_avatar" class="input_upload_field" data-set="avatar" data-preview="avatar" accept="image/*">
                  <br>
                  <label for="change_avatar" class="btn_upload_picture mt-5 w-full">CHANGE AVATAR</label>
                </div>
              </div>
              <div>
                <div class="flex flex-col mt-5 z-10 h-[100%] w-[100%] gap-3 sm:w-[100%]">

                  <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "company_information") ? 'bg-green-700 text-white' : "" ?>" href="?page=profile&sub=company_information">
                    <i class="fa fa-building-o "></i>
                    <p class="name">Company Information</p>
                  </a>
                  <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "general_information") ? 'bg-green-700 text-white' : "" ?>" href="?page=profile&sub=general_information">
                    <i class="fa fa-user-circle-o"></i>
                    <p class="name">General Information</p>
                  </a>
                  <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "password") ? 'bg-green-700 text-white' : "" ?>" href="?page=profile&sub=password">
                    <i class="fa fa-key "></i>
                    <p class="name">Password</p>
                  </a>
                  <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("sub") == "logout") ? 'bg-green-700 text-white' : "" ?>" href="/<?= $__name__ ?>/logout.php">
                    <i class="fa fa-sign-out"></i>
                    <span class="name">Log Out</span>
                  </a>
                </div>


              </div>
            </div>

            <div class="container p-6 max-w-3xl ">

              <h2 class="text-lg font-medium text-green-600 ">Change Password</h2>
              <hr class="my-2 border-gray-400">
              <form method="post" class="frm_<?= $page ?>_password">
                <div class="flex flex-col mb-4">
                  <label for="tb_pw" class="text-gray-700 font-medium mb-2">Old password</label>
                  <p class="text-gray-600 text-xs italic mt-1">Please enter your current password to proceed with the password change.</p>
                  <input type="password" name="tb_pw" id="tb_pw" class="border border-gray-400 p-2 rounded-lg focus:outline-none focus:shadow-outline hover:border-green-800 transition-all duration-200 focus:border-green-800 focus:shadow-outline focus:bg-white focus:shadow-none focus:mb-0" placeholder="Enter your current password">
                </div>
                <div class="flex flex-col mb-4">
                  <label for="tb_newpw" class="text-gray-700 font-medium mb-2">New password</label>
                  <p class="text-gray-600 text-xs italic mt-1">Enter your new password below. Make sure it's at least 8 characters long.</p>

                  <input type="password" name="tb_newpw" id="tb_newpw" class="border border-gray-400 p-2 rounded-lg focus:outline-none focus:shadow-outline hover:border-green-800 transition-all duration-200 focus:border-green-800 focus:shadow-outline focus:bg-white focus:shadow-none focus:mb-0" placeholder="Enter your new password">
                </div>
                <div class="flex flex-col mb-4">
                  <label for="tb_cnewpw" class="text-gray-700 font-medium mb-2">Confirm new password</label>
                  <p class="text-gray-600 text-xs italic mt-1">Confirm your new password by retyping it below. Make sure it matches the new password you entered above.</p>
                  <input type="password" name="tb_cnewpw" id="tb_cnewpw" class="border border-gray-400 p-2 rounded-lg focus:outline-none focus:shadow-outline hover:border-green-500 transition-all duration-200 focus:border-green-800 focus:shadow-outline focus:bg-white focus:shadow-none focus:mb-0" placeholder="Retype new password">
                </div>

                <button class="btn bg-green-700 text-white py-3 px-4 rounded-lg hover:bg-green-600 transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-10">
                  UPDATE
                </button>
              </form>
            </div>
          </div>
      </div>



    <?php } else {
            navigate("?page=profile&sub=general_information");
          } ?>
    </div>
  </div>
<?php } else {
          navigate("?page=profile&sub=general_information");
        } ?>
<?php } elseif ($page == "settings") { ?>
  4
<?php } else {
        navigate("./");
      } ?>
</div>
</div>

</body>
<script src="https://unpkg.com/vue-easy-lightbox@next/dist/vue-easy-lightbox.umd.min.js"></script>  
<script>
  const {
    createApp
  } = Vue

  const app = createApp({
    data() {
      return {
        siteName: "<?= $__name__ ?>",
        userId: "<?= $u_id ?>",
        companyId: "<?= $c_id ?>",
        searchTerm: "<?= ($update) ? $data["j_name"] : "" ?>",
        didSearch: false,
        isLoading: true,
        searchResults: [],
        jobs: [],
        applicants: [],
        selectedApplicantsTab: "all",
        currentPage: 0, // for Jobs pagination
        pageNumber: 0, // for Job description suggestions pagination
        pageSize: 5,
        description: '<?= ($update) ? str_replace(array("\r", "\n"), '', $data["j_description"]) : "" ?>',
      }
    },
    async mounted() {
      try {
        const {
          jobs
        } = await (
          await fetch(`/${this.siteName}/dashboard/company/routes/getJobsByCompanyId.php?id=${this.userId}`)
        ).json();

        const {
          applicants
        } = await (
          await fetch(`/${this.siteName}/dashboard/company/routes/getApplicantsByCompanyId.php?id=${this.companyId}`)
        ).json();

        if (jobs) {
          this.jobs = jobs
        }

        if (applicants) {
          this.applicants = applicants
        }
      } catch (e) {
        console.log("Error occured:", e)
      } finally {
        this.isLoading = false
      }
    },
    computed: {
      filteredApplicants() {
        return this.applicants
          .filter((applicant) => {
            if (this.selectedApplicantsTab === "all") return true
            if (this.selectedApplicantsTab === "pending" && applicant.status === 1) {
              console.log("pending applicant", applicant)
              return true
            }
            if (this.selectedApplicantsTab === "hired" && applicant.status === 2) {
              console.log("hired applicant", applicant)
              return true
            }
            if (this.selectedApplicantsTab === "declined" && applicant.status === 3) return true

            return false
          })
          .filter((applicant) =>
            `${applicant.firstname} ${applicant.lastname}`
            .toLowerCase()
            .includes(this.searchTerm.toLowerCase())
          )
      },
      visibleApplicants() {
        return this.filteredApplicants.slice(this.currentPage * this.pageSize, this.currentPage * this.pageSize + this.pageSize)
      },
      hasNextApplicantsPage() {
        if (this.filteredApplicants.length === 0) return false

        if (this.currentPage !== Math.ceil(this.filteredApplicants.length / this.pageSize) - 1)
          return true

        return false
      },
      filteredJobs() {
        return this.jobs.filter((job) =>
          job.j_name.toLowerCase().includes(this.searchTerm.toLowerCase())
        )
      },
      visibleJobs() {
        return this.filteredJobs.slice(this.currentPage * this.pageSize, this.currentPage * this.pageSize + this.pageSize)
      },
      hasNextJobsPage() {
        if (this.filteredJobs.length === 0) return false

        if (this.currentPage !== Math.ceil(this.filteredJobs.length / this.pageSize) - 1)
          return true

        return false
      },
      hasPrevPage() {
        if (this.currentPage > 0)
          return true

        return false
      },
      visibleResults() {
        return this.searchResults.slice(
          this.pageNumber * this.pageSize,
          this.pageNumber * this.pageSize + this.pageSize
        )
      },
      isNextBtnVisible() {
        if (this.searchResults > this.pageSize) return true
        if (
          1 + this.pageNumber !==
          Math.ceil(this.searchResults.length / this.pageSize) // page count
        )
          return true

        return false
      },
      isPrevBtnVisible() {
        if (this.pageNumber > 0) return true

        return false
      },
      isSearchBtnDisabled() {
        if (this.searchTerm === "") return true
        if (this.isLoading) return true

        return false
      },
    },
    watch: {
      searchTerm() {
        this.currentPage = 0
      }
    },
    methods: {
      async onSearch() {
        try {
          this.didSearch = true
          this.isLoading = true

          const response = await fetch(
            `https://findjobdescriptions.vercel.app/api/search/${this.searchTerm}?limit=20`
          )
          const {
            result
          } = await response.json()

          this.searchResults = result.jobs
          console.log("searchResults", this.searchResults)
        } catch (e) {
          console.log("An error occured", e)
        } finally {
          this.isLoading = false
        }
      },
      onSelectResult(job) {
        this.searchTerm = job.title
        this.description = job.description

        this.searchResults = []
        this.didSearch = false
      },
      onClickApplicantsTab(tabName) {
        this.selectedApplicantsTab = tabName
      },
      onHireApplicant(applicantId) {
        Swal.fire({
          icon: 'question',
          title: 'Hire?',
          text: 'Do you want to continue this action?',
          showCancelButton: true,
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.isConfirmed) {
            $.ajax({
              url: "./routes/manage_applicants.php",
              method: "post",
              data: {
                id: applicantId,
                action: 2
              },
              success: (res) => {
                console.log(res)
                if (res.success) {
                  setTimeout(() => {
                    window.location.href = "?page=hire&sub=applicants"
                  }, 500);
                } else {
                  Swal.fire(
                    'Failed',
                    `${res.message}`,
                    'error'
                  )
                }
              }
            });
          }
        })
      },
      onDeclineApplicant(applicantId) {
        Swal.fire({
          icon: 'question',
          title: 'Decline?',
          text: 'Do you want to continue this action?',
          showCancelButton: true,
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.isConfirmed) {
            $.ajax({
              url: "./routes/manage_applicants.php",
              method: "post",
              data: {
                id: applicantId,
                action: 3
              },
              success: (res) => {
                console.log(res)
                if (res.success) {
                  Swal.fire(
                    'Success',
                    `${res.message}`,
                    'success'
                  )
                  setTimeout(() => {
                    window.location.href = "?page=hire&sub=applicants"
                  }, 500);
                } else {
                  Swal.fire(
                    'Failed',
                    `${res.message}`,
                    'error'
                  )
                }
              }
            });
          }
        })
      },
      getAgeFromBirthDate(birthDate) {
        if (!birthDate) return "N/A"

        const duration = luxon.DateTime.fromFormat(
          birthDate,
          "yyyy-MM-dd", {
            zone: "Asia/Manila",
          }
        ).diffNow("years").years

        const dropSign = Math.abs(duration)
        const dropDecimals = Math.trunc(dropSign)
        return dropDecimals
      },
      getFormattedDate(dateStr) {
        const date = new Date(dateStr)
        const year = date.getFullYear()
        const monthInt = date.getMonth() + 1
        const month = monthInt < 10 ? `0${monthInt}` : `${monthInt}`
        const dayOfTheMonthInt = date.getDate()
        const dayOfTheMonth = dayOfTheMonthInt < 10 ? `0${dayOfTheMonthInt}` : `${dayOfTheMonthInt}`

        return `${month}/${dayOfTheMonth}/${year}`
      }
    },
  })
  app.component("lightbox", {
    props: {
      pdf: String
    },
    data() {
      return {
        siteName: "<?php echo $__name__ ?>",
        isVisible: false
      }
    },
    mounted() {
      console.log("mounted:", this.siteName)
    },
    computed: {
      getPdfUrl() {
        return `${window.location.protocol}//${window.location.host}/${this.siteName}/resume/${this.pdf.split("/")[this.pdf.split("/").length-1]}`
      }
    },
    methods: {
      onHide() {
        this.isVisible = false
      },
      show() {
        this.isVisible = true
      }
    },
    
    template: `
    
    <div>
      <button
      class="w-full px-5 py-3 bg-green-600 text-white rounded-md hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-opacity-50"
        @click="show"
      >
        <i class="fa fa-file-text-o"></i>
        VIEW RESUME
      </button>
      <div
        v-if="isVisible"
        class="fixed z-[2000] inset-0 bg-black/50 grid"
      >
        <div class="max-w-3xl mx-auto w-full pt-6 px-6 pb-6">
          <div class="flex justify-end h-6">
          <button type="button" class="text-white text-3xl flex items-center" @click="onHide">
  ×
</button>
          </div>
          <object
            :data="getPdfUrl"
            type="application/pdf"
            class="w-full [height:_calc(100vh_-_3rem_-_1.5rem)]"
          >
          </object>
        </div>
      </div>
    </div>
    `
  })
  

  app.use(VueEasyLightbox)
  app.mount("#app")
</script>

</html>