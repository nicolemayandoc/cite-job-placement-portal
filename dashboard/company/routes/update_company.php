<?php 

session_start();
require_once '../../../config.php';
require_once '../../../functions.php';
require_once '../../../session.php';

header("Content-Type: application/json");


function message($status,$message){
    $msg = array(
        "success" => $status,
        "message" => $message
    );
    echo arraytojson($msg);
    die();
}


if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $data = $_POST;

    $name = $data["tb_name"];
    $cnum = $data["tb_cnum"];
 
$contactNumberWithAreaCode = "+63$cnum";
    $address = $data["tb_address"];
    $description = $data["tb_description"];

    if($name == ""){
        message(false,"Company name is required.");
    }
    if($cnum == ""){
        message(false,"Business number is required.");
    } elseif (strlen($cnum) < 10) {
        message(false, "Business number must contain 10 digits.");
    } elseif (!preg_match("/^9\d{9}$/", $cnum)) {
        message(false, "Invalid business number format. Please enter a mobile number starting with '9'.");
    }
    if($address == ""){
        message(false,"Company address is required.");
    }
    if($description == ""){
        message(false,"Company description is required.");
    }
    $contactNumberWithAreaCode = "+63$cnum";
    $update_company_query = mysqli_query($con,"
    
        UPDATE
            `tbl_company`
        SET
            `c_name` = '$name',
            `c_address` = '$address',
            `c_cnum` = '$contactNumberWithAreaCode',
            
            `c_description` = '$description'
        WHERE
            `id` = $c_id
    ");

    if($update_company_query){
        message(true,"Succesfully saved.");
    }else{
        message(false,"Found error in the server.");
    }
}
?>