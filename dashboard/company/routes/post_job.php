<?php

session_start();
require_once '../../../config.php';
require_once '../../../functions.php';
require_once '../../../session.php';

header("Content-Type: application/json");

function getCompanyName($conn, $companyId) {
    $query = "SELECT c_name FROM tbl_company WHERE userid = ?";
    $stmt = $conn->prepare($query);
    $stmt->bind_param("i", $companyId);
    $stmt->execute();
    $result = $stmt->get_result();
    $companyName = $result->fetch_assoc()["c_name"];

    return $companyName;
}

if($_SERVER['REQUEST_METHOD'] === 'POST'){

    $position_name = mysqli_value($con,"position_name");
    $position_age = mysqli_value($con,"position_age");
    $position_age_max = mysqli_value($con,"position_age_max");
    $position_gender = mysqli_value($con,"position_gender");
    $minimum_salary = mysqli_value($con,"minimum_salary");
    $maximum_salary = mysqli_value($con,"maximum_salary");
    $currency_symbol = "₱";
    $highlights = mysqli_value($con,"highlights");
    $description = mysqli_value($con,"description");

    function message($status,$message){
        $msg = array(
            "success" => $status,
            "message" => $message
        );
        echo arraytojson($msg);
        die();
    }

 
    if($position_age == ""){
        message(false, "Please enter the qualification age (min).");
    }elseif($position_age < 18){
        message(false, "The qualification age must be 18 years old and above.");
    }

    if($position_age_max == ""){
        message(false, "Please enter the qualification age (max).");
    }elseif($position_age_max > 64){
        message(false, "The maximum qualification age must be 64 years old and below.");
    }
    if ($position_age >= $position_age_max) {
        if ($position_age == $position_age_max) {
            message(false, "The minimum and maximum age must be different.");
        } else {
            message(false, "The maximum age must be greater than the minimum age.");
        }
    }
    
      if($minimum_salary < 10000){
        message(false, "Minimum salary must be at least 10,000.");
    }
    if($maximum_salary == ""){
        message(false, "Please enter the maximum salary.");
    } elseif($minimum_salary >= $maximum_salary) {
        if ($minimum_salary == $maximum_salary && $minimum_salary <= 10000) {
            message(false, "The minimum and maximum salaries cannot be equal and less than or equal to 10,000. Please adjust the values and try again");
        } else {
            
            message(false, "Maximum salary must be greater than the minimum salary.");
        }
    }
    
    if($currency_symbol == ""){
        message(false, "Please select currency symbol.");
    }
    if($highlights == ""){
        message(false, "Please enter the job highlights.");
    }
    if($description == ""){
        message(false, "Please enter the job description.");
    }

    $save_query = mysqli_query($con,"
    INSERT INTO `tbl_jobs`(
        `userid`,
        `j_name`,
        `j_age`,
        `j_age_max`,
        `j_min`,
        `j_max`,
        `j_currency_symbol`,
        `j_highlights`,
        `j_description`,
        `j_gender`
    )
    VALUES(
        $u_id,
        '$position_name',
        $position_age,
        $position_age_max,
        $minimum_salary,
        $maximum_salary,
        '$currency_symbol',
        '$highlights',
        '$description',
        '$position_gender'
    )
    ");
   
    $dateMinNow = new DateTime('now');
    $dateMin = $dateMinNow->modify("-$position_age year")->format('Y-m-d');
    $dateMaxNow = new DateTime('now');
    $dateMax = $dateMaxNow->modify("-$position_age_max year")->format('Y-m-d');

    $query = "SELECT *
        FROM `tbl_accounts`
        WHERE  `type` = 3
        AND `verification_state` = 2
        AND bday <= '$dateMin'
        AND bday >= '$dateMax'
    ";

    $select_all_applicants = mysqli_query($con, $query);
    while($row = mysqli_fetch_assoc($select_all_applicants)){
        $cnum = $row["cnum"];
        $firstname = $row["firstname"];
        $companyName = getCompanyName($con, $u_id);
        $sms_message = "Job Alert:\nHello $firstname,\nPostion Name: $position_name at $companyName is now open! Login to your CITE Job Placement Portal account to apply.";
        $sms_log = mysqli_query($con,"
            INSERT INTO `tbl_sms_logs`(
                `receiverid`,
                `message`
            )
            VALUES(
                $u_id,
                '$sms_message'
            )
        ");
        clicksend_sms($cnum,$sms_message);
    }
    

    if($save_query){
        message(true,"Successfully posted.");
    }else{
        message(false,"Failed to post the jobs.");
    }
}