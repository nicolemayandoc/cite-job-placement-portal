<?php 
session_start();
require_once '../../../config.php';
require_once '../../../functions.php';
require_once '../../../session.php';

header("Content-Type: application/json");

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $data = $_POST;

    function message($status,$message){
        $msg = array(
            "success" => $status,
            "message" => $message
        );
        echo arraytojson($msg);
        die();
    }


    $pw = $data["tb_pw"];
    $newpw = $data["tb_newpw"];
    $cnewpw = $data["tb_cnewpw"];

    if($pw == ""){
        message(false,"Please enter the Password.");
    }
    if($newpw == ""){
        message(false,"Please enter the New Password.");
    }
    if($cnewpw == ""){
        message(false,"Please confirm the New Password.");
    }

    if(strlen($newpw) < 8){
        message(false, "Password must be at least 8 characters.");
    }

    if($newpw != $cnewpw) {
        message(false,"New password does not match. Enter new password again here.");
    }


    if(!password_verify($pw, $u_password)){
        message(false,"The Current password is incorrect. Please try again.");
    }

    if (password_verify($newpw, $u_password)) {
        message(false,"You've using the Password that you already used");
    }

    $hashed_new_password = password_hash($newpw, PASSWORD_DEFAULT);

    $change_password_query = mysqli_query($con,"
    UPDATE
        `tbl_accounts`
    SET
        `password` = '$hashed_new_password'
    WHERE
        `id` = $u_id
    ");

    if($change_password_query){
        message(true,"Successfully saved!");
    }else{
        message(false,"Failed to save the password!");
    }
}
?>