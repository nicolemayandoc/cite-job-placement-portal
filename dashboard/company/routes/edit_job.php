<?php

session_start();
require_once '../../../config.php';
require_once '../../../functions.php';
require_once '../../../session.php';

header("Content-Type: application/json");

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    

    $id = mysqli_value($con,"id");
    $position_name = mysqli_value($con,"position_name");
    $position_age = mysqli_value($con,"position_age");
    $position_age_max = mysqli_value($con,"position_age_max");
    $minimum_salary = mysqli_value($con,"minimum_salary");
    $maximum_salary = mysqli_value($con,"maximum_salary");
    $currency_symbol = "₱";
    $gender = mysqli_value($con,"position_gender");
    $highlights = mysqli_value($con,"highlights");
    $description = mysqli_value($con,"description");

    function message($status,$message){
        $msg = array(
            "success" => $status,
            "message" => $message
        );
        echo arraytojson($msg);
        die();
    }

    if($position_name == ""){
        message(false, "Please enter the position name.");
    }
    if($position_age == ""){
        message(false, "Please enter the qualification age (min).");
    }elseif($position_age < 18){
        message(false, "The qualification age must be 18 years old and above.");
    }

    if($position_age_max == ""){
        message(false, "Please enter the qualification age (max).");
    }elseif($position_age_max > 64){
        message(false, "The maximum qualification age must be 64 years old and below.");
    }
    if($position_age >= $position_age_max){
        message(false, "Minimum age must be less than maximum age.");
      }
      if($minimum_salary < 10000){
        message(false, "Minimum salary must be at least 10,000.");
    }
    if($maximum_salary == ""){
        message(false, "Please enter the maximum salary.");
    } elseif($minimum_salary >= $maximum_salary) {
        if ($minimum_salary == $maximum_salary && $minimum_salary <= 10000) {
            message(false, "The minimum salary must be greater than 10000 when it is equal to the maximum salary.");
        } else {
            message(false, "Maximum salary must be greater than the minimum salary.");
        }
    }
    if($highlights == ""){
        message(false, "Please enter the job highlights.");
    }
    if($description == ""){
        message(false, "Please enter the job description.");
    }


    $update_job = mysqli_query($con,"
    UPDATE
        `tbl_jobs`
    SET
        `j_name` = '$position_name',
        `j_age` = $position_age,
        `j_age_max` = $position_age_max,
        `j_min` = $minimum_salary,
        `j_max` = $maximum_salary,
        `j_currency_symbol` = '$currency_symbol',
        `j_highlights` = '$highlights',
        `j_description` = '$description',
        `j_gender` = '$gender'
    WHERE
        `id` = $id
    ");


    if($update_job){
        message(true,"Successfully saved.");
    }else{
        message(false,"Failed to update the selected job.");
    }
}