<?php 
session_start();
require_once '../../../config.php';
require_once '../../../functions.php';
require_once '../../../session.php';

header("Content-Type: application/json");

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $data = $_POST;

    function message($status,$message){
        $msg = array(
            "success" => $status,
            "message" => $message
        );
        echo arraytojson($msg);
        die();
    }

    $firstname = $data["tb_firstname"];
    $lastname = $data["tb_lastname"];
    $age = $data["tb_age"];
    $bday = date("Y-m-d",strtotime($data["tb_bday"]));
    $address = $data["tb_address"];
    $today = new DateTime();
    $bday = new DateTime($bday);
    $diff = $today->diff($bday);
    $age = $diff->y;
    $email = $data["tb_email"];
    $cnum = $data["tb_cnum"];
    
    if (!isset($age)) {
        message(false, "Please enter your birthdate to update your profile.");
      } else if ($age < 18) {
        message(false, "You must be 18 years old or older to update your profile. Please enter a valid birthdate.");
      } else if ($age >= 64) {
        message(false, "You cannot update your profile as you are over 64 years old. Please contact our support team for further assistance.");
      }
      
    if($firstname == ""){
        message(false,"Please enter your first name.");
    }
    if($lastname == ""){
        message(false,"Please enter your last name.");
    }
    if($bday == ""){
        message(false,"Please enter your birth day.");
    }
    if($age == ""){
        message(false,"Please enter your age.");
    }
    if($address == ""){
        message(false,"Please enter your address.");
    }

    if($email == ""){
        message(false,"Please enter email address.");
    }
    
    if($cnum == ""){
        message(false,"Please enter contact number.");
    } elseif (strlen($cnum) < 10) {
        message(false, "Contact no. must contain 10 digits.");
    } elseif (!preg_match("/^9\d{9}$/", $cnum)) {
        message(false, "Invalid contact number format. Please enter a mobile number starting with '9'.");
    }

    if(checkemail($con,$email,"AND id != $u_id")){
        message(false,"This Email Address has already been registered. Please use a different one.");
    }
    
  

    $formattedBday = $bday->format('Y-m-d');
    $contactNumberWithAreaCode = "+63$cnum";
    $update_general = mysqli_query($con,"
    UPDATE
        `tbl_accounts`
    SET
        `firstname` = '$firstname',
        `lastname` = '$lastname',
        `bday` = '$formattedBday',
        `age` = '$age',
        `address` = '$address',
        `cnum` = '$contactNumberWithAreaCode',
        `email` = '$email'
    WHERE
        `id` = $u_id
    ");

    if($update_general){
        message(true,"Successfully updated.");
    }else{
        message(false,"Failed to update your account.");
    }
}
?>  