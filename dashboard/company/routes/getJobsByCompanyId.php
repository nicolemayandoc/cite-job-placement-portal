<?php
require_once '../../../config.php';
header("Content-Type: application/json");


if ($_SERVER["REQUEST_METHOD"] !== "GET") {
  echo json_encode([
    "message" => "Only get requests are supported."
  ]);
  exit();
}

$companyId = $_GET["id"];
$query = "SELECT * FROM `tbl_jobs` WHERE userid = ?";

$stmt = $con->prepare($query);
$stmt->bind_param("i", $companyId);
$stmt->execute();

$result = $stmt->get_result();
$jobs = array();

while ($row = $result->fetch_assoc())
  array_push($jobs, $row);

echo json_encode([
  "message" => "Retrived all jobs.",
  "jobs" => $jobs
]);
