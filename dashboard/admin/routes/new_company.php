<?php 

session_start();
require_once '../../../config.php';
require_once '../../../functions.php';
require_once '../../../session.php';

header("Content-Type: application/json");

$data = $_POST;

$account_type = 2;
$u_id = $data["u_id"];
$company_name = $data["company_name"];
$company_cnum = $data["company_cnum"];
$company_address = $data["company_address"];

$department = $data["company_department"];



function message($status,$message){
    $msg = array(
        "success" => $status,
        "message" => $message
    );
    echo arraytojson($msg);
    die();
}



if($company_name == ""){
    message(false,"Company name is required.");
}
if($company_cnum == ""){
    message(false,"Business number is required.");
}elseif(strlen($company_cnum) < 11){
    message(false,"Business number must contains 11 digits.");
}elseif(!preg_match("/^09\d{9}$/", $company_cnum)){
    message(false,"Invalid Business number format. Please enter a mobile number starting with '09'.");
}

if($company_address == ""){
    message(false,"Company address is required.");
}

if($department == ""){
    message(false,"Department is required.");
}

$companyNameUserQry = mysqli_query($con,"SELECT c_name FROM `tbl_company` WHERE `c_name` = '$company_name'");

if (hasResult($companyNameUserQry)) {
    message(false,"This company name is registered already.");
}

//check if the company is already registered
$validate_company = mysqli_query($con,"
    SELECT * FROM `tbl_company` WHERE `userid` = $u_id
");

if(hasResult($validate_company)){
    message(false,"The user you want to merge with this company is already merged with another company.");
}

$register_company = mysqli_query($con,"
    INSERT INTO `tbl_company`(
        `userid`,
        `c_name`,
        `c_address`,
        `c_cnum`,
        `c_position`,
        `department`
    )
    VALUES(
        $u_id,
        '$company_name',
        '$company_address',
        '$company_cnum',
        '',
        '$department'
    )
");


if($register_company){
    message(true,"Company created successfully!");
}else{
    message(false,"Failed to add company!");
}
?>