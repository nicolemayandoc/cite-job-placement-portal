<?php 

session_start();
require_once '../../../config.php';
require_once '../../../functions.php';
require_once '../../../session.php';

header("Content-Type: application/json");


function message($status,$message){
    $msg = array(
        "success" => $status,
        "message" => $message
    );
    echo arraytojson($msg);
    die();
}

function getUserIdOfCompanyId($conn, $companyId) {
    $query = "SELECT userid FROM tbl_company WHERE id = ?";
    $stmt = $conn->prepare($query);
    $stmt->bind_param('i', $companyId);
    $stmt->execute();

    $result = $stmt->get_result();
    $userId = $result->fetch_assoc()["userid"];
    return $userId;
}

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $data = $_POST;

    $c_id = $data["tb_id"];
    $name = $data["tb_name"];
    $cnum = $data["tb_cnum"];
    $publisher_firstname = $data["tb_publisher_firstname"];
    $publisher_lastname = $data["tb_publisher_lastname"];
  
    $address = $data["tb_address"];
    $created_at = $data["tb_created_at"];

    if($name == ""){
        message(false,"Company name is required. ");
    }
    if($cnum == ""){
        message(false,"Business number is required.");
    }elseif(strlen($cnum) < 10){
        message(false,"Contact number must contain 10 digits.");
    } elseif (!preg_match("/^9\d{9}$/", $cnum)) {
        message(false, "Invalid business number format. Please enter a mobile number starting with '9'.");
    }
   
    if($address == ""){
        message(false,"Company address is required.");
    }

    if($publisher_firstname == ""){
        message(false,"Owner first name is required.");
    }

    if($publisher_lastname == ""){
        message(false,"Owner last name is required.");
    }

    $userId = getUserIdOfCompanyId($con, $c_id);
    mysqli_query($con,"
        UPDATE
            `tbl_accounts`
        SET
            `firstname` = '$publisher_firstname',
            `lastname` = '$publisher_lastname'
        WHERE
            `id` = $userId
    ");
    $contactNumberWithAreaCode = "+63$cnum";
    $update_company_query = mysqli_query($con,"
        UPDATE
            `tbl_company`
        SET
            `c_name` = '$name',
            `c_address` = '$address',
            `c_cnum` = '$contactNumberWithAreaCode',
            `created_at` = '$created_at'
        WHERE
            `id` = $c_id
    ");

    if($update_company_query){
        message(true,"Succesfully saved.");
    }else{
        message(false,"Found error in the server.");
    }
}
?>