<?php
require_once '../../../config.php';
header("Content-Type: application/json");


if ($_SERVER["REQUEST_METHOD"] !== "GET") {
  echo json_encode([
    "message" => "Only get requests are supported."
  ]);
  exit();
}

$query = "SELECT
tbl_applicants.id, tbl_applicants.applicantsid,
tbl_accounts.firstname, tbl_accounts.lastname,
tbl_accounts.cnum, tbl_accounts.bday,
tbl_accounts.address, tbl_accounts.age,
tbl_resume.path AS 'resume',
tbl_applicants.companyid,
tbl_applicants.jobid, tbl_jobs.j_name,
tbl_applicants.status, tbl_applicants.created_at
FROM tbl_applicants
INNER JOIN tbl_accounts ON tbl_accounts.id = tbl_applicants.applicantsid
INNER JOIN tbl_resume ON tbl_resume.userid = tbl_applicants.applicantsid
INNER JOIN tbl_jobs ON tbl_jobs.id = tbl_applicants.jobid";

$result = $con->query($query);
$applicants = array();

while ($row = $result->fetch_assoc())
  array_push($applicants, $row);

echo json_encode([
  "message" => "Retrieved all applicants.",
  "applicants" => $applicants
]);
