<?php
require_once '../../../config.php';
header("Content-Type: application/json");


if ($_SERVER["REQUEST_METHOD"] !== "GET") {
  echo json_encode([
    "message" => "Only get requests are supported."
  ]);
  exit();
}

$query = "SELECT * FROM `tbl_company`";
$result = $con->query($query);
$companies = array();

while ($row = $result->fetch_assoc())
  array_push($companies, $row);

echo json_encode([
  "message" => "Retrieved all companies.",
  "companies" => $companies
]);
