<?php
require_once '../../../config.php';
header("Content-Type: application/json");


if ($_SERVER["REQUEST_METHOD"] !== "GET") {
  echo json_encode([
    "message" => "Only get requests are supported."
  ]);
  exit();
}

$query = "SELECT * FROM `tbl_accounts` WHERE type = 2";
$result = $con->query($query);
$companyAccounts = array();

while ($row = $result->fetch_assoc())
  array_push($companyAccounts, $row);

echo json_encode([
  "message" => "Retrieved all company accounts.",
  "companyAccounts" => $companyAccounts
]);
