<?php
require_once '../../../config.php';
header("Content-Type: application/json");


if ($_SERVER["REQUEST_METHOD"] !== "GET") {
  echo json_encode([
    "message" => "Only get requests are supported."
  ]);
  exit();
}

$query = "SELECT * FROM `tbl_jobs`";
$result = $con->query($query);
$jobs = array();

while ($row = $result->fetch_assoc())
  array_push($jobs, $row);

echo json_encode([
  "message" => "Retrieved all jobs.",
  "jobs" => $jobs
]);
