<?php 
session_start();
require_once '../../../config.php';
require_once '../../../functions.php';
require_once '../../../session.php';

header("Content-Type: application/json");

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $data = $_POST;

    function message($status,$message){
        $msg = array(
            "success" => $status,
            "message" => $message
        );
        echo arraytojson($msg);
        die();
    }

    $firstname = $data["tb_firstname"];
    $lastname = $data["tb_lastname"];
    $bday = date("Y-m-d",strtotime($data["tb_bday"]));
    $address = $data["tb_address"];
    $email = $data["tb_email"];
    $cnum = $data["tb_cnum"];

    if($firstname == ""){
        message(false,"Please enter your first name.");
    }
    if($lastname == ""){
        message(false,"Please enter your last name.");
    }
    if (is_nan(strtotime($bday))) {
        message(false, "Please enter a valid date of birth.");
    } else if ($bday == "1970-01-01" || $bday == "2222-22-22") {
        message(false, "Please enter a valid date of birth.");
    } else {
        $today = new DateTime();
        $birthdate = new DateTime($bday);
        $age = $today->diff($birthdate)->y;
        if ($age < 18) {
            message(false, "You must be 18 years old or older to update your profile. Please enter a valid birthdate.");
        } else if ($age > 64) {
            message(false, "You cannot update your profile as you are over 64 years old. Please contact our support team for further assistance.");
        }
    }   
    
    if($address == ""){
        message(false,"Please enter your address.");
    }

    if($email == ""){
        message(false,"Please enter email address.");
    }

    if($cnum == ""){
        message(false,"Please enter contact number.");
    }elseif(strlen($cnum) < 11){
        message(false,"Contact number must contain 11 numbers.");
    }elseif(!preg_match("/^09\d{9}$/", $cnum)){
        message(false,"Invalid contact number format. Please enter a mobile number starting with '09'.");
    }

    if(checkemail($con,$email,"AND id != $u_id")){
        message(false,"The Email address you entered is already taken. Create another one.");
    }

    if(checkcnum($con,$cnum,"AND id != $u_id")){
        message(false,"The Contact number you entered is already taken. Create another one.");
    }

    $update_general = mysqli_query($con,"
    UPDATE
        `tbl_accounts`
    SET
        `firstname` = '$firstname',
        `lastname` = '$lastname',
        `bday` = '$bday',
        `age` = -1,
        `address` = '$address',
        `cnum` = '$cnum',
        `email` = '$email'
    WHERE
        `id` = $u_id
    ");

    if($update_general){
        message(true,"Successfully updated.");
    }else{
        message(false,"Failed to update your account.");
    }
}
?>  