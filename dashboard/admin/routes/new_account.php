<?php
require_once '../../../config.php';
require_once '../../../functions.php';
require_once '../../../session.php';

header("Content-Type: application/json");

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = $_POST;

    $account_type  = $data["account_type"];
    $fname = $data["fname"];
    $lname = $data["lname"];
    $cnum = $data["cnum"];
    $bday = date("Y-m-d", strtotime($data["bday"]));
    $address = $data["address"];
    $email = $data["email"];
    $password = $data["password"];
    $cpassword = $data["cpassword"];
    $department = "none";

    function message($status, $message)
    {
        $msg = array(
            "success" => $status,
            "message" => $message
        );
        echo arraytojson($msg);
        die();
    }

    if ($fname == "") {
        message(false, "First name is required. ");
    }
    if ($lname == "") {
        message(false, "Last name is required. ");
    }
    if ($cnum == "") {
        message(false, "Contact number is required.");
    } elseif (strlen($cnum) < 10) {
        message(false, "Contact no. must contain 10 digits.");
    } elseif (!preg_match("/^9\d{9}$/", $cnum)) {
        message(false, "Invalid contact number format. Please enter a mobile number starting with '9'.");
    }
    if ($bday == "" || $bday == "1970-01-01" || $bday == "2222-22-22") {
        message(false, "Please enter a valid Birthday.");
    } else {
        $today = new DateTime();
        $birthdate = new DateTime($bday);
        $age = $today->diff($birthdate)->y;
        if ($age < 18) {
            message(false, "You can't add an account under 18 years old.");
        } else if ($age >= 64) {
            message(false, "You can't add an account over 64 years old.");
        }
    }
    
    

    if ($address == "") {
        message(false, "Address is required. ");
    }
    if ($email == "") {
        message(false, "Email address is required. ");
    } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        message(false, "Email is not valid. ");
    } else if (strpos($email, "@") === false || strpos($email, ".com") === false) {
        message(false, "Email should contain '@' and '.com'.");
    }
    if ($password == "") {
        message(false, "Password is required. ");
    } elseif ($password != $cpassword) {
        message(false, "Password doesn't match.");
    } elseif (strlen($password) < 8) {
        message(false, "Password must be 8 characters");
    }
    if ($department == "") {
        message(false, "Please select Department.");
    }
    if ($account_type == "" || $account_type == "0") {
        message(false, "Please select account level.");
    }
    //check if email is already registered
    $check_email = mysqli_query($con, "SELECT * FROM `tbl_accounts` WHERE `email` = '$email'");
    if ($check_email) {
        if (hasResult($check_email)) {
            message(false, "The email address you entered is already taken. Create another one.");
        }
    } else {
        message(false, "There was a problem with your Email Address.");
    }

    $check_cnum = mysqli_query($con, "SELECT * FROM `tbl_accounts` WHERE `cnum` = '$cnum'");
    if ($check_cnum) {
        if (hasResult($check_cnum)) {
            message(false, "The Contact number you entered is already taken. Create another one.");
        }
    } elseif (!is_numeric($cnum)) {
        message(false, "Contact number must contain only numbers.");
    }

    //register_account
    $hashed_password = password_hash($password, PASSWORD_DEFAULT);
    $contactNumberWithAreaCode = "+63$cnum";
    $register_account = mysqli_query($con, "
    INSERT INTO `tbl_accounts`(
        `firstname`,
        `lastname`,
        `cnum`,
        `bday`,
        `age`,
        `address`,
        `email`,
        `password`,
        `type`,
        `department`
    )
    VALUES(
            '$fname',
            '$lname',
            '$contactNumberWithAreaCode',
            '$bday',
            -1,
            '$address',
            '$email',
            '$hashed_password',
            $account_type,
            '$department'
        )
    ");

    if ($register_account) {
        message(true, "New account created successfully.");
    } else {
        message(false, "Account registration failed. ");
    }
}
