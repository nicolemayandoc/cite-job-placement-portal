$(document).ready(() => {
    $(".frm_new_account").on("submit",(e) => {
        e.preventDefault();
        var data = $('.frm_new_account').serializeArray()
        $.ajax({
            url : "./routes/new_account.php",
            method: "post",
            data : data,
            success: (res) => {
                console.log(res)
                if(res.success){
                    Swal.fire(
                        'Success',
                        `${res.message}`,
                        'success'
                    ) 
                    setTimeout(() => {
                        window.location.reload()
                    }, 500);
                }else{
                    Swal.fire(
                        'Failed',
                        `${res.message}`,
                        'error'
                    )
                }
            }
        });
        
    });
});