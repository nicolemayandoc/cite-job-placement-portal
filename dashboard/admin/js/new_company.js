$(document).ready(() => {
  $(".frm_new_company").on("submit", (e) => {
    e.preventDefault()

    const btnSubmit = $(".frm_new_company > button")
    btnSubmit.prop("disabled", true)
    btnSubmit.attr(
      "class",
      "bg-violet-400 text-white mt-[1rem] h-[50px] rounded-[0.5rem] border-none cursor-none"
    )

    var data = $(".frm_new_company").serializeArray()
    $.ajax({
      url: "./routes/new_company.php",
      method: "post",
      data: data,
      success: (res) => {
        console.log(res)
        if (res.success) {
          Swal.fire("Success", `${res.message}`, "success")
          setTimeout(() => {
            window.location.href = "?page=hire&sub=company"
          }, 500)
        } else {
          Swal.fire("Failed", `${res.message}`, "error")
        }
      },
      complete: () => {
        btnSubmit.prop("disabled", false)
        btnSubmit.attr("class", "button btn_submit")
      },
    })
  })
})
