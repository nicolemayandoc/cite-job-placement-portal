$(document).ready(() => {
  $(".post_job").on("submit", (e) => {
    e.preventDefault()
    $(".btn_submit").prop("disabled", true)
    var data = $(".post_job").serializeArray()
    console.log("submitted")
    $.ajax({
      url: "./routes/post_job.php",
      method: "post",
      data: data,
      success: (res) => {
        console.log(res)
        if (res.success) {
          Swal.fire("Success", `${res.message}`, "success")

          setTimeout(() => {
            window.location.href = "?page=hire&sub=jobs"
          }, 500)
        } else {
          Swal.fire("Failed", `${res.message}`, "error")
          $(".btn_submit").prop("disabled", false)
        }
      },
      error: (jqXHR, arg2, arg3) => {
        console.log("jqXHR:", jqXHR)
        console.log("arg2:", arg2)
        console.log("arg3:", arg3)
      }
    })
  })
})
