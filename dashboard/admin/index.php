<?php
session_start();
require_once '../../config.php';
require_once '../../functions.php';
require_once '../../session.php';

setlocale(LC_MONETARY, "en_US");

if ($islogin) {
  if ($u_type == 1) {
    $page = (form("page")) ? value("page") : "dashboard";

    $load_jobs = mysqli_query($con, "SELECT * FROM `tbl_jobs`");
    $count_jobs = mysqli_num_rows($load_jobs);

    $load_company = mysqli_query($con, "SELECT * FROM `tbl_company`");
    $count_company = mysqli_num_rows($load_company);

    if (form("manage")) {
      $update = true;
      $id = mysqli_value($con, "manage");
      if (is_numeric($id)) {
        $manage_Job = mysqli_query($con, "SELECT * FROM `tbl_jobs`");
        if (hasResult($manage_Job)) {
          $data = mysqli_fetch_assoc($manage_Job);
        } else {
          $update = false;
        }
      } else {
        $update = false;
      }
    } else {
      $update = false;
    }

    if (form("filter") && value("sub") == "applicants") {
      $filter = strtolower(mysqli_value($con, "filter"));
      if ($filter == "pending") {
        $load_applicants = mysqli_query($con, "SELECT tbl_applicants.id, tbl_applicants.applicantsid, tbl_accounts.firstname, tbl_accounts.lastname, tbl_accounts.cnum, tbl_accounts.bday, tbl_accounts.address, tbl_accounts.age, tbl_resume.path AS 'resume', tbl_applicants.companyid, tbl_applicants.jobid, tbl_jobs.j_name, tbl_applicants.status, tbl_applicants.created_at FROM tbl_applicants INNER JOIN tbl_accounts ON tbl_accounts.id = tbl_applicants.applicantsid INNER JOIN tbl_resume ON tbl_resume.userid = tbl_applicants.applicantsid INNER JOIN tbl_jobs ON tbl_jobs.id = tbl_applicants.jobid WHERE tbl_applicants.status = 1");
      } elseif ($filter == "hired") {
        $load_applicants = mysqli_query($con, "SELECT tbl_applicants.id, tbl_applicants.applicantsid, tbl_accounts.firstname, tbl_accounts.lastname, tbl_accounts.cnum, tbl_accounts.bday, tbl_accounts.address, tbl_accounts.age, tbl_resume.path AS 'resume', tbl_applicants.companyid, tbl_applicants.jobid, tbl_jobs.j_name, tbl_applicants.status, tbl_applicants.created_at FROM tbl_applicants INNER JOIN tbl_accounts ON tbl_accounts.id = tbl_applicants.applicantsid INNER JOIN tbl_resume ON tbl_resume.userid = tbl_applicants.applicantsid INNER JOIN tbl_jobs ON tbl_jobs.id = tbl_applicants.jobid WHERE tbl_applicants.status = 2");
      } elseif ($filter == "declined") {
        $load_applicants = mysqli_query($con, "SELECT tbl_applicants.id, tbl_applicants.applicantsid, tbl_accounts.firstname, tbl_accounts.lastname, tbl_accounts.cnum, tbl_accounts.bday, tbl_accounts.address, tbl_accounts.age, tbl_resume.path AS 'resume', tbl_applicants.companyid, tbl_applicants.jobid, tbl_jobs.j_name, tbl_applicants.status, tbl_applicants.created_at FROM tbl_applicants INNER JOIN tbl_accounts ON tbl_accounts.id = tbl_applicants.applicantsid INNER JOIN tbl_resume ON tbl_resume.userid = tbl_applicants.applicantsid INNER JOIN tbl_jobs ON tbl_jobs.id = tbl_applicants.jobid WHERE tbl_applicants.status = 3");
      } else {
        $load_applicants = mysqli_query($con, "SELECT tbl_applicants.id, tbl_applicants.applicantsid, tbl_accounts.firstname, tbl_accounts.lastname, tbl_accounts.cnum, tbl_accounts.bday, tbl_accounts.address, tbl_accounts.age, tbl_resume.path AS 'resume', tbl_applicants.companyid, tbl_applicants.jobid, tbl_jobs.j_name, tbl_applicants.status, tbl_applicants.created_at FROM tbl_applicants INNER JOIN tbl_accounts ON tbl_accounts.id = tbl_applicants.applicantsid INNER JOIN tbl_resume ON tbl_resume.userid = tbl_applicants.applicantsid INNER JOIN tbl_jobs ON tbl_jobs.id = tbl_applicants.jobid");
      }
    } else {
      $filter = "all";
      $load_applicants = mysqli_query($con, "SELECT tbl_applicants.id, tbl_applicants.applicantsid, tbl_accounts.firstname, tbl_accounts.lastname, tbl_accounts.cnum, tbl_accounts.bday, tbl_accounts.address, tbl_accounts.age, tbl_resume.path AS 'resume', tbl_applicants.companyid, tbl_applicants.jobid, tbl_jobs.j_name, tbl_applicants.status, tbl_applicants.created_at FROM tbl_applicants INNER JOIN tbl_accounts ON tbl_accounts.id = tbl_applicants.applicantsid INNER JOIN tbl_resume ON tbl_resume.userid = tbl_applicants.applicantsid INNER JOIN tbl_jobs ON tbl_jobs.id = tbl_applicants.jobid");
    }

    $count_applicants = mysqli_num_rows($load_applicants);

    if (form("view") && value("sub") == "company") {
      $company_id = mysqli_value($con, "view");

      if (is_numeric($company_id)) {
        $validate_company = mysqli_query($con, "
        SELECT
          tbl_accounts.firstname,
          tbl_accounts.lastname,
          tbl_company.*
        FROM
          `tbl_company`
        INNER JOIN tbl_accounts ON
          tbl_accounts.id = tbl_company.userid
        WHERE
          tbl_company.id =  $company_id
        ");
        if (hasResult($validate_company)) {
          $company_action = "view";
          $data = mysqli_fetch_assoc($validate_company);

          $publisher_name = $data["firstname"] . " " . $data["lastname"];
          $publisher_firstname = $data["firstname"];
          $publisher_lastname = $data["lastname"];
          $company_name = $data["c_name"];
          $company_address = $data["c_address"];
          $company_cnum = $data["c_cnum"];
          $company_position = $data["c_position"];
          $company_created_at = $data["created_at"];


          $load_company_reports = mysqli_query($con, "
            SELECT
              tbl_accounts.firstname,
              tbl_accounts.lastname,
              tbl_company_reports.*
            FROM
              `tbl_company_reports`
            INNER JOIN tbl_accounts ON
              tbl_accounts.id = tbl_company_reports.reported_by
            WHERE
              `company_id` = $company_id
          ");
        } else {
          $company_action = "";
        }
      } else {
        $company_action = "";
      }
    } elseif (form("add") && value("sub") == "company") {
      $company_action = "add";
    } elseif (form("userid")) {
      $company_action = "selected_company";
      $userid = mysqli_value($con, "userid");
      if (is_numeric($userid)) {
        $load_user_info = mysqli_query($con, "SELECT * FROM `tbl_accounts` WHERE `id` = $userid AND `type` = 2");
        if (hasResult($load_user_info)) {
          $data = mysqli_fetch_assoc($load_user_info);
        } else {
          $company_action = "";
        }
      } else {
        $company_action = "";
      }
    } else {
      $company_action = "";
    }


    if (form("filter") && value("sub") == "list" && value("page") == "accounts") {
      $filter = strtolower(mysqli_value($con, "filter"));

      if ($filter == "all") {
        $load_accounts = mysqli_query($con, "SELECT * FROM `tbl_accounts`");
      } else {
        function filter($filter)
        {
          if ($filter == "admin") {
            return 1;
          }
          if ($filter == "company") {
            return 2;
          }
          if ($filter == "client") {
            return 3;
          }
        }

        $account_type = filter($filter);

        $load_accounts = mysqli_query($con, "SELECT * FROM `tbl_accounts` WHERE type = $account_type");
      }
    } else {
      $filter = "all";
      $load_accounts = mysqli_query($con, "SELECT * FROM `tbl_accounts`");
    }

    if (value("page") == "hire" && value("sub") == "jobs") {
      if (form("add")) {
        $jobs_action = "add";
      } elseif (form("company")) {
        $jobs_action = "company";

        $selected_company_id = value("company");

        if (is_numeric($selected_company_id)) {
          $company_id = $selected_company_id;
          $validate_company = mysqli_query($con, "
          SELECT
            tbl_accounts.firstname,
            tbl_accounts.lastname,
            tbl_company.*
          FROM
            `tbl_company`
          INNER JOIN tbl_accounts ON
            tbl_accounts.id = tbl_company.userid
          WHERE
            tbl_company.userid =  $company_id
          ");
          if (hasResult($validate_company)) {
            $company_action = "view";
            $data = mysqli_fetch_assoc($validate_company);

            $publisher_name = $data["firstname"] . " " . $data["lastname"];
            $publisher_firstname = $data["firstname"];
            $publisher_lastname = $data["lastname"];
            $company_name = $data["c_name"];
            $company_address = $data["c_address"];
            $company_cnum = $data["c_cnum"];
            $company_position = $data["c_position"];
            $company_created_at = $data["created_at"];
          } else {
            $jobs_action = "";
          }
        } else {
          $jobs_action = "";
        }
      } elseif (form("manage")) {
        $job_id = value("manage");
        if (is_numeric($job_id)) {
          $job_query_validate = mysqli_query($con, "
            SELECT
              tbl_accounts.firstname,
              tbl_accounts.lastname,
              tbl_company.c_name,
              tbl_company.c_address,
              tbl_jobs.*
            FROM
              `tbl_jobs`
            INNER JOIN tbl_accounts ON
              tbl_accounts.id = tbl_jobs.userid
            INNER JOIN tbl_company ON
              tbl_company.userid = tbl_jobs.userid
            WHERE tbl_jobs.id = $job_id
          ");
          if (hasResult($job_query_validate)) {
            $jobs_action = "manage";

            $data = mysqli_fetch_assoc($job_query_validate);

            $company_name = $data["c_name"];
            $company_address  = $data["c_address"];
            $publisher_name = $data["firstname"] . " " . $data["lastname"];
            $publisher_firstname = $data["firstname"];
            $publisher_lastname = $data["lastname"];

            $position_name = $data["j_name"];
            $position_age = $data["j_age"];
            $position_age_max = $data["j_age_max"];
            $age = $data["j_age"];
            $min = $data["j_min"];
            $max = $data["j_max"];
            $description = $data["j_description"];
            $gender = $data["j_gender"];
          } else {
            $jobs_action = "";
          }
        } else {
          $jobs_action = "";
        }
      } else {
        $jobs_action = "";
      }
    }

    if (form("filter")) {
      $filter = value("filter");
    }


    $load_company_accounts = mysqli_query($con, "SELECT * FROM `tbl_accounts` WHERE type = 2");
  } else {
    navigate("../../");
  }
} else {
  navigate("../../");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>DASHBOARD</title>
  <link rel="icon" href="../../assets/logo.png">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous" defer></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11" defer></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="../../header.css">
  <!-- javascript -->
  <script src="js/index.js" defer></script>
  <script src="js/post_job.js" defer></script>
  <script src="js/edit_job.js" defer></script>
  <script src="js/manage_applicants.js" defer></script>
  <script src="js/update_account.js" defer></script>
  <script src="js/update_general.js" defer></script>
  <script src="js/new_company.js" defer></script>
  <script src="js/update_company.js" defer></script>
  <script src="js/delete_company.js" defer></script>
  <script src="js/add_account.js" defer></script>
  <script src="js/update_password.js" defer></script>
  <!-- Import Vue - a declarative UI framework -->
  <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
  <!-- Import Luxon - library for calculating dates -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/luxon/3.1.0/luxon.min.js"></script>
  <!-- Import Tailwind - a framework for writing CSS faster -->
  <script src="https://cdn.tailwindcss.com"></script>
</head>

<body>
  <div class="main" id="app">
    <?php include '../../header.php' ?>

    <div class="body" id="body_page_<?= $page ?>">
      <div class="profile_box" style="display:none">
        <div class="profile_box_header">
          <p class="profile_name">
            <?= $u_fname . " " . $u_lname ?>
          </p>
          <p class="profile_email">
            <?= $u_email ?>
          </p>
        </div>
        <div class="profile_box_body">
          <a href="./?page=profile">Account Information</a>
        </div>
        
      </div>

      <?php if ($page == "dashboard") { ?>
        <h2 class="w-[min(100%,_42rem)] mx-auto mt-6">Hi, <?= $u_fname . " " . $u_lname ?> 👋</h2>
        <div class="dashboard w-[min(100%,_42rem)] mx-auto">
          <div class="box">
            <div class="box_name">
              Open Jobs
              <span class="font-bold">(<?= $count_jobs ?>)</span>
            </div>
            <a href="?page=hire&sub=jobs">
              VIEW
            </a>
          </div>
          <div class="box">
            <div class="box_name">
              Applicants
              <span class="font-bold">(<?= $count_applicants ?>)</span>
            </div>
            <a href="?page=hire&sub=applicants">
              VIEW
            </a>
          </div>
          <div class="box">
            <div class="box_name">
              Companies
              <span class="font-bold">(<?= $count_company ?>)</span>
            </div>
            <a href="?page=hire&sub=company">
              VIEW
            </a>
          </div>
        </div>
      <?php } elseif ($page == "hire") { ?>
        <?php if (form("sub")) { ?>
          <!-- sidebar -->
          <div class="
              bg-white border-r
              [border-right-color:_rgb(204,_204,_204)]
              w-[3rem]
              sm:w-[300px]
              py-4
              sm:pr-4
            ">
            <a href="?page=hire&sub=company" class="
                flex gap-2
                pl-4 sm:pl-8 py-4 mb-2 rounded-r-2xl
                <?=
                (value("sub") == "company")
                  ? '[background-color:_#0e8136] text-white'
                  : 'border border-white hover:[border-color:_#0e8136]'
                ?>
              ">
              <i class="fa-solid fa-building h-6 flex items-center"></i>
              <p class="name hidden sm:block">Companies</p>
            </a>
            <a href="?page=hire&sub=jobs" class="
                flex gap-2
                pl-4 sm:pl-8 py-4 mb-2  rounded-r-2xl
                <?=
                (value("sub") == "jobs")
                  ? '[background-color:_#0e8136] text-white'
                  : 'border border-white hover:[border-color:_#0e8136]'
                ?>
              ">
              <i class="fa fa-briefcase h-6 flex items-center"></i>
              <p class="name hidden sm:block">Jobs</p>
            </a>
            <a href="?page=hire&sub=applicants" class="
                flex gap-2
                pl-4 sm:pl-8 py-4 mb-2 rounded-r-2xl
                <?=
                (value("sub") == "applicants")
                  ? '[background-color:_#0e8136] text-white'
                  : 'border border-white hover:[border-color:_#0e8136]'
                ?>
              ">
              <i class="fa fa-users h-6 flex items-center"></i>
              <p class="name hidden sm:block">Applicants</p>
            </a>
          </div>

          <div class="content">
            <div class="showcase" id="showcase_sub_<?= value("sub") ?>">
              <?php if (value("sub") == "jobs") { ?>
                <?php if ($jobs_action == "add") { ?>
                  <div class="t1">
                    <div class="sm:flex justify-between items-center mb-4">
                      <div>
                        <h3 class="text-lg text-green-700 font-bold">
                          <a href="?page=hire&sub=jobs" class="text-green-700 font-bold hover:underline">Jobs</a>
                          >
                          <a class="text-black">Company List</a>
                        </h3>

                        <i class="mb-3"> First, You need to select a Company</i>
                      </div>
                      <a href="?page=hire&sub=jobs" class="bg-green-700 hover:bg-green-600 text-white font-bold py-2 px-4 rounded inline-flex justify-between items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
                          <path d="M18.293 5.293l-1.414-1.414L12 10.586 7.121 5.707l-1.414 1.414L10.586 12l-4.879 4.879 1.414 1.414L12 13.414l4.879 4.879 1.414-1.414L13.414 12l4.879-4.879z" />
                        </svg>
                        <span>
                          Cancel
                        </span>
                      </a>

                    </div>

                    <div class="container">
                      <div class="
                          w-full
                          [color:_green]
                          p-4 border-b
                          [border-color:_rgb(204, 204, 204)]
                          sm:flex justify-between gap-2
                        ">
                        <div class="title">
                          Company List
                        </div>
                        <div class="flex gap-2">
                          <!-- Search filters and pagination buttons -->
                          <div class="md:flex gap-2">

                            <div>
                              <input v-model="searchTerm" type="text" placeholder=" Search ..." class="border border-gray-300 px-3 py-2 rounded-md w-full sm:w-[12rem] outline-none text-gray-800 focus:ring focus:ring-blue-300/40 focus:border-blue-500" />
                            </div>
                            <div class="flex gap-2">
                              <button type="button" @click="currentPage--" :disabled="!hasPrevPage" class="disabled:text-gray-200">
                                Prev
                              </button>
                              <button type="button" @click="currentPage++" :disabled="!hasNextCompaniesPage" class="disabled:text-gray-200">
                                Next
                              </button>
                            </div>
                          </div>


                        </div>
                      </div>
                      <div class="container_body">
                        <div v-if="isLoading" class="text-center py-2">
                          Loading ...
                        </div>
                        <div v-else-if="visibleCompanies.length === 0" class="py-4">
                          <img src="./assets/empty.png" alt="empty" width="200" class="mx-auto">
                          <p class="text-center">Company not found</p>
                        </div>
                        <a v-else v-for="company in visibleCompanies" :key="company.id" :href="`?page=hire&sub=jobs&company=${company.userid}`" class="box justify-between">
                          <div class="sm:flex sm:flex-col">


                            <p class="name" style="font-weight: 600; color: gray;">{{ company.c_name }}</p>
                            <p class="salary_range" style="font-size: small; font-weight: 400; color: gray;">{{ company.c_address }}</p>
                          </div>


                          <i class="fa fa-angle-right"></i>
                        </a>

                      </div>
                    </div>
                  <?php } elseif ($jobs_action == "company") { ?>
                    <div class="t1 mb-2">


                      <h3 class="text-lg text-green-700 font-bold mb-2">
                        <a href="?page=hire&sub=jobs" class="text-green-700 font-bold hover:underline">Jobs</a>
                        >
                        <a href="?page=hire&sub=jobs&add" class="text-green-700 font-bold hover:underline">Company List</a>
                        >
                        <a class="text-black">Add a Job</a>
                      </h3>

                      <i class="mb-3"> Second, Fill out the job details and post the job </i>
                      <div class="container">

                        <div class="container_title">
                          COMPANY INFORMATION



                        </div>

                        <div class="container_body">
                          <form method="post" style="margin: 1rem" autocomplete="off">
                            <div class="field">
                              <label for="name">Company Name</label>
                              <input type="text" id="name" name="tb_name" placeholder="Name" value="<?= $company_name ?>" style="color: gray;" readonly>
                            </div>
                            <div class="field">
                              <label for="name">Company Address</label>
                              <input type="text" id="name" name="tb_address" placeholder="Address" value="<?= $company_address ?>" style="color: gray;" readonly>
                            </div>
                            <div class="field">
                              <label for="name">Contact number</label>
                              <input type="number" id="name" name="tb_cnum" placeholder="Contact number" value="<?= $company_cnum ?>" style="color: gray;" readonly maxlength="11" oninput="this.value=this.value.slice(0,11)">
                            </div>
                            <div class="field">
                              <label for="name">Owner Name</label>
                              <input type="text" id="name" name="tb_publisher_name" placeholder="Publisher's name" value="<?= $publisher_name ?>" style="color: gray;" readonly>
                            </div>

                            <div class="field">
                              <label for="name">Created at</label>
                              <input type="text" id="name" name="tb_created_at" placeholder="Created at" value="<?= date("M d, Y", strtotime($company_created_at)) ?>" style="color: gray;" disabled>
                            </div>
                          </form>
                        </div>
                      </div>
                      <div class="container mt-3">
                        <div class="container_title">
                          ADD NEW JOB
                        </div>
                        <div class="container_body form_box">
                          <form method="post" class="post_job">
                            <div class="field">

                              <input type="hidden" name="company_id" id="company_id" value="<?= $company_id ?>">
                            </div>
                            <div class="field">
                              <label for="name" style="color: black; font-weight: 500;">Job Title</label>
                              <input type="text" name="position_name" id="position_name" placeholder="e.g. Full Stack Developer">
                            </div>

                            <div class="field">
                              <label for="name" style="color:black; font-weight: 500;">Minimum Age Limit</label>
                              <input type="number" name="position_age" id="position_age" placeholder="e.g. 18" maxlength="2" oninput="this.value=this.value.slice(0,2)">

                            </div>

                            <div class="field">
                              <label for="name" style="color:black; font-weight: 500;">Maximum Age Limit</label>
                              <input type="number" name="position_age_max" id="position_age_max" placeholder="e.g. 64" maxlength="2" oninput="this.value=this.value.slice(0,2)">

                            </div>
                            <div class="field">
                              <label for="gender" style="color: black; font-weight: 500;">Gender</label>
                              <div class="sm:flex">
                                <div class="field_radio">
                                  <input type="radio" name="gender" id="male" checked value="Male" style="margin-right: 5px; width: auto" />
                                  <label for="male" style="margin-right: 10px;">Male</label>
                                </div>
                                <div class="field_radio">
                                  <input type="radio" name="gender" id="female" value="Female" style="margin-right: 5px; width: auto" />
                                  <label for="female" style="margin-right: 10px;">Female</label>
                                </div>
                                <div class="field_radio">
                                  <input type="radio" name="gender" id="both" value="Both" style="margin-right: 5px; width: auto" />
                                  <label for="both" style="margin-right: 10px;">Prefer not to say</label>
                                </div>
                              </div>
                            </div>

                            <div class="field">
                              <label for="name" style="color:black; font-weight: 500;">Minimum Salary</label>
                              <input type="number" name="minimum_salary" id="minimum_salary" placeholder="e.g. 10,000" maxlength="5" oninput="this.value=this.value.slice(0,5)">
                            </div>
                            <div class="field">
                              <label for="name" style="color:black; font-weight: 500;">Maximum Salary</label>
                              <input type="number" name="maximum_salary" id="maximum_salary" placeholder="e.g. 20,000" maxlength="11" oninput="this.value=this.value.slice(0,11)">
                            </div>
                            <!--
                          <div class="field" style="display:none">
                            <select name="currency_symbol" id="currency_symbol">
                              <option value="" selected disabled>Currency symbol</option>
                              <option value="₱">PH Peso</option>
                              <option value="$">US Dollars</option>
                            </select>
                          </div>
                        -->
                            <div class="field">
                              <label for="name" style="color:black; font-weight: 500;">Job Description</label>
                              <textarea name="description" class="description" id="content" placeholder="e.g. someone who can work on both the back-end and front-end of systems."><?= ($update) ? $data["j_description"] : "" ?></textarea>
                            </div>
                            <button class="button btn_submit disabled:bg-violet-300">
                              SAVE
                            </button>
                          </form>
                          <?php if ($update) { ?>
                            <button data-id="<?= ($update) ? $data["id"] : "" ?>" class="btn_delete_job disabled:bg-violet-300">
                              DELETE
                            </button>
                          <?php } ?>
                        </div>
                      </div>
                    <?php } elseif ($jobs_action == "manage") { ?>
                      <div class="t1 mb-2">


                        <h3 class="text-lg text-green-700 font-bold mb-2">
                          <a href="?page=hire&sub=jobs" class="text-green-700 font-bold hover:underline">Jobs</a>
                          >

                          <a class="text-black">Edit a Job</a>
                        </h3>


                        <div class="container">
                          <div class="container_title">
                            COMPANY INFORMATION

                          </div>

                          <div class="container_body">
                            <form method="post" style="margin: 1rem" autocomplete="off">
                              <div class="field">
                                <label for="name">Company Name</label>
                                <input type="text" id="name" name="tb_name" placeholder="Name" value="<?= $company_name ?>" style="color: gray;" disabled>
                              </div>
                              <div class="field">
                                <label for="name">Company Address</label>
                                <input type="text" id="name" name="tb_address" placeholder="Address" value="<?= $company_address ?>" style="color: gray;" disabled>
                              </div>
                              <div class="field">
                                <label for="name">Owner Name</label>
                                <input type="text" id="name" name="tb_publisher_name" placeholder="Publisher's name" value="<?= $publisher_name ?>" style="color: gray;" disabled>
                              </div>
                            </form>

                          </div>
                        </div>
                        <div class="container mt-4">
                          <div class="container_title">

                            MANAGE SELECTED JOB
                          </div>
                          <div class="container_body form_box">
                            <form method="post" class="edit_job">
                              <div class="field">
                                <input type="hidden" name="id" value="<?= $data["id"] ?>" required readonly>
                              </div>
                              <div class="field">
                                <label for="position_name" style="color: black; font-weight: 500;">Job Title</label>
                                <input type="text" name="position_name" id="position_name" placeholder="Full Stack Developer" value="<?= $position_name ?>">
                              </div>
                              <div class="field">
                                <label for="position_age" style="color:black; font-weight: 500;">Minimum Age </label>
                                <input type="number" name="position_age" id="position_age" placeholder="e.g. 18" value="<?= $position_age ?>" maxlength="2" oninput="this.value=this.value.slice(0,2)">
                              </div>
                              <div class="field">
                                <label for="position_age_max" style="color:black; font-weight: 500;">Maximum Age </label>
                                <input type="number" name="position_age_max" id="position_age_max" placeholder="e.g. 64" value="<?= $position_age_max ?>" maxlength="2" oninput="this.value=this.value.slice(0,2)">
                              </div>
                              <div class="field">

                                <label for="gender" style="color: black; font-weight: 500;">Gender</label>
                                <div class="field_radio_group" style="display: flex; align-items: center;">
                                  <div class="field_radio" style="display: flex; align-items: center;">
                                    <input type="radio" name="gender" id="Male" <?= ($gender == "Male") ? "checked" : "" ?> value="Male">
                                    <label for="Male" style="margin-right: 10px;">Male</label>
                                  </div>
                                  <div class="field_radio" style="display: flex; align-items: center;">
                                    <input type="radio" name="gender" id="Female" <?= ($gender == "Female") ? "checked" : "" ?> value="Female">
                                    <label for="Female" style="margin-right: 10px;">Female</label>
                                  </div>
                                  <div class="field_radio" style="display: flex; align-items: center;">
                                    <input type="radio" name="gender" id="Both" <?= ($gender == "Both") ? "checked" : "" ?> value="Both">
                                    <label for="Both" style="margin-right: 10px;">Prefer not to say</label>
                                  </div>
                                </div>
                              </div>

                              <div class="field">
                                <label for="minimum_salary" style="color:black; font-weight: 500;">Minimum Salary</label>
                                <input type="number" name="minimum_salary" id="minimum_salary" placeholder="e.g. 10,000" value="<?= $min ?>" maxlength="5" oninput="this.value=this.value.slice(0,5)" required>
                              </div>
                              <div class="field">
                                <label for="maximum_salary" style="color:black; font-weight: 500;">Maximum Salary</label>
                                <input type="number" name="maximum_salary" id="maximum_salary" placeholder="e.g. 20,000" value="<?= $max ?>" maxlength="11" oninput="this.value=this.value.slice(0,11)">
                              </div>
                              <!--
 <div class="field" style="display:none">
   <select name="currency_symbol" id="currency_symbol">
     <option value="" selected disabled>Currency symbol</option>
     <option value="₱">PH Peso</option>
     <option value="$">US Dollars</option>
   </select>
 </div>
 -->
                              <div class="field">
                                <label for="description" style="color:black; font-weight: 500;">Job Description</label>
                                <textarea name="description" class="description" id="content" placeholder="Description" style="padding: 8px;"><?= ($update) ? $data["j_description"] : "" ?></textarea>
                              </div>
                              <button class="button btn_submit">
                                SAVE
                              </button>

                            </form>
                            <?php if ($update) { ?>
                              <button data-id="<?= ($update) ? $data["id"] : "" ?>" class="button btn_delete_job">
                                DELETE
                              </button>
                            <?php } ?>
                          </div>
                        </div>




                      <?php } else { ?>
                        <div class="t1">
                          <div class="grid sm:flex justify-between items-center mb-4">
                            <div class="mb-3 sm:mb-0">
                              <h3 class="text-lg font-bold">Jobs</h3>
                              <p class="text-gray-500 text-sm italic">A list of all the jobs.</p>
                            </div>
                            <a href="./?page=hire&sub=jobs&add" class="bg-green-600 hover:bg-green-600 text-white font-bold py-2 px-4 rounded">
                              Add Job
                            </a>
                          </div>
                          <div class="container">
                            <div class="sm:flex gap-2 justify-between items-center [color:_green] p-4 border-b [border-color:_rgb(204, 204, 204)]">
                              <div class="title">
                                Jobs
                              </div>
                              <div class="sm:flex gap-2">
                                <div>
                                  <input v-model="searchTerm" type="text" placeholder=" Search ..." class="w-full border border-gray-300 px-3 py-2 rounded-md w-[12rem] outline-none text-gray-800 focus:ring focus:ring-blue-300/40 focus:border-blue-500" />
                                </div>
                                <div class="flex gap-2">
                                  <button type="button" @click="currentPage--" :disabled="!hasPrevPage" class="disabled:text-gray-200">
                                    Prev
                                  </button>
                                  <button type="button" @click="currentPage++" :disabled="!hasNextJobsPage" class="disabled:text-gray-200">
                                    Next
                                  </button>
                                </div>
                              </div>
                            </div>
                            <div class="container_body">
                              <div v-if="isLoading" class="text-center py-2">
                                Loading ...
                              </div>
                              <div v-else-if="visibleJobs.length === 0" class="py-4">
                                <img src="./assets/empty.png" alt="empty" width="200" class="mx-auto">
                                <p class="text-center">No jobs found</p>
                              </div>
                              <a v-else v-for="job in visibleJobs" :key="job.id" :href="`?page=hire&sub=jobs&manage=${job.id}`" class="box justify-between">
                                <div class="sm:flex items-center">
                                  <div class="sm:flex sm:flex-col">

                                    <p class="name" style="font-weight: 500;">{{ job.j_name }}</p>
                                    <p class="salary_range">
                                      {{ job.j_currency_symbol }}
                                      {{ job.j_min }}
                                      -
                                      {{ job.j_currency_symbol }}
                                      {{ job.j_max }}
                                    </p>
                                    <p class="posted_at">
                                      {{ getFormattedDate(job.j_created_at) }}
                                    </p>
                                  </div>
                                </div>
                                <i class="fa fa-angle-right"></i>
                              </a>
                            </div>
                          </div>
                        <?php } ?>
                      <?php } elseif (value("sub") == "company") { ?>
                        <?php if ($company_action == "view") { ?>

                          <div class="t1">
                            <div class="flex justify-between items-center mb-4">
                              <div>
                                <h3 class="text-lg text-green-700 font-bold mb-2">
                                  <a href="?page=hire&sub=company" class="text-green-700 font-bold hover:text-green-500">Company</a>
                                  >
                                  <a class="text-black">Edit a Company</a>


                                </h3>


                              </div>

                            </div>
                            <div class="container">
                              <div class="p-4 grid md:flex [color:_rgb(45,_174,_110)] border-b [border-color:_rgb(204, 204, 204)] justify-between">
                                <div class="text-gray-900 mb-1 md:mb-0">
                                  Company
                                </div>
                                <div class="filters grid md:flex gap-x-4 flex-col md:flex-row">
                                  <?php if (isset($_GET["action"])) : ?>
                                    <a href="./?page=hire&sub=company&view=<?= $company_id ?>&action=overview" class="py-2 px-4 text-white bg-blue-600 hover:bg-blue-700 rounded mb-2 md:mb-0 <?php if ($_GET['action'] == 'overview') echo 'active'; ?>">
                                      Overview
                                    </a>
                                    <a href="./?page=hire&sub=company&view=<?= $company_id ?>&action=reports" class="py-2 px-4 text-black bg-yellow-400 hover:bg-yellow-500 rounded mb-2 md:mb-0 <?php if ($_GET['action'] == 'reports') echo 'active'; ?>">
                                      Reports
                                    </a>
                                    <a href="./?page=hire&sub=company&view=<?= $company_id ?>&action=delete" class="py-2 px-4 text-white bg-red-600 hover:bg-red-700 rounded mb-2 md:mb-0 <?php if ($_GET['action'] == 'delete') echo 'active'; ?>">
                                      Delete Company
                                    </a>
                                  <?php endif ?>
                                </div>
                              </div>


                              <div class="container_body">
                                <?php if (form("action")) {
                                  $action = value("action");
                                ?>
                                  <?php if ($action == "overview") { ?>
                                    <form method="post" style="margin: 1rem" autocomplete="off" class="frm_profile_company">
                                      <div class="field">
                                        <label for="name" style="color: black; font-weight: 500;">Company name</label>
                                        <input type="hidden" id="name" name="tb_id" placeholder="e.g. PHINMA Corporation" value="<?= $_GET["view"] ?>">
                                        <input type="text" id="name" name="tb_name" placeholder="e.g. PHINMA Corporation" value="<?= $company_name ?>">
                                      </div>
                                      <div class="field">
                                        <label for="name" style="color: black; font-weight: 500;">Company address</label>
                                        <input type="text" id="name" name="tb_address" placeholder="e.g. 39 Plaza Dr, Rockwell Center, Makati, 1200 Metro Manila " value="<?= $company_address ?>">
                                      </div>
                                      <div class="field">
                                        <label for="name" style="color: black; font-weight: 500;">Business phone</label>
                                        <input type="number" id="name" name="tb_cnum" placeholder="e.g. 9XXX-XXX-YYY" value="<?= $company_cnum ?>" maxlength="10" oninput="this.value=this.value.slice(0,10)">
                                      </div>
                                      <div class="field">
                                        <label for="tb_publisher_lastname" style="color: black; font-weight: 500;">Owner First name</label>
                                        <input type="text" id="name" name="tb_publisher_firstname" placeholder="e.g. Jane" value="<?= $publisher_firstname ?>">
                                      </div>
                                      <div class="field">
                                        <label for="tb_publisher_lastname" style="color: black; font-weight: 500;">Owner Last name</label>
                                        <input type="text" id="name" name="tb_publisher_lastname" placeholder="e.g. Doe" value="<?= $publisher_lastname ?>">
                                      </div>


                                      <div class="field">
                                        <label for="name" style="color: gray; font-weight: 500;">Created at</label>
                                        <input type="date" id="name" name="tb_created_at" placeholder="Created at" value="<?= date("Y-m-d", strtotime($company_created_at)) ?>" style="color: gray;" readonly>
                                      </div>


                                      <button class="button btn_submit">
                                        UPDATE
                                      </button>
                                    <?php } elseif ($action == "reports") { ?>
                                      <?php if (hasResult($load_company_reports)) { ?>
                                        <?php while ($row = mysqli_fetch_assoc($load_company_reports)) { ?>
                                          <div class="box">
                                            <div class="sm:flex items-center gap-3">
                                              <div>
                                                <p style="font-size: 2rem; line-height: 2rem; text-transform: uppercase; margin-bottom: .3rem"><?= $row["firstname"] . " " . $row["lastname"] ?></p>
                                                <p style="font-size: .8rem; margin-bottom: 0; color: gray;"><?= date("M d,Y", strtotime($row["created_at"])) ?></p>


                                                <p style="font-size: 1rem; margin-top: .3rem"><?= $row['message']; ?></p>
                                              </div>

                                            </div>
                                            <!-- <i class="fa fa-angle-right"></i> -->
                                          </div>

                                        <?php } ?>
                                      <?php } else { ?>
                                        <div class="empty_box">
                                          <img src="./assets/empty.png" alt="empty" width="200">
                                          <p>No reports</p>
                                        </div>
                                      <?php } ?>
                                    <?php } elseif ($action == "delete") { ?>
                                      <div class="empty_box">
                                        <p>
                                          Delete <strong><?= $company_name ?></strong> Company?
                                        </p>

                                        <p>
                                          <span style="font-weight: 500; color: red;">WARNING :</span> This action cannot be undone.
                                        </p>

                                        <button class="btn_delete btn_delete_company" data-id="<?= $company_id ?>"> DELETE</button>
                                      </div>
                                    <?php } else {
                                    navigate("./?page=hire&sub=company&view=$company_id&action=overview");
                                  } ?>
                                  <?php } else {
                                  navigate("./?page=hire&sub=company&view=$company_id&action=overview");
                                } ?>
                              </div>
                            </div>

                          <?php } elseif ($company_action == "add") { ?>

                            <div class="t1">
                              <div class="sm:flex justify-between items-center mb-4">
                                <div class="mb-3 sm:mb-0">
                                  <h3 class="text-lg text-green-700 font-bold mb-2">
                                    <a href="?page=hire&sub=company" class="text-green-700 font-bold hover:text-green-500">Company</a>
                                    >
                                    <a class="text-black">Select a Company</a>
                                  </h3>
                                  <i> 1.) You need to select a Company Account</i>
                                </div>
                                <a href="?page=hire&sub=company" class="bg-gray-600 hover:bg-gray-500 text-white font-bold py-2 px-4 rounded inline-flex justify-between items-center">
                                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
                                    <path d="M18.293 5.293l-1.414-1.414L12 10.586 7.121 5.707l-1.414 1.414L10.586 12l-4.879 4.879 1.414 1.414L12 13.414l4.879 4.879 1.414-1.414L13.414 12l4.879-4.879z" />
                                  </svg>
                                  <div>
                                    Cancel
                                  </div>
                                </a>
                              </div>
                              <div class="container">
                                <div class="[color:_rgb(45, 174, 110)] p-4 [border-color:_rgb(204, 204, 204)] md:flex gap-2 justify-between items-center">
                                  <div class="mb-1 md:mb-0">
                                    List of Accounts
                                  </div>
                                  <div class="sm:flex gap-2">
                                    <div>
                                      <input v-model="searchTerm" type="text" placeholder=" Search ..." class="w-full border border-gray-300 px-3 py-2 rounded-md w-[12rem] outline-none text-gray-800 focus:ring focus:ring-blue-300/40 focus:border-blue-500" />
                                    </div>
                                    <div class="flex gap-2">
                                      <button type="button" @click="currentPage--" :disabled="!hasPrevPage" class="disabled:text-gray-200">
                                        Prev
                                      </button>
                                      <button type="button" @click="currentPage++" :disabled="!hasNextJobsPage" class="disabled:text-gray-200">
                                        Next
                                      </button>
                                    </div>
                                  </div>
                                </div>
                                <div class="container_body border">
                                  <div v-if="isLoading" class="text-center py-2">
                                    Loading ...
                                  </div>
                                  <div v-else-if="visibleCompanyAccounts.length === 0" class="py-4">
                                    <img src="./assets/empty.png" alt="empty" width="200" class="mx-auto">
                                    <p class="text-center">No company accounts found</p>
                                  </div>
                                  <a v-else v-for="companyAccount in visibleCompanyAccounts" :key="companyAccount.id" :href="`./?page=hire&sub=company&userid=${companyAccount.id}`" class="box justify-between">
                                    <div class="">
                                      <p class="s_name">
                                        <span class="label">Full name : </span>
                                        {{ companyAccount.firstname }} {{ companyAccount.lastname }}
                                      </p>
                                      <p class="address">
                                        <span class="label">Address : </span>
                                        {{ companyAccount.address }}
                                      </p>
                                      <p class="posted_at">
                                        <span class="label">Created at : </span>
                                        {{ getFormattedDate(companyAccount.created_at) }}
                                      </p>
                                    </div>
                                    <i class="fa fa-angle-right"></i>
                                  </a>
                                </div>
                              </div>

                            <?php } elseif ($company_action == "selected_company") { ?>

                              <div class="t1">
                                <div class="flex justify-between items-center mb-4">
                                  <div>

                                    <h3 class="text-lg text-green-700 font-bold mb-2">
                                      <a href="?page=hire&sub=company" class="text-green-700 font-bold hover:text-green-500">Company</a>

                                      >
                                      <a href="?page=hire&sub=company&add" class="text-green-700 font-bold hover:text-green-500">Select a Company</a>
                                      > <a class="text-black">Add new company</a>
                                    </h3>
                                    <i> 2.) FIll out the Company information.</i>
                                  </div>


                                </div>
                                <div class="container">
                                  <div class="container_title flex justify-between items-center">
                                    Company List

                                  </div>

                                  <div class="container_body">
                                    <form method="post" style="margin: 1rem" autocomplete="off" class="frm_new_company">
                                      <div class="divider">
                                        <p>OWNER(S) DETAILS</p>
                                        <span></span>
                                      </div>
                                      <div class="field">
                                        <input type="hidden" id="u_id" name="u_id" placeholder="User id" value="<?= $data["id"] ?>" readonly>
                                      </div>
                                      <div class="field">
                                        <label for="name">Owner Name</label>
                                        <input type="text" id="name" name="name" placeholder="Name" value="<?= $data["firstname"] ?> <?= $data["lastname"] ?>" disabled>
                                      </div>
                                      <div class="field">
                                        <label for="cnum">Contact number</label>
                                        <input type="text" id="cnum" name="cnum" placeholder="Contact number" value="<?= $data["cnum"] ?>" disabled maxlength="11" oninput="this.value=this.value.slice(0,11)">
                                      </div>
                                      <div class="field">
                                        <label for="address">Address</label>
                                        <input type="text" id="address" name="address" placeholder="Address" value="<?= $data["address"]; ?>" disabled>
                                      </div>
                                      <div class="divider">
                                        <p>COMPANY INFORMATION</p>
                                        <span></span>

                                      </div>
                                      <div class="field">
                                        <label for="c_name" style="color:black; font-weight: 500;">Company name</label>


                                        <input type="text" id="c_name" name="company_name" placeholder="e.g. PHINMA Corporation">
                                      </div>
                                      <div class="field">
                                        <label for="c_address" style="color:black; font-weight: 500;">Company address</label>
                                        <input type="text" id="c_address" name="company_address" placeholder="e.g. 39 Plaza Dr, Rockwell Center, Makati, 1200 Metro Manila">
                                      </div>
                                      <div class="field">
                                        <label for="c_department" style="color:black; font-weight: 500;">Department</label>
                                        <input type="text" id="c_department" name="company_department" placeholder="e.g. CITE">
                                      </div>
                                      <div class="field">
                                        <label for="c_cnum" style="color:black; font-weight: 500;">Business phone</label>
                                        <input type="number" id="c_cnum" name="company_cnum" placeholder="e.g. 09XXX-XXX-YYY" maxlength="11" oninput="this.value=this.value.slice(0,11)">
                                      </div>
                                      <button class="button btn_submit">
                                        SAVE
                                      </button>
                                    </form>
                                  </div>
                                </div>
                              <?php } else { ?>

                                <div class="t1">
                                  <div class="grid sm:flex justify-between items-center mb-4">
                                    <div class="mb-3 sm:mb-0">
                                      <h3 class="text-lg font-bold">Company</h3>
                                      <p class="text-gray-500 text-sm italic">List of all companies.</p>
                                    </div>
                                    <a href="./?page=hire&sub=company&add" class="bg-green-600 hover:bg-green-700 text-white font-bold py-2 px-4 rounded">
                                      Add Company
                                    </a>

                                  </div>
                                  <div class="container">
                                    <div class="container_title">
                                      <div class="lg:text-xl w-full lg:flex items-center justify-between">
                                        <div class="text-lg lg:text-xl mb-1 lg:mb-0">Company List</div>
                                        <!-- Search filters and pagination buttons -->
                                        <div class="md:flex gap-2">
                                          <div>
                                            <input v-model="searchTerm" type="text" placeholder="Search ..." class="
                                                w-full
                                                sm:w-56
                                                border border-gray-300
                                                px-3 py-2 rounded-md w-[12rem]
                                                outline-none text-gray-800
                                                focus:ring focus:ring-blue-300/40 focus:border-blue-500
                                              " />
                                          </div>
                                          <div class="flex gap-2">
                                            <button type="button" @click="currentPage--" :disabled="!hasPrevPage" class="disabled:text-gray-200">
                                              Prev
                                            </button>
                                            <button type="button" @click="currentPage++" :disabled="!hasNextCompaniesPage" class="disabled:text-gray-200">
                                              Next
                                            </button>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="filter">
                                        <a href="./?page=hire&sub=company&add"></a>
                                      </div>
                                    </div>
                                    <div class="container_body">
                                      <div v-if="isLoading" class="text-center py-2">
                                        Loading ...
                                      </div>
                                      <div v-else-if="visibleCompanies.length === 0" class="py-4">
                                        <img src="./assets/empty.png" alt="empty" width="200" class="mx-auto">
                                        <p class="text-center">Company not found</p>
                                      </div>
                                      <a v-else v-for="company in visibleCompanies" :key="company.id" :href="`?page=hire&sub=company&view=${company.id}`" class="box justify-between">
                                        <div class="sm:flex items-center">
                                          <div class="sm:flex sm:flex-col">
                                            <p class="name sm:text-2xl" style="font-weight: 600; color: black;">{{ company.c_name }}</p>
                                            <p class="salary_range" style="font-size: small; font-weight: 400; color: gray;">{{ company.c_address }}</p>
                                          </div>
                                        </div>


                                        <i class="fa fa-angle-right"></i>
                                      </a>
                                    </div>
                                  </div>
                                <?php } ?>
                              <?php } elseif (value("sub") == "applicants") { ?>
                                <div class="t1">
                                  <div class="lg:flex justify-between">
                                    <div class="gap-3 justify-between items-center">
                                      <h3 class="text-lg font-bold">Applicants</h3>
                                      <p class="text-gray-500 text-sm italic mb-4"> List of all applicants in different positions.</p>
                                    </div>
                                    <div class="mb-3">
                                      <div>
                                        <input v-model="searchTerm" type="text" placeholder=" Search ..." class="w-full border border-gray-300 px-3 py-2 rounded-md w-[12rem] outline-none text-gray-800 focus:ring focus:ring-blue-300/40 focus:border-blue-500" />
                                      </div>
                                      <div class="flex justify-end gap-2">
                                        <button type="button" @click="currentPage--" :disabled="!hasPrevPage" class="disabled:text-gray-200">
                                          Prev
                                        </button>
                                        <button type="button" @click="currentPage++" :disabled="!hasNextApplicantsPage" class="disabled:text-gray-200">
                                          Next
                                        </button>
                                      </div>
                                    </div>
                                  </div>



                                  <div class="container">
                                    <div class="md:flex justify-between items-center p-4 border-b border-green-500 border-opacity-75">
                                      <div class="mb-1 sm:mb-0">Applicants</div>
                                      <div class="filter flex flex-wrap md:flex-nowrap">
                                        <button type="button" :class="selectedApplicantsTab === 'all' ? 'bg-green-600 text-white' : 'bg-white text-black border-gray-300'" @click="onClickApplicantsTab('all')" class="flex-1 border-r-2 border-gray-300 border-l-2 md:rounded-l-lg rounded-r-none w-full md:w-auto pr-2 py-2 px-4">
                                          ALL
                                        </button>
                                        <button type="button" :class="selectedApplicantsTab === 'pending' ? 'bg-green-600 text-white' : 'bg-white text-black border-gray-300'" @click="onClickApplicantsTab('pending')" class="flex-1 border-r-2 border-gray-300 border-l-2 w-full md:w-auto pr-2 py-2 px-4">
                                          PENDING
                                        </button>
                                        <button type="button" :class="selectedApplicantsTab === 'hired' ? 'bg-green-600 text-white' : 'bg-white text-black border-gray-300'" @click="onClickApplicantsTab('hired')" class="flex-1 border-r-2 border-gray-300 border-l-2 w-full md:w-auto pr-2 py-2 px-4">
                                          HIRED
                                        </button>
                                        <button type="button" :class="selectedApplicantsTab === 'declined' ? 'bg-green-600 text-white' : 'bg-white text-black'" @click="onClickApplicantsTab('declined')" class="flex-1 md:rounded-r-lg rounded-l-none py-2 px-4 border-l-2">
                                          DECLINED
                                        </button>
                                      </div>
                                    </div>




                                    <div class="container_body">
                                      <div v-if="isLoading" class="text-center py-2">
                                        Loading ...
                                      </div>
                                      <div v-else-if="visibleApplicants.length === 0" class="py-4">
                                        <img src="./assets/empty.png" alt="empty" width="200" class="mx-auto">
                                        <p class="text-center">No applicants found</p>
                                      </div>
                                      <div v-else v-for="applicant in visibleApplicants" :key="applicant.id" class="box">
                                        <div class="lg:flex">
                                          <div class="sm:flex sm:flex-col">

                                            <p class="j_name">
                                              <span class="label" style="font-weight: 550;">Job Title : </span>
                                              {{ applicant.j_name }}
                                            </p>
                                            <p class="name">
                                              <span class="label" style="font-weight: 550;">Full name : </span>
                                              {{ applicant.firstname }} {{ applicant.lastname }}
                                            </p>
                                            <p class="posted_at">
                                              <span class="label" style="font-weight: 550;">Birthday : </span>
                                              {{ getFormattedDate(applicant.bday) }}
                                            </p>
                                            <p class="posted_at">
                                              <span class="label" style="font-weight: 550;">Age : </span>
                                              {{ getAgeFromBirthDate(applicant.bday) }}
                                            </p>
                                            <p class="address">
                                              <span class="label" style="font-weight: 550;">Address : </span>
                                              {{ applicant.address }}
                                            </p>
                                            <p class="posted_at">
                                              <span class="label" style="font-weight: 550;">Posted at : </span>
                                              {{ getFormattedDate(applicant.created_at) }}
                                            </p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                <?php } else {
                                navigate("?page=hire&sub=company");
                              } ?>
                                </div>
                                </div>
                              <?php } else {
                              navigate("?page=hire&sub=company");
                            } ?>
                            <?php } elseif ($page == "profile") { ?>
                              <?php if (form("sub")) { ?>
                                <!-- sidebar -->
                                <div class="
                                  bg-white border-r
                                  [border-right-color:_rgb(204,_204,_204)]
                                  w-[3rem]
                                  sm:w-[300px]
                                  py-4
                                  
                                  sm:pr-4
                                ">
                                  <a href="?page=profile&sub=general_information" class="
                                      flex gap-2
                                      pl-4 sm:pl-8 py-4 rounded-r-2xl mb-2
                                      <?=
                                      (value("sub") == "general_information")
                                        ? '[background-color:_#0e8136] text-white'
                                        : 'border border-white hover:[border-color:_#0e8136]'
                                      ?>
                                    ">
                                    <i class="fa-solid fa-circle-info h-6 flex items-center"></i>
                                    <p class="name hidden sm:block">General Information</p>
                                  </a>
                                  <a href="?page=profile&sub=password" class="
                                      flex gap-2
                                      pl-4 sm:pl-8 py-4  mb-2 rounded-r-2xl
                                      <?=
                                      (value("sub") == "password")
                                        ? '[background-color:_#0e8136] text-white'
                                        : 'border border-white hover:[border-color:_#0e8136]'
                                      ?>
                                    ">
                                    <i class="fa-solid fa-key h-6 flex items-center"></i>
                                    <p class="name hidden sm:block">Password</p>
                                  </a>
                                 
                                  <a href="/<?= $__name__ ?>/logout.php" class="
                                      flex gap-2
                                      pl-4 sm:pl-8 py-4  mb-2 rounded-r-2xl
                                      <?=
                                      (value("sub") == "logout")
                                        ? '[background-color:_#0e8136] text-white'
                                        : 'border border-white hover:[border-color:_#0e8136]'
                                      ?>
                                    ">
                                    <i class="fa fa-sign-out"></i>
                                    <p class="name hidden sm:block">Log Out</p>
                                  </a>
                                </div>
                                <div class="content">
                                  <div class="showcase" id="showcase_sub_<?= value("sub") ?>">
                                    <?php if (value("sub") == "general_information") { ?>
                                      <div class="container">
                                        <div class="container_title">
                                          Avatar
                                        </div>
                                        <div class="container_body">
                                          <img src="../../assets/images/<?= $u_avatar ?>" class="company_logo_preview profile_picture">
                                          <input type="file" name="input_upload_field" id="input_upload_field" class="input_upload_field" data-set="avatar" accept="image/*">
                                          <br>
                                          <label for="input_upload_field" class="btn_upload_picture">CHANGE AVATAR</label>
                                        </div>
                                      </div>
                                      <div class="container">
                                        <div class="container_title">
                                          Personal information & Account settings
                                        </div>
                                        <div class="container_body">
                                          <form method="post" class="frm_<?= $page ?>">
                                            <div class="field">
                                              <label for="tb_firstnmae" style="color: black; font-weight: 500;">First name</label>
                                              <input type="text" name="tb_firstname" id="tb_firstname" placeholder="e.g. John" value="<?= $u_fname ?>">
                                            </div>
                                            <div class="field">
                                              <label for="tb_lastname" style="color: black; font-weight: 500;">Last name</label>
                                              <input type="text" name="tb_lastname" id="tb_lastname" placeholder="e.g. Doe" value="<?= $u_lname ?>">
                                            </div>
                                            <div class="field">
                                              <label for="tb_age" style="color: black; font-weight: 500;">Age</label>
                                              <div class="border border-gray-300 w-full py-1 px-2">{{ calculatedAge }}</div>
                                            </div>
                                            <div class="field">
                                              <label for="tb_bday" style="color: black; font-weight: 500;">Birthday</label>
                                              <input type="date" name="tb_bday" id="tb_bday" v-model="birthDate" max="2005-01-01">
                                            </div>
                                            <div class="field">
                                              <label for="tb_address" style="color: black; font-weight: 500;">Address</label>
                                              <textarea name="tb_address" class="tb_address" id="tb_address" placeholder="e.g. 39 Plaza Dr, Rockwell Center, Makati, 1200 Metro Manila "><?= $u_address ?></textarea>
                                            </div>
                                            <div class="field">
                                              <label for="tb_email" style="color: black; font-weight: 500;">Email Address</label>
                                              <input type="email" name="tb_email" id="tb_email" placeholder="e.g. johndoe@gmail.com" value="<?= $u_email ?>">
                                            </div>
                                            <div class="field">
                                              <label for="tb_cnum" style="color: black; font-weight: 500;">Contact number</label>
                                              <input type="number" name="tb_cnum" id="tb_cnum" placeholder="e.g. 09XXX-XXX-YYY" value="<?= $u_cnum ?>" maxlength="11" oninput="this.value=this.value.slice(0,11)">
                                            </div>
                                            <button class="button btn_submit">
                                              UPDATE
                                            </button>
                                          </form>
                                        </div>
                                      </div>
                                    <?php } elseif (value("sub") == "password") { ?>
                                      <div class="container">
                                        <div class="container_title">
                                          Change Password
                                        </div>
                                        <div class="container_body">
                                          <form method="post" class="frm_<?= $page ?>_account">
                                            <div class="password">
                                              <label for="tb_pw" style="font-size: 16px; color: #333; font-weight: 500;">Current Password</label>
                                              <input type="password" name="tb_pw" id="tb_pw" placeholder="Enter your current password">
                                            </div>
                                            <div class="password" style="margin-top: 16px;">
                                              <label for="tb_newpw" style="font-size: 16px; color: #333; font-weight: 500;">New Password</label>
                                              <input type="password" name="tb_newpw" id="tb_newpw" placeholder="Enter your new password">
                                            </div>
                                            <div class="password" style="margin-top: 16px;">
                                              <label for="tb_cnewpw" style="color: black; font-weight: 500;">
                                                <label for="tb_cnewpw">Confirm new password</label>
                                                <input type="password" name="tb_cnewpw" id="tb_cnewpw" placeholder="Retype new password">
                                            </div>
                                            <button class="button btn_submit">
                                              SAVE
                                            </button>
                                          </form>
                                        </div>
                                      </div>
                                    <?php } else {
                                      navigate("?page=profile&sub=general_information");
                                    } ?>
                                  </div>
                                </div>
                              <?php } else {
                                navigate("?page=profile&sub=general_information");
                              } ?>
                            <?php } elseif ($page == "accounts") { ?>
                              <?php if (form("sub")) { ?>
                                <div class="
                                    bg-white border-r
                                    [border-right-color:_rgb(204,_204,_204)]
                                    w-[3rem]
                                    sm:w-[300px]
                                    py-4
                                    sm:pr-4
                                  ">
                                  <a href="?page=accounts&sub=list" class="
                                      flex gap-2
                                      pl-4 sm:pl-8 py-4 rounded-r-2xl 
                                      <?=
                                      (value("sub") == "list")
                                        ? '[background-color:_#0e8136] text-white'
                                        : 'border border-white hover:[border-color:_#0e8136]' 
                                      ?> 
                                    " style="margin-bottom: 15px;">
                                    <i class="fa-solid fa-users h-6 flex items-center"></i>
                                    <p class="name hidden sm:block">List</p>
                                  </a>
                                  <a href="?page=accounts&sub=add" class="
                                      flex gap-2
                                      pl-4 sm:pl-8 py-4 rounded-r-2xl
                                      <?=
                                      (value("sub") == "add")
                                        ? '[background-color:_#0e8136] text-white'
                                        : 'border border-white hover:[border-color:_#0e8136]'
                                      ?>
                                    ">
                                    <i class="fa-solid fa-user-plus h-6 flex items-center"></i>
                                    <p class="name hidden sm:block">Accounts</p>
                                  </a>
                                </div>
                                <div class="content">
                                  <div class="showcase" id="showcase_sub_<?= value("sub") ?>">
                                    <?php if (value("sub") == "list") { ?>
                                      <div class="container">
                                        <div class="[color:_rgb(45,_174,_110)] p-4 border-b [border-color:_rgb(204, 204, 204)]">
                                          <div class="flex flex-col md:flex-row justify-between items-center">
                                            <div class="title mb-4 md:mb-0">
                                              List of all Accounts
                                            </div>
                                            <div class="filter text-black flex-wrap flex justify-center md:justify-end">
                                              <a href="./?page=accounts&sub=list&filter=all" class="<?= ($filter == "all") ? "active" : "" ?>">ALL</a>
                                              <a href="./?page=accounts&sub=list&filter=admin" class="<?= ($filter == "admin") ? "active" : "" ?>">ADMIN</a>
                                              <a href="./?page=accounts&sub=list&filter=company" class="<?= ($filter == "company") ? "active" : "" ?>">COMPANY</a>
                                              <a href="./?page=accounts&sub=list&filter=client" class="<?= ($filter == "client") ? "active" : "" ?>">CLIENT</a>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="container_body">
                                          <?php if (hasResult($load_accounts)) { ?>
                                            <?php while ($row = mysqli_fetch_assoc($load_accounts)) { ?>
                                              <div class="box">
                                                <div class="">
                                                  <p class="s_name">
                                                    <span class="label" style="font-weight: 550;">Full name : </span>
                                                    <?= $row['firstname']; ?> <?= $row['lastname']; ?>
                                                  </p>
                                                  <p class="posted_at">
                                                    <span class="label" style="font-weight: 550;">Level : </span>
                                                    <?php if ($row["type"] == 1) { ?>
                                                      Admin
                                                    <?php } elseif ($row["type"] == 2) { ?>
                                                      Company
                                                    <?php } elseif ($row["type"] == 3) { ?>
                                                      Client
                                                    <?php } else {
                                                      echo "Unknown";
                                                    } ?>
                                                  </p>
                                                  <p class="posted_at">
                                                    <span class="label" style="font-weight: 550;">Age : </span>
                                                    <?= birthDateToAge($row["bday"]) ?>
                                                  </p>
                                                  <p class="address">
                                                    <span class="label" style="font-weight: 550;">Address : </span>
                                                    <?= $row['address']; ?>
                                                  </p>
                                                  <p class="posted_at">
                                                    <span class="label" style="font-weight: 550;">Created at : </span>
                                                    <?= date("m/d/Y", strtotime($row["created_at"])) ?>
                                                  </p>
                                                </div>
                                              </div>
                                            <?php } ?>
                                          <?php } else { ?>
                                            <div class="showcase">
                                              <img src="./assets/empty.png" alt="empty" width="200">
                                              <p>No Jobs Found</p>
                                            </div>
                                          <?php } ?>
                                        </div>
                                      </div>
                                    <?php } elseif (value("sub") == "company_information") { ?>

                                      <div class="container">
                                        <div class="container_title">
                                          Company Information
                                        </div>
                                        <div class="container_body">
                                          <form method="post" class="frm_<?= $page ?>_company">
                                            <div class="field">
                                              <label for="tb_name">Company name</label>
                                              <input type="text" name="tb_name" id="tb_name" placeholder="First name" value="<?= $c_name ?>">
                                            </div>
                                            <div class="field">
                                              <label for="tb_cnum">Contact number</label>
                                              <input type="number" name="tb_cnum" id="tb_cnum" placeholder="Contact number" value="<?= $c_cnum ?>" maxlength="11" oninput="this.value=this.value.slice(0,11)">
                                            </div>
                                            <div class="field">
                                              <label for="tb_position">Position <br>(your position)</label>
                                              <input type="text" name="tb_position" id="tb_position" placeholder="Position (Your position)" value="<?= $c_position ?>">
                                            </div>
                                            <div class="field">
                                              <label for="tb_address">Address</label>
                                              <textarea name="tb_address" class="tb_address" id="tb_address" placeholder="Address"><?= $c_address ?></textarea>
                                            </div>
                                            <button class="button btn_submit">
                                              SAVE
                                            </button>
                                          </form>
                                        </div>
                                      </div>
                                    <?php } elseif (value("sub") == "manage") { ?>
                                      <div class="container">
                                        <div class="container_title">
                                          Password
                                        </div>
                                        <div class="container_body">
                                          <form method="post" class="frm_<?= $page ?>_password">
                                            <div class="field">
                                              <label for="tb_pw">Old password</label>
                                              <input type="password" name="tb_pw" id="tb_pw" placeholder="Old password">
                                            </div>
                                            <div class="field">
                                              <label for="tb_newpw">New password</label>
                                              <input type="password" name="tb_newpw" id="tb_newpw" placeholder="New password">
                                            </div>
                                            <div class="field">
                                              <label for="tb_cnewpw">Confirm new password</label>
                                              <input type="password" name="tb_cnewpw" id="tb_cnewpw" placeholder="Confirm new password">
                                            </div>
                                            <button class="button btn_submit">
                                              SAVE
                                            </button>
                                          </form>
                                        </div>
                                      </div>
                                    <?php } elseif (value("sub") == "add") { ?>
                                      <div class="container">
                                        <div class="container_title">
                                          Create account
                                        </div>
                                        <div class="container_body form_box">
                                          <form method="post" class="frm_new_account" autocomplete="off">
                                            <div class="divider">
                                              <p>USER INFORMATION</p>
                                              <span></span>
                                            </div>
                                            <div class="field">
                                              <label for="tb_firstnmae" style="color: black; font-weight: 500; ">First name</label>
                                              <input type="text" name="fname" id="tb_firstname" placeholder="e.g. John" style="text-transform: capitalize;">
                                            </div>

                                            <div class="field">
                                              <label for="tb_lastname" style="color: black; font-weight: 500;">Last name</label>
                                              <input type="text" name="lname" id="tb_lastname" placeholder="e.g. Doe " style="text-transform: capitalize;">
                                            </div>
                                            <div class="field">
                                              <label for="tb_bday" style="color: black; font-weight: 500;">Birthday</label>
                                              <input type="date" name="bday" id="tb_bday" maxlength="8" oninput="this.value=this.value.slice(0,10)">
                                            </div>

                                            <div class="field">
                                              <label for="tb_cnum" style="color: black; font-weight: 500;">Contact number</label>
                                              <input type="number" name="cnum" id="tb_cnum" placeholder="e.g. 9XXX-XXX-YYY" maxlength="10" oninput="this.value=this.value.slice(0,10)" pattern="^09\d{9}$">
                                            </div>

                                            <div class="field">
                                              <label for="tb_address" style="color: black; font-weight: 500;">Address</label>
                                              <textarea name="address" id="tb_address" placeholder="  e.g. 39 Plaza Dr, Rockwell Center, Makati, 1200 Metro Manila "></textarea>
                                            </div>
                                            <div class="divider">
                                              <p>ACCOUNT INFORMATION</p>
                                              <span></span>
                                            </div>
                                            <div class="field">
                                              <label for="tb_email" style="color: black; font-weight: 500;">Email</label>
                                              <input type="text" name="email" id="tb_email" placeholder="e.g. johndoe@gmail.com">
                                            </div>
                                            <div class="field">
                                              <label for="tb_password" style="color: black; font-weight: 500;">Password</label>
                                              <input type="password" name="password" id="tb_password" placeholder="*********">
                                            </div>
                                            <div class="field">
                                              <label for="tb_cpassword" style="color: black; font-weight: 500;">Confirm Password</label>
                                              <input type="password" name="cpassword" id="tb_cpassword" placeholder="*********">
                                            </div>
                                            <div class="field">
                                              <label for="account_type" style="color: black; font-weight: 500;">ACCOUNT LEVEL</label>
                                              <select name="account_type" id="account_type">
                                                <option value="" selected>-- SELECT ACCOUNT LEVEL --</option>
                                                <option value="1">ADMINISTRATOR</option>
                                                <option value="2">COMPANY</option>
                                                <option value="3">STUDENT</option>
                                              </select>
                                            </div>
                                            <button class="button btn_submit">
                                              SAVE
                                            </button>
                                          </form>
                                        </div>
                                      </div>
                                    <?php } else {
                                      navigate("?page=accounts&sub=list");
                                    } ?>
                                  </div>
                                </div>
                              <?php } else {
                                navigate("?page=accounts&sub=list");
                              } ?>
                            <?php } else {
                            navigate("./");
                          } ?>
                              </div>
                            </div>
                          </div>
                            <script>
                              const app = Vue.createApp({
                                data() {
                                  return {
                                    siteName: "<?= $__name__ ?>",
                                    birthDate: "<?= $u_bday ?>",
                                    hasSubmitted: false,
                                    isLoading: true,
                                    companies: [],
                                    jobs: [],
                                    companyAccounts: [],
                                    applicants: [],
                                    selectedApplicantsTab: "all",
                                    pageSize: 10,
                                    currentPage: 0,
                                    searchTerm: "",
                                  }
                                },
                                async mounted() {
                                  try {
                                    const {
                                      companies
                                    } = await (
                                      await fetch(`/${this.siteName}/dashboard/admin/routes/getAllCompanies.php`)
                                    ).json();

                                    const {
                                      jobs
                                    } = await (
                                      await fetch(`/${this.siteName}/dashboard/admin/routes/getAllJobs.php`)
                                    ).json();

                                    const {
                                      companyAccounts
                                    } = await (
                                      await fetch(`/${this.siteName}/dashboard/admin/routes/getAllCompanyAccounts.php`)
                                    ).json();

                                    const {
                                      applicants
                                    } = await (
                                      await fetch(`/${this.siteName}/dashboard/admin/routes/getAllApplicants.php`)
                                    ).json();

                                    if (companies) {
                                      this.companies = companies
                                    }

                                    if (jobs) {
                                      this.jobs = jobs
                                    }

                                    if (companyAccounts) {
                                      this.companyAccounts = companyAccounts
                                    }

                                    if (applicants) {
                                      this.applicants = applicants
                                    }
                                  } catch (e) {
                                    console.log("Error occured:", e)
                                  } finally {
                                    this.isLoading = false
                                  }
                                },
                                watch: {
                                  searchTerm() {
                                    this.currentPage = 0
                                  }
                                },
                                methods: {
                                  onClickApplicantsTab(tabName) {
                                    this.selectedApplicantsTab = tabName
                                  },
                                  getAgeFromBirthDate(birthDate) {
                                    if (!birthDate) return "N/A"

                                    const duration = luxon.DateTime.fromFormat(
                                      birthDate,
                                      "yyyy-MM-dd", {
                                        zone: "Asia/Manila",
                                      }
                                    ).diffNow("years").years

                                    const dropSign = Math.abs(duration)
                                    const dropDecimals = Math.trunc(dropSign)
                                    return dropDecimals
                                  },
                                  getFormattedDate(dateStr) {
                                    const date = new Date(dateStr)
                                    const year = date.getFullYear()
                                    const monthInt = date.getMonth() + 1
                                    const month = monthInt < 10 ? `0${monthInt}` : `${monthInt}`
                                    const dayOfTheMonthInt = date.getDate()
                                    const dayOfTheMonth = dayOfTheMonthInt < 10 ? `0${dayOfTheMonthInt}` : `${dayOfTheMonthInt}`

                                    return `${month}/${dayOfTheMonth}/${year}`
                                  }
                                },
                                computed: {
                                  filteredApplicants() {
                                    return this.applicants
                                      .filter((applicant) => {
                                        if (this.selectedApplicantsTab === "all") return true
                                        if (this.selectedApplicantsTab === "pending" && applicant.status === "1") {
                                          console.log("pending applicant", applicant)
                                          return true
                                        }
                                        if (this.selectedApplicantsTab === "hired" && applicant.status === "2") {
                                          console.log("hired applicant", applicant)
                                          return true
                                        }
                                        if (this.selectedApplicantsTab === "declined" && applicant.status === "3") return true

                                        return false
                                      })
                                      .filter((applicant) =>
                                        `${applicant.firstname} ${applicant.lastname}`
                                        .toLowerCase()
                                        .includes(this.searchTerm.toLowerCase())
                                      )
                                  },
                                  visibleApplicants() {
                                    return this.filteredApplicants.slice(this.currentPage * this.pageSize, this.currentPage * this.pageSize + this.pageSize)
                                  },
                                  hasNextApplicantsPage() {
                                    if (this.filteredApplicants.length === 0) return false

                                    if (this.currentPage !== Math.ceil(this.filteredApplicants.length / this.pageSize) - 1)
                                      return true

                                    return false
                                  },
                                  filteredCompanyAccounts() {
                                    return this.companyAccounts.filter((companyAccount) =>
                                      `${companyAccount.firstname} ${companyAccount.lastname}`
                                      .toLowerCase()
                                      .includes(this.searchTerm.toLowerCase())
                                    )
                                  },
                                  visibleCompanyAccounts() {
                                    return this.filteredCompanyAccounts.slice(this.currentPage * this.pageSize, this.currentPage * this.pageSize + this.pageSize)
                                  },
                                  hasNextCompanyAccountsPage() {
                                    if (this.filteredCompanyAccounts.length === 0) return false

                                    if (this.currentPage !== Math.ceil(this.filteredCompanyAccounts.length / this.pageSize) - 1)
                                      return true

                                    return false
                                  },
                                  filteredJobs() {
                                    return this.jobs.filter((job) =>
                                      job.j_name.toLowerCase().includes(this.searchTerm.toLowerCase())
                                    )
                                  },
                                  visibleJobs() {
                                    return this.filteredJobs.slice(this.currentPage * this.pageSize, this.currentPage * this.pageSize + this.pageSize)
                                  },
                                  hasNextJobsPage() {
                                    if (this.filteredJobs.length === 0) return false

                                    if (this.currentPage !== Math.ceil(this.filteredJobs.length / this.pageSize) - 1)
                                      return true

                                    return false
                                  },
                                  filteredCompanies() {
                                    return this.companies.filter((company) =>
                                      company.c_name.toLowerCase().includes(this.searchTerm.toLowerCase())
                                    )
                                  },
                                  visibleCompanies() {
                                    return this.filteredCompanies.slice(this.currentPage * this.pageSize, this.currentPage * this.pageSize + this.pageSize)
                                  },
                                  hasNextCompaniesPage() {
                                    if (this.filteredCompanies.length === 0) return false

                                    if (this.currentPage !== Math.ceil(this.filteredCompanies.length / this.pageSize) - 1)
                                      return true

                                    return false
                                  },
                                  hasPrevPage() {
                                    if (this.currentPage > 0)
                                      return true

                                    return false
                                  },
                                  calculatedAge() {
                                    if (!this.birthDate) return "N/A"

                                    const duration = luxon.DateTime.fromFormat(
                                      this.birthDate,
                                      "yyyy-MM-dd", {
                                        zone: "Asia/Manila",
                                      }
                                    ).diffNow("years").years

                                    const dropSign = Math.abs(duration)
                                    const dropDecimals = Math.trunc(dropSign)

                                    if (isNaN(dropDecimals)) return "N/A"
                                    return dropDecimals
                                  }
                                }
                              })
                              app.mount("#app")
                            </script>
</body>

</html>