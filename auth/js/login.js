// From: https://stackoverflow.com/a/26358856/19193225
function getBrowserName() {
  if (
    (navigator.userAgent.indexOf("Opera") ||
      navigator.userAgent.indexOf("OPR")) != -1
  )
    return "Opera"

  if (navigator.userAgent.indexOf("Edg") != -1) return "Edge"
  if (navigator.userAgent.indexOf("Chrome") != -1) return "Chrome"
  if (navigator.userAgent.indexOf("Safari") != -1) return "Safari"
  if (navigator.userAgent.indexOf("Firefox") != -1) return "Firefox"

  if (
    navigator.userAgent.indexOf("MSIE") != -1 ||
    !!document.documentMode == true
  ) {
    //IF IE > 10
    return "IE"
  }

  return "Unknown"
}

$(document).ready(() => {
  $(".login_account").on("submit", (e) => {
    e.preventDefault()
    const data = [
      {
        name: "browserName",
        value: getBrowserName(),
      },
      ...$(".login_account").serializeArray(),
    ]

    if (grecaptcha.getResponse() === "") {
      Swal.fire(
        "CAPTCHA Required",
        "Please fullfill the CAPTCHA to proceed.",
        "error"
      )
      return
    }

    $.ajax({
      url: "../auth/routes/login.php",
      method: "post",
      data,
      success: (res) => {
        console.log(res)
        if (res.success) {
          Swal.fire("Success", `${res.message}`, "success")
          setTimeout(() => {
            window.location.href = "../"
          }, 2000)
        } else {
          console.log("res", res)
          if (res.message === "Company account is not associated with a company.") {
            Swal.fire("Warning", `${res.message}`, "warning")
          } else {
            Swal.fire("Failed", `${res.message}`, "error")
          }
        }
      },
    })
  })
})
