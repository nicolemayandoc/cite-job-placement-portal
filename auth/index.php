<?php
session_start();
require_once '../config.php';
require_once '../functions.php';
require_once '../session.php';

if ($islogin == true) {
    navigate("../");
} else {
    if (form("a")) {
        $a = value("a");
        $step = (form("step")) ? value("step") : "1";
        $type = (form("type")) ? value("type") : "3";
    } else {
        navigate("../");
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../assets/logo.png">
    <title>CITE Job Portal</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous" defer></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11" defer></script>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <!-- javascript -->
    <script src="./js/register_company.js" defer></script>
    <script src="./js/register.js" defer></script>
    <script src="./js/login.js" defer></script>
    <script src="https://cdn.tailwindcss.com"></script>
    <!-- Recaptcha -->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body>

    <div class="parent clearfix">
        <div class="bg-illustration">
            <a href="/<?= $__name__ ?>" class="header_logo">
                <div class=" gap-2 sm:block lg:flex md:block  sm:items-center">
                    <img src="/<?= $__name__ ?>/assets/logo.png" alt="logo">
                    <p class="mt-5">CITE Job Portal</p>
                </div>
            </a>
        </div>

        <div class="login">

            <div class="container">
            <h1 class="text-3xl font-normal text-black">
  <span class="font-bold text-green-300">CITE</span> Job Placement Portal
</h1>

                <div class="login-form">
                <p style="text-align: center;">Please login to your account</p>
                    <form method="post" class="form login_account" autocomplete="off">
                        <input type="email" name="email" placeholder="Email">
                        <input id="txtPassword" type="password" name="password" placeholder="Password">
                        <div class="w-full mb-4">
                            <label>
                                <input type="checkbox" id="btnToggleTxtPassword"> <span class="text-sm text-gray-500">Show Password</span>
                            </label>
                        </div>
                        <div class="g-recaptcha py-2" data-sitekey="6Ld-7gkkAAAAAJsB1gujdU56p703NYZci-VY2zTs"></div>
                        <button type="submit" name="register_company">Sign in</button>

                    </form>
                </div>

            </div>
        </div>

    </div>
    <script>
        const btnToggleTxtPassword = document.querySelector("#btnToggleTxtPassword")
        if (btnToggleTxtPassword) {
            btnToggleTxtPassword.addEventListener("change", (e) => {
                const txtPassword = document.querySelector("#txtPassword")
                if (!txtPassword) return

                if (e.target.checked) {                    
                    txtPassword.type = "text"
                } else {
                    txtPassword.type = "password"
                }
            })
        }
    </script>
</body>

</html>