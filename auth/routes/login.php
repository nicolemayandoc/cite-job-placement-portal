<?php
session_start();

require_once '../../config.php';
require_once '../../functions.php';

header("Content-Type: application/json");

function hasAssociatedCompany($conn, $userId) {
    $query = "SELECT id FROM tbl_company WHERE userid = ?";
    $stmt = $conn->prepare($query);
    $stmt->bind_param("i", $userId);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        return true;
    }
    return false;
}

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $email = mysqli_value($con,"email");
    $password = mysqli_value($con,"password");
    $browserName = mysqli_value($con,"browserName");

    function message($status,$message){
        $msg = array(
            "success" => $status,
            "message" => $message
        );
        echo arraytojson($msg);
        die();
    }
    if($email == ""){
        message(false,"Please enter your email.");
    }
    if($password == ""){
        message(false,"Please enter your password.");
    }

    if($browserName === ""){
        message(false,"Could not determine your browser.");
    }

    //check if email is already registered
    $check_email_result = mysqli_query($con, "SELECT id, password FROM `tbl_accounts` WHERE `email` = '$email' ");
    if(hasResult($check_email_result)){
        $raw_password = $check_email_result->fetch_assoc()["password"];
        if (password_verify($password, $raw_password)) {
            // On successful login, record login.
            $userData = mysqli_query($con, "SELECT * FROM `tbl_accounts` WHERE `email` = '$email' ");
            $fetchedUser = mysqli_fetch_assoc($userData);

            if ($fetchedUser["type"] == 2 && !hasAssociatedCompany($con, $fetchedUser["id"])) {
                message(false, "Company account is not associated with a company.");
            } else {
                $stmt = $con->prepare("INSERT INTO tbl_login_history (account_id, browser_name) VALUES (?, ?)");
                $stmt->bind_param("is", $fetchedUser["id"], $browserName);
                $stmt->execute();

                // Save user data to the session.
                $_SESSION["islogin"] = true;
                $_SESSION["data"] = $fetchedUser;

                // Send response to the browser.
                message(true,"Successful login.");
            }
        }
        message(false,"Email and password is not valid, Please try again.");
    }else{
        message(false,"Email and password is not valid, Please try again.");
    }
}
?>