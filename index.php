<?php
session_start();
require_once 'config.php';
require_once 'functions.php';
require_once 'session.php';

if ($islogin) {
    if ($u_type == 1) {
        navigate("./dashboard/admin");
    }
}

function getJobCount($conn)
{
    $query = "SELECT COUNT(id) AS job_count FROM `tbl_jobs`";
    $result = $conn->query($query);

    $row = $result->fetch_assoc();
    return $row["job_count"];
}

function getApplicantCount($conn)
{
    $query = "SELECT COUNT(id) AS applicant_count FROM `tbl_applicants`";
    $result = $conn->query($query);

    $row = $result->fetch_assoc();
    return $row["applicant_count"];
}

function getUserCount($conn)
{
    $query = "SELECT COUNT(id) AS user_count FROM `tbl_accounts`";
    $result = $conn->query($query);

    $row = $result->fetch_assoc();
    return $row["user_count"];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="./assets/logo.png">
    <title>CITE Job Portal</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous" defer></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11" defer></script>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="verify.css">
    <link rel="stylesheet" href="./header.css">
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
</head>

<body>
    <div class="main">
        <?php include 'header.php' ?>
        <div class="body">
            <div
                    class="bg-contain h-[16rem] sm:h-[24rem] md:h-[32rem] md:bg-cover bg-no-repeat bg-center"
                    style="background-image: url('/<?= $__name__ ?>/assets/homepage-bg2.png')"
                >
            </div>
            <!-- Slider Image End -->

        </div>
        <div class="flex justify-center p-8 flex-wrap">
            <div class="bg-green-600 shadow-md flex items-center justify-center px-[8rem] py-2" style="height: 120px;">
                <div class="bg-white flex items-center justify-center rounded mb-2" style="width: 60px; height: 60px;">
                    <i class="fa fa-briefcase text-green-600 text-[1.4rem]"></i>
                </div>
                <div class="pl-4">
                    <h5 class="text-white font-semibold text-[1.2rem] mb-0">Jobs</h5>
                    <h1 class="text-white text-[1.5rem] mb-0" data-toggle="counter-up">
                        <?= getJobCount($con) ?>
                    </h1>
                </div>
            </div>

            <div class="bg-white shadow-md flex items-center justify-center px-[8rem] py-2" style="height: 120px;">
                <div class="bg-green-600 flex items-center justify-center rounded mb-2" style="width: 60px; height: 60px;">
                    <i class="fa fa-user text-white text-[1.4rem]"></i>
                </div>
                <div class="pl-4">
                    <h5 class="text-green-600 font-semibold text-[1.2rem] mb-0">Applicants</h5>
                    <h1 class="text-green-600 text-[1.5rem] mb-0" data-toggle="counter-up">
                        <?= getApplicantCount($con) ?>
                    </h1>
                </div>
            </div>
            <div class="col-lg-4 wow zoomIn" data-wow-delay="0.6s">
                <div class="bg-green-600 shadow-md flex items-center justify-center px-[8rem] py-2" style="height: 120px;">
                    <div class="bg-white flex items-center justify-center rounded mb-2" style="width: 60px; height: 60px;">
                        <i class="fa fa-users text-green-600 text-[1.4rem]"></i>
                    </div>
                    <div class="pl-4">
                        <h5 class="text-white font-semibold text-[1.2rem] mb-0">Users</h5>
                        <h1 class="text-white text-[1.5rem] mb-0" data-toggle="counter-up">
                            <?= getUserCount($con) ?>
                        </h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="flex about-us-homepage flex-wrap justify-between mt-20 mx-5 sm:mr-[7rem]">
            <div class="w-full sm:w-[50%] sm:mx-auto">

                <h4 class="text-2xl font-semibold text-green-600 animate-up">ABOUT US</h4>


                <h2 class="mt-2 text-4xl font-bold animate-up">Connecting Job Seekers with Their Dream Jobs</h2>
                <div class="section-title animate-up mt-3"> </div>
                <p class="mt-10 animate-up fpnt-light text-gray-600">We are a new job placement portal dedicated to helping students and recent graduates kickstart their careers. Our team is passionate about connecting job seekers with top employers across different industries. With our fresh and innovative approach, we aim to provide a user-friendly platform that makes job hunting easier and more accessible. Our mission is to empower job seekers by offering them the tools and resources they need to achieve their career goals.</p>

                <div class="mt-4 grid grid-homepage grid-cols-2 gap-2 ">
                    <div class="flex items-center gap-2">
                        <i class="fa fa-check text-2xl text-green-600 grid-homepage" aria-hidden="true"></i>
                        <p class="font-semibold">Personalized Job Alerts</p>
                    </div>
                    <div class="flex items-center gap-2">
                        <i class="fa fa-check text-2xl text-green-600" aria-hidden="true"></i>
                        <p class="font-semibold">User-friendly Platform</p>
                    </div>
                    <div class="flex items-center gap-2">
                        <i class="fa fa-check text-2xl text-green-600" aria-hidden="true"></i>
                        <p class="font-semibold">SMS Job Alerts</p>
                    </div>
                    <div class="flex items-center gap-2">
                        <i class="fa fa-check text-2xl text-green-600" aria-hidden="true"></i>
                        <p class="font-semibold ">Mobile App</p>
                    </div>
                </div>

                <div class="flex items-center gap-3 mt-7 mb-4 animate-up">
                    <div class="bg-green-600 px-5 py-3 rounded-sm animate-up">
                        <i class="fa fa-phone text-white text-2xl" aria-hidden="true"></i>
                    </div>
                    <div>
                        <h4 class=" text-xl animate-up">Call to ask any question.</h4>
                        <h5 class=" text-xl animate-up text-green-600 font-semibold">+63 995-078-5660</h5>
                    </div>
                </div>
                <div class="mt-10 call-homepage">
                    <a href="/cite-job-placement-portal/contact-us/" class=" focus:outline-none text-xl text-white bg-green-600 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-sm text-sm px-10 py-2 mr-2 mb-6 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800 call-homepage">Contact Us</a>
                </div>
            </div>

            <div class="mt-8 image-homepage">
                <img src="assets/login.png" alt="image" class="object-cover h-full w-full">
            </div>
        </div>



        <div class="mt-40 container-items justify-between pb-24">
    <div class="text-center flex flex-col items-center">
        <h4 class="text-3xl font-semibold text-green-600">WHY CHOOSE US?</h4>
        <h2 class="mt-2 text-4xl font-bold w-full md:w-1/2">Connecting Job Seekers with Their Dream Jobs</h2>
        <div class="section-title  mt-3"> </div>
    </div>
    <div class="grid grid-cols-1 md:grid-cols-3 gap-10 mt-10">
        <div class="flex flex-col gap-5">
            <div class="relative flex flex-col gap-2">
                <div class="bg-green-600 px-5 py-3 rounded-sm w-[15%]">
                    <i class="fa fa-bell-o text-white text-2xl" aria-hidden="true"></i>
                </div>
                <p class="font-semibold text-2xl">Personalized Job Alerts</p>
                <p class="text-gray-500">Our platform enables job seekers to receive customized job alerts based on their preferred industry, job type, and location, simplifying their job search process.



</p>
            </div>
            <div class="relative flex flex-col gap-2 mt-14">
                <div class="bg-green-600 px-5 py-3 rounded-sm w-[15%]">
                    <i class="fa fa-bell-o text-white text-2xl" aria-hidden="true"></i>
                </div>
                <p class="font-semibold text-2xl">Job Listing</p>
                <p class="text-sm">Our platform provides job seekers with access to diverse job opportunities that align with their skills and interests.</p>
            </div>
        </div>

        <div class="text-center">
            <img src="assets/login.png" alt="image">
        </div>

        <div class="flex flex-col gap-5">
            <div class="relative flex flex-col gap-2">
                <div class="bg-green-600 px-5 py-3 rounded-sm w-[15%]">
                    <i class="fa fa-bell-o text-white text-2xl" aria-hidden="true"></i>
                </div>
                <p class="font-semibold text-2xl">User-Friendly Platform</p>
                <p class="text-sm">
                Our platform simplifies the job search process for both job seekers and employers, allowing job seekers to search for job openings and apply, while employers can post openings and manage applications.
                </p>
            </div>
            <div class="relative flex flex-col gap-2 mt-14">
                <div class="bg-green-600 px-5 py-3 rounded-sm w-[15%]">
                    <i class="fa fa-mobile text-white text-2xl" aria-hidden="true"></i>
                </div>
                <p class="font-semibold text-2xl">Mobile App</p>
                <p class="text-sm">Our platform offers a mobile app for job seekers to search, apply, and receive job alerts on their smartphones or tablets, making job hunting more convenient and efficient.</p>
            </div>
        </div>
    </div>
</div>
<?php include "./footer.php" ?>


    <script>
        const aboutUs = document.querySelector('.about-us-homepage');
        const imageAboutUs = document.querySelector('.image-homepage');
        const callAboutUs = document.querySelector('.call-homepage');
        const textElements = aboutUs.querySelectorAll('.animate-up');
        const grid = document.querySelector('.grid-homepage');

        const observer = new IntersectionObserver(entries => {
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                    textElements.forEach(el => el.classList.add('animate'));
                    imageAboutUs.classList.add('animate');
                    aboutUs.classList.add('animate');
                    grid.classList.add('animate');
                    callAboutUs.classList.add('animate');
                } else {
                    imageAboutUs.classList.remove('animate');
                    aboutUs.classList.remove('animate');
                    grid.classList.remove('animate');
                    callAboutUs.classList.remove('animate');
                }
            });  
        });
        observer.observe(aboutUs);
    </script>


</body>

</html>