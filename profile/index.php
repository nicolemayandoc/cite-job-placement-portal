<?php
session_start();
require_once '../config.php';
require_once '../functions.php';
require_once '../session.php';

if (!$islogin) {
    navigate("../");
}

if (!form("page") && value("page") !== "") {
    navigate("./?page=general_information");
} else {
    $page = value("page");
    if ($page == "general_information") {
        if (form("step") && value("step") == "1") {

            if (isset($_SESSION["random_id"])) {
                $session = $_SESSION["random_id"];
            } else {
                $session = randomid();
                $_SESSION["random_id"] = $session;
            }

            $check_session =  mysqli_query($con, "SELECT * FROM `tbl_verificationcode` WHERE `id` = '$u_id'");
            if (hasResult($check_session)) {
                $already_sent = true;
            } else {

                $generated_code = rand(100000, 999999);

                $sms_message = "CITE Job Placement Portal \nVerification code: $generated_code";

                clicksend_sms($u_cnum, $sms_message);

                $sms_log = mysqli_query($con, "
                    INSERT INTO `tbl_sms_logs`(
                        `receiverid`,
                        `message`
                    )
                    VALUES(
                        $u_id,
                        '$sms_message'
                    )
                ");

                $create_new_session = mysqli_query($con, "
                    INSERT INTO `tbl_verificationcode`(
                        `id`,
                        `session`,
                        `code`
                    )
                    VALUES(
                        '$u_id',
                        '$session',
                        '$generated_code'
                    )
                ");

                $already_sent = false;
            }
        } else {
            $already_sent = false;
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../assets/logo.png">
    <title>CITE Job Portal - PROFILE</title>
    <!-- js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous" defer></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11" defer></script>
    <script src="./js/index.js" defer></script>
    <script src="./js/update_general.js" defer></script>
    <script src="./js/update_account.js" defer></script>
    <script src="./js/update_password.js" defer></script>
    <script src="./js/send_verification_code.js" defer></script>
    <script src="./js/resume_account.js" defer></script>
    <!-- css -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="./style.css">
    <link rel="stylesheet" href="../verify.css">
    <link rel="stylesheet" href="../header.css">
    <!-- Import Vue - a declarative UI framework -->
    <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
    <!-- Import Luxon - library for calculating dates -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/luxon/3.1.0/luxon.min.js"></script>
    <!-- Import Tailwind - a framework for writing CSS faster -->
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body>
    <div class="main">
        <?php include '../header.php' ?>
        <div class="body">


            <div class="showcase">
                <div class="content box_<?= $page ?>">
                    <?php if ($page == "general_information") { ?>
                        <?php if (form("sub") && value("sub") !== "" && value("sub") == "verified") { ?>
                            <?php if (form("step") && value("step") == "1" && $u_verification_state == 0) { ?>
                                <div class="flex flex-wrap gap-4 justify-center mt-5">
                                    <div class="flex flex-col w-full mb-4 sm:w-[20rem]">
                                        <div class="container flex flex-col items-center max-w-3xl mx-auto">
                                            <div class="container_title w-full text-center">
                                                Avatar
                                            </div>
                                            <div class="container_body w-full">
                                                <img src="../assets/images/<?= $u_avatar ?>" class="object-contain w-full company_logo_preview profile_picture">
                                                <input type="file" name="input_upload_field" id="input_upload_field" class="input_upload_field" data-set="avatar" accept="image/*">
                                                <br>
                                                <label for="input_upload_field" class="btn_upload_picture w-full">CHANGE AVATAR</label>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="flex flex-col mt-8 z-10 h-[100%] w-[100%] gap-3 sm:w-[100%]" id="sidebar">
                                                <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "general_information") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="?page=general_information">
                                                    <i class="fa fa-user"></i>
                                                    <p class="name" id="line_name">Account Information</p>
                                                </a>
                                                <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "password") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="?page=password">
                                                    <i class="fa fa-key"></i>
                                                    <p class="name" id="line_name">Password</p>
                                                </a>
                                                <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "resume") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="?page=resume">
                                                    <i class="fa fa-file-text-o"></i>
                                                    <p class="name" id="line_name">Resume</p>
                                                </a>
                                                <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "logout") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="/<?= $__name__ ?>/logout.php">
                                                    <i class="fa fa-sign-out"></i>
                                                    <span class="name">Log Out</span>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="container max-w-3xl h-[350px] sm:h-[300px]">
                                        <div class="container_title">
                                            Step 1, Verify your mobile number.
                                        </div>
                                        <div class="container_body ">
                                            <?php if ($already_sent) { ?>
                                                <p>A verification code has been sent to your mobile number (<?= $u_cnum ?>). Please check your messages and enter the code in the next step to verify your account.</p>
                                            <?php } else { ?>
                                                <p>A verification code has been sent to your mobile number (<?= $u_cnum ?>). Please check your messages and enter the code on the next page to continue with the verification process.</p>
                                            <?php } ?>
                                            <br>
                                            <form method="post" class="frm_<?= $page ?>_verify" autocomplete="off">
                                                <div class="field">
                                                    <label for="tb_code">Verfication code </label>
                                                    <input type="number" name="tb_code" id="tb_code" placeholder="Combination of 6 Digits">
                                                </div>
                                                <button class="button btn_submit bg-green-700 text-white py-2 px-4 rounded-lg hover:bg-green-600 transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-10">
                                                    CONTINUE
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                    <div>
                                    <?php } elseif (form("step") && value("step") == "2") { ?>

                                    <?php } else { ?>
                                        <?php if ($u_verification_state == 0) { ?>
                                            <div class="flex flex-wrap gap-4 justify-center mt-5">
                                                <div class="flex flex-col w-full mb-4 sm:w-[20rem]">
                                                    <div class="container flex flex-col items-center max-w-3xl mx-auto">
                                                        <div class="container_title w-full text-center">
                                                            Avatar
                                                        </div>
                                                        <div class="container_body w-full">
                                                            <img src="../assets/images/<?= $u_avatar ?>" class="object-contain w-full company_logo_preview profile_picture">
                                                            <input type="file" name="input_upload_field" id="input_upload_field" class="input_upload_field" data-set="avatar" accept="image/*">
                                                            <br>
                                                            <label for="input_upload_field" class="btn_upload_picture w-full">CHANGE AVATAR</label>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="flex flex-col mt-8 z-10 h-[100%] w-[100%] gap-3 sm:w-[100%]" id="sidebar">
                                                            <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "general_information") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="?page=general_information">
                                                                <i class="fa fa-user"></i>
                                                                <p class="name" id="line_name">Account Information</p>
                                                            </a>
                                                            <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "password") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="?page=password">
                                                                <i class="fa fa-key"></i>
                                                                <p class="name" id="line_name">Password</p>
                                                            </a>
                                                            <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "resume") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="?page=resume">
                                                                <i class="fa fa-file-text-o"></i>
                                                                <p class="name" id="line_name">Resume</p>
                                                            </a>
                                                            <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "logout") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="/<?= $__name__ ?>/logout.php">
                                                                <i class="fa fa-sign-out"></i>
                                                                <span class="name">Log Out</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="container max-w-3xl h-[145px] sm:h-[115px]">
                                                    <div class="container_title">
                                                        Step 1, Verify your mobile number.
                                                    </div>
                                                    <div class="container_body hover:bg-gray-500/10">
                                                        <a href="?page=general_information&sub=verified&step=1" class="hover:text-blue-700">Send a verification code to my mobile number  (<?= $u_cnum ?>).</a>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } elseif ($u_verification_state == 1) { ?>
                                            <div class="flex flex-wrap gap-4 justify-center mt-5">
                                                <div class="flex flex-col w-full mb-4 sm:w-[20rem]">
                                                    <div class="container flex flex-col items-center max-w-3xl mx-auto">
                                                        <div class="container_title w-full text-center">
                                                            Avatar
                                                        </div>
                                                        <div class="container_body w-full">
                                                            <img src="../assets/images/<?= $u_avatar ?>" class="object-contain w-full company_logo_preview profile_picture">
                                                            <input type="file" name="input_upload_field" id="input_upload_field" class="input_upload_field" data-set="avatar" accept="image/*">
                                                            <br>
                                                            <label for="input_upload_field" class="btn_upload_picture w-full">CHANGE AVATAR</label>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="flex flex-col mt-8 z-10 h-[100%] w-[100%] gap-3 sm:w-[100%]" id="sidebar">
                                                            <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "general_information") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="?page=general_information">
                                                                <i class="fa fa-user"></i>
                                                                <p class="name" id="line_name">Account Information</p>
                                                            </a>
                                                            <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "password") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="?page=password">
                                                                <i class="fa fa-key"></i>
                                                                <p class="name" id="line_name">Password</p>
                                                            </a>
                                                            <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "resume") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="?page=resume">
                                                                <i class="fa fa-file-text-o"></i>
                                                                <p class="name" id="line_name">Resume</p>
                                                            </a>
                                                            <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "logout") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="/<?= $__name__ ?>/logout.php">
                                                                <i class="fa fa-sign-out"></i>
                                                                <span class="name">Log Out</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="container max-w-3xl h-[170px] sm:h-[140px] ">
                                                    <div class="container_title">
                                                        Step 2, Upload your resume.
                                                    </div>
                                                    <div class="container_body  hover:bg-gray-500/10">
                                                        <a href="?page=resume" class="hover:text-blue-700">Please upload your resume. We use it to confirm your account and ensure the security of our platform. Click the "CONTINUE" to proceed.</a>
                                                    </div>
                                                </div>
                                            <?php } elseif ($u_verification_state == 2) { ?>
                                                <div class="container">
                                                    <div class="container_title">
                                                        Step 3, Finish
                                                    </div>
                                                    <div class="container_body">
                                                        <p>You are now fully verified</p>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php } else {
                                            navigate("?page=general_information");
                                        } ?>
                                    <?php } ?>
                                <?php } else { ?>
                                    <div class="flex flex-wrap gap-4 justify-center mt-5">
                                        <div class="flex flex-col w-full mb-4 sm:w-[20rem]">
                                            <div class="container flex flex-col items-center max-w-3xl mx-auto">
                                                <div class="container_title w-full text-center">
                                                    Avatar
                                                </div>
                                                <div class="container_body w-full">
                                                    <img src="../assets/images/<?= $u_avatar ?>" class="object-contain w-full company_logo_preview profile_picture">
                                                    <input type="file" name="input_upload_field" id="input_upload_field" class="input_upload_field" data-set="avatar" accept="image/*">
                                                    <br>
                                                    <label for="input_upload_field" class="btn_upload_picture w-full">CHANGE AVATAR</label>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="flex flex-col mt-8 z-10 h-[100%] w-[100%] gap-3 sm:w-[100%]" id="sidebar">
                                                    <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "general_information") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="?page=general_information">
                                                        <i class="fa fa-user"></i>
                                                        <p class="name" id="line_name">Account Information</p>
                                                    </a>
                                                    <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "password") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="?page=password">
                                                        <i class="fa fa-key"></i>
                                                        <p class="name" id="line_name">Password</p>
                                                    </a>
                                                    <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "resume") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="?page=resume">
                                                        <i class="fa fa-file-text-o"></i>
                                                        <p class="name" id="line_name">Resume</p>
                                                    </a>
                                                    <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "logout") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="/<?= $__name__ ?>/logout.php">
                                                        <i class="fa fa-sign-out"></i>
                                                        <span class="name">Log Out</span>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="container max-w-3xl">
                                            <div class="container_title text-lg font-medium text-green-600 mb-2">
                                                Personal information & Account settings
                                            </div>
                                            <div class="container_body">
                                                <?php if ($u_verification_state == 0 || $u_verification_state < 2) { ?>
                                                    <div class="msg">
                                                        <p>
                                                            Your account is not fully verified.
                                                        </p>
                                                        <a href="?page=general_information&sub=verified">
                                                            GET VERIFIED NOW
                                                        </a>
                                                    </div>
                                                <?php } ?>
                                                <form method="post" class="frm_<?= $page ?>" id="app">
                                                    <div class="flex flex-wrap -mx-2 ">
                                                        <div class="w-full md:w-1/2 px-2 mb-4 md:mb-0">
                                                            <label for="tb_firstname" class="block text-black font-medium mb-2">First name</label>
                                                            <div class="flex ">
                                                                <input type="text" name="tb_firstname" id="tb_firstname" placeholder="e.g. John" value="<?= $u_fname ?>" class="w-full border border-gray-400 rounded py-2 px-4 mb-3 leading-tight focus:ring-1 focus:ring-green-800 focus:bg-white focus:border-green-400 capitalize">
                                                            </div>
                                                        </div>
                                                        <div class="w-full md:w-1/2 px-2">
                                                            <label for="tb_lastname" class="block text-black font-medium mb-2">Last name</label>
                                                            <input type="text" name="tb_lastname" id="tb_lastname" placeholder="e.g. Doe" value="<?= $u_lname ?>" class="w-full border border-gray-400 rounded py-2 px-4 mb-3 leading-tight focus:ring-1 focus:ring-green-800 focus:bg-white focus:border-green-400 capitalize">
                                                        </div>
                                                    </div>


                                                    <div class="flex flex-wrap -mx-2">
                                                        <div class="w-full md:w-1/2 px-2 mb-4 md:mb-0">
                                                            <label for="tb_bday" class="block text-black font-medium mb-2">Birthday</label>
                                                            <input type="date" name="tb_bday" id="tb_bday" v-model="birthDate" class="w-full border border-gray-400 rounded py-2 px-4 mb-3 leading-tight focus:ring-1 focus:ring-green-800 focus:bg-white focus:border-green-400">
                                                        </div>
                                                        <div class="w-full md:w-1/2 px-2">
                                                            <label for="tb_age" class="block text-black font-medium mb-2">Age</label>
                                                            <div class="block w-full border rounded py-2 px-3">{{ calculatedAge }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="flex flex-wrap -mx-2 mb-4">
                                                        <div class="w-full md:w-1/2 px-2 mb-4 md:mb-0">
                                                            <label for="tb_address" class="block text-black font-medium mb-2">Address</label>
                                                            <input type="text" name="tb_address" id="tb_address" placeholder="e.g. 39 Plaza Dr, Rockwell Center, Makati, 1200 Metro Manila" class="w-full border border-gray-400 rounded py-2 px-4 mb-3 leading-tight focus:ring-1 focus:ring-green-800 focus:bg-white focus:border-green-400" value="<?= $u_address ?>">
                                                            <p class="text-gray-600 text-xs italic mt-1">Please provide your complete address, including city and zip code.</p>
                                                        </div>

                                                        <div class="w-full md:w-1/2 px-2">
                                                            <label for="tb_gender" class="block text-black font-medium mb-2 focus:ring-1 focus:ring-green-800 focus:bg-white focus:border-green-400">Gender</label>
                                                            <select name="tb_gender" id="tb_gender" class="block w-full border rounded py-2 px-3">
                                                                <option <?php if ($u_gender === 'PREFER_NOT_TO_SAY') : ?> selected <?php endif ?>>
                                                                    Prefer not to say
                                                                </option>
                                                                <option <?php if ($u_gender === 'MALE') : ?> selected <?php endif ?>>
                                                                    Male
                                                                </option>
                                                                <option <?php if ($u_gender === 'FEMALE') : ?> selected <?php endif ?>>
                                                                    Female
                                                                </option>
                                                            </select>
                                                        </div>

                                                        <div class="w-full md:w-1/2 px-2 mb-6 mt-2 md:mb-0">
                                                            <label for="tb_cnum" class="block text-black font-medium mb-2">Contact Number</label>
                                                            <input type="number" name="tb_cnum" id="tb_cnum" placeholder="e.g. 09XXX-XXX-YYY" value="<?= substr($u_cnum, 1) ?>" maxlength="10" oninput="this.value=this.value.slice(0,10)" class="w-full border border-gray-400 rounded py-2 px-4 mb-3 leading-tight focus:ring-1 focus:ring-green-800 focus:bg-white focus:border-green-400">
                                                        </div>

                                                        <div class="w-full md:w-1/2 px-2 mt-2 ">
                                                            <label for="tb_email" class="block text-black font-medium mb-2">Email</label>
                                                            <input type="email" name="tb_email" id="tb_email" placeholder="e.g. johndoe@gmail.com" value="<?= $u_email ?>" class="w-full border border-gray-400 rounded py-2 px-4 mb-3 leading-tight focus:ring-1 focus:ring-green-800 focus:bg-white focus:border-green-400">
                                                        </div>
                                                    </div>


                                                    <button class="button btn_submit btn bg-green-700 text-white py-2 px-4 rounded-lg hover:bg-green-600 transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-10">
                                                        UPDATE
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } elseif ($page == "password") { ?>
                                <div class="flex gap-4 flex-wrap justify-center mt-5">
                                    <div class="flex flex-col w-full mb-4 sm:w-[20rem]">
                                        <div class="container flex flex-col items-center max-w-3xl mx-auto">
                                            <div class="container_title w-full text-center">
                                                Avatar
                                            </div>
                                            <div class="container_body w-full">
                                                <img src="../assets/images/<?= $u_avatar ?>" class="object-contain w-full company_logo_preview profile_picture">
                                                <input type="file" name="input_upload_field" id="input_upload_field" class="input_upload_field" data-set="avatar" accept="image/*">
                                                <br>
                                                <label for="input_upload_field" class="btn_upload_picture w-full">CHANGE AVATAR</label>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="flex flex-col mt-8 z-10 h-[100%] w-[100%] gap-3 sm:w-[100%]" id="sidebar">
                                                <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "general_information") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="?page=general_information">
                                                    <i class="fa fa-user"></i>
                                                    <p class="name" id="line_name">Account Information</p>
                                                </a>
                                                <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "password") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="?page=password">
                                                        <i class="fa fa-key"></i>
                                                        <p class="name" id="line_name">Password</p>
                                                    </a>
                                                <a class="flex gap-2 items-center px-4 py-3 rounded-md  hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700<?= (value("page") == "resume") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="?page=resume">
                                                    <i class="fa fa-file-text-o"></i>
                                                    <p class="name" id="line_name">Resume</p>
                                                </a>
                                                <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "logout") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="/<?= $__name__ ?>/logout.php">
                                                    <i class="fa fa-sign-out"></i>
                                                    <span class="name">Log Out</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container p-6 max-w-3xl ">
                                        <h2 class="text-lg font-medium text-green-600 mb-1">Change Password</h2>
                                        <hr class="my-2 border-gray-400 mb-4">
                                        <form method="post" class="frm_<?= $page ?>_account">
                                            <div class="flex flex-col mb-4">
                                                <label for="tb_pw" class="text-gray-700 font-medium mb-2">Old password</label>
                                                <p class="text-gray-600 text-xs italic mt-1">Please enter your current password to proceed with the password change.</p>
                                                <input type="password" name="tb_pw" id="tb_pw" class="border border-gray-400 p-2 rounded-lg focus:outline-none focus:shadow-outline hover:border-green-800 transition-all duration-200 focus:border-green-800 focus:shadow-outline focus:bg-white focus:shadow-none focus:mb-0" placeholder="Enter your current password">
                                            </div>
                                            <div class="flex flex-col mb-4">
                                                <label for="tb_newpw" class="text-gray-700 font-medium mb-2">New password</label>
                                                <p class="text-gray-600 text-xs italic mt-1">Enter your new password below. Make sure it's at least 8 characters long.</p>

                                                <input type="password" name="tb_newpw" id="tb_newpw" class="border border-gray-400 p-2 rounded-lg focus:outline-none focus:shadow-outline hover:border-green-800 transition-all duration-200 focus:border-green-800 focus:shadow-outline focus:bg-white focus:shadow-none focus:mb-0" placeholder="Enter your new password">
                                            </div>
                                            <div class="flex flex-col mb-4">
                                                <label for="tb_cnewpw" class="text-gray-700 font-medium mb-2">Confirm new password</label>
                                                <p class="text-gray-600 text-xs italic mt-1">Confirm your new password by retyping it below. Make sure it matches the new password you entered above.</p>
                                                <input type="password" name="tb_cnewpw" id="tb_cnewpw" class="border border-gray-400 p-2 rounded-lg focus:outline-none focus:shadow-outline hover:border-green-500 transition-all duration-200 focus:border-green-800 focus:shadow-outline focus:bg-white focus:shadow-none focus:mb-0" placeholder="Retype new password">
                                            </div>

                                            <button class="btn bg-green-700 text-white py-2 px-4 rounded-lg hover:bg-green-600 transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-10">
                                                UPDATE
                                            </button>
                                        </form>
                                    </div>
                                </div>

                            <?php } elseif ($page == "resume") { ?>
                                <div class="flex flex-wrap gap-4 justify-center mt-5">
                                    <div class="flex flex-col w-full mb-4 sm:w-[20rem]">
                                        <div class="container flex flex-col items-center max-w-3xl mx-auto">
                                            <div class="container_title w-full text-center">
                                                Avatar
                                            </div>
                                            <div class="container_body w-full">
                                                <img src="../assets/images/<?= $u_avatar ?>" class="object-contain w-full company_logo_preview profile_picture">
                                                <input type="file" name="input_upload_field" id="input_upload_field" class="input_upload_field" data-set="avatar" accept="image/*">
                                                <br>
                                                <label for="input_upload_field" class="btn_upload_picture w-full">CHANGE AVATAR</label>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="flex flex-col mt-8 z-10 h-[100%] w-[100%] gap-3 sm:w-[100%]" id="sidebar">
                                                <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "general_information") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="?page=general_information">
                                                    <i class="fa fa-user"></i>
                                                    <p class="name" id="line_name">Account Information</p>
                                                </a>
                                                <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "password") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="?page=password">
                                                    <i class="fa fa-key"></i>
                                                    <p class="name" id="line_name">Password</p>
                                                </a>
                                                <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "resume") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="?page=resume">
                                                    <i class="fa fa-file-text-o"></i>
                                                    <p class="name" id="line_name">Resume</p>
                                                </a>
                                                <a class="flex gap-2 items-center px-4 py-3 rounded-md hover:text-white hover:bg-green-700 transistion text-green-700 border border-green-700 <?= (value("page") == "logout") ? 'bg-green-700 text-white' : 'text-green-700 border border-green-700' ?>" href="/<?= $__name__ ?>/logout.php">
                                                    <i class="fa fa-sign-out"></i>
                                                    <span class="name">Log Out</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container max-w-3xl h-auto sm:h-[53%] " >
                                        <div class="container_title text-lg font-medium text-green-600 mb-3">
                                            Resume
                                        </div>
                                        <div class="container_body">
                                            <?php if ($have_resume) { ?>
                                                <form method="post" class="frm_<?= $page ?>_account">
                                                    <div class="field">
                                                        <label for="">Resume <br>
                                                            <p class="text-red-600 text-xs italic mt-1">*Must PDF File</p>
                                                        </label>
                                                        <input type="file" class="fileToUpload" id="file_button">
                                                        <label for="file_button" class="btn_upload_picture ">
                                                            <i class="fa fa-file-pdf-o"></i>
                                                            UPLOAD RESUME
                                                        </label>
                                                    </div>

                                                    <div class="button_container flex flex-col sm:flex-row sm:justify-between items-center gap-4 mt-5 sm:w-auto w-full">
                                                        <button class="button btn_submit bg-green-700 hover:bg-green-600 text-white font-medium py-3 px-6 rounded-lg transition duration-300 ease-in-out transform hover:scale-15 hover:-translate-y-1 ">
                                                            <i class="fa fa-save mr-2"></i> Save
                                                        </button>
                                                        <a href="../resume/<?= $r_path ?>" class="button btn_submit bg-blue-500 hover:bg-blue-600 text-white font-medium py-3 px-6 rounded-lg transition duration-300 ease-in-out transform hover:scale-15 hover:-translate-y-1  w-full sm:w-auto" download>
                                                            <i class="fa fa-download mr-2"></i> Download Resume
                                                        </a>

                                                    </div>




                                                </form>
                                        </div>
                                    <?php } else { ?>
                                        <?php if ($u_verification_state == 0 || $u_verification_state < 2) { ?>
                                            <div class="msg">
                                                <p>
                                                    Uploading your resume will be used to verify/confirm your account.
                                                </p>
                                            </div>
                                        <?php } ?>
                                        <form method="post" class="frm_<?= $page ?>_account">
                                            <div class="field">
                                            <label for="">Resume <br>
                                                    <p class="text-red-600 text-xs italic mt-1">*Must PDF File</p>
                                                </label>
                                                <input type="file" id="file_button" class="fileToUpload">
                                                <label for="file_button" class="btn_upload_picture">
                                                    <i class="fa fa-file-pdf-o"></i>
                                                    UPLOAD RESUME
                                                </label>
                                            </div>
                                            <button class="button btn_submit bg-green-600 hover:bg-green-500 text-white font-medium py-3 px-6 rounded-lg transition duration-300 ease-in-out transform hover:scale-15 hover:-translate-y-1 ">
                                            <i class="fa fa-save mr-2"></i>  SAVE
                                            </button>
                                        </form>
                                    <?php } ?>
                                    </div>
                                </div>
                            <?php } else {
                            navigate("./?page=general_information");
                        } ?>
                                    </div>
                                </div>
                </div>
            </div>

            <script>
                document.addEventListener("DOMContentLoaded", function() {
                    const sidebarHeader = document.querySelector(".sidebar-header");
                    sidebarHeader.addEventListener("click", function() {
                        const sidebar = document.querySelector(".sidebar-container");
                        sidebar.classList.toggle("expanded");
                    });
                });

                const app = Vue.createApp({
                    data() {
                        return {
                            birthDate: "<?= $u_bday ?>",
                        }
                    },
                    computed: {
                        calculatedAge() {
                            if (!this.birthDate) return "N/A"

                            const duration = luxon.DateTime.fromFormat(
                                this.birthDate,
                                "yyyy-MM-dd", {
                                    zone: "Asia/Manila",
                                }
                            ).diffNow("years").years

                            const dropSign = Math.abs(duration)
                            const dropDecimals = Math.trunc(dropSign)
                            return dropDecimals
                        }
                    }
                })
                app.mount("#app")
            </script>
</body>

</html>