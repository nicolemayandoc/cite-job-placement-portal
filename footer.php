<footer class="bg-green-800 text-white">
    <div class="max-w-6xl mx-auto px-3 py-12">
        <div class="grid sm:grid-cols-2 mb-8">
            <section class="flex sm:justify-center">
                <a href="/<?= $__name__ ?>" class="flex items-center text-xl font-medium">
                    <div class="w-24 h-24 bg-white rounded-full border-8 border-green-800">
                        <img
                            src="/<?= $__name__ ?>/assets/logo.png"
                            alt="logo"
                            class=""
                        />
                    </div>
                    <p>CITE Job Portal</p>
                </a>
            </section>
            <section>
                <h3 class="font-semibold mb-1">Contact Us</h3>
                <p>Telephone: (075) 522-5635</p>
                <p>Fax: (075) 522-2496</p>
                <p>Mobile: +63 995-078-5660</p>
                <p>Email: info.up@phinmaed.com</p>
                <p>Arellano Street, Dagupan City, 2400, Pangasinan</p>
            </section>
        </div>
        <section class="text-center">
            <h3 class="font-semibold mb-1">Navigation</h3>
            <ul class="flex justify-center gap-6 sm:gap-12">
                <li>
                    <a href="/<?= $__name__ ?>">HOME</a>
                </li>
                <li>
                    <a href="/<?= $__name__ ?>/jobs">JOBS</a>
                </li>
                <li>
                    <a href="/<?= $__name__ ?>/contact-us">CONTACT US</a>
                </li>
                <li>
                    <a href="/<?= $__name__ ?>/auth?a=already">
                        LOGIN
                    </a>
                </li>
            </ul>
        </section>
    </div>
</footer>