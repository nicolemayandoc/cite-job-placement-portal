<?php
session_start();

require_once '../../../config.php';
require_once '../../../functions.php';

header("Content-Type: application/json");

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $email = mysqli_value($con,"email");
    $password = mysqli_value($con,"password");
    $browserName = mysqli_value($con,"browser_name");

    function message($status,$message, $id, $type){
    	$result["message"] = $message;
    	$result["status"] = $status;
        $result["id"] = $id;
        $result["type"] = $type;

        //    echo arraytojson($msg);
    	echo json_encode($result);
        die();
    }

    if($email == ""){
        message(false,"Please enter your email.");
    }
    if($password == ""){
        message(false,"Please enter your password.");
    }

 //check if email is already registered
    $check_email_result = mysqli_query($con, "SELECT id, password FROM `tbl_accounts` WHERE `email` = '$email' ");
    if(hasResult($check_email_result)){
        $raw_password = $check_email_result->fetch_assoc()["password"];
        if (password_verify($password, $raw_password)) {
            // On successful login, record login.
            $userData = mysqli_query($con, "SELECT * FROM `tbl_accounts` WHERE `email` = '$email' ");
            $fetchedUser = mysqli_fetch_assoc($userData);
            $stmt = $con->prepare("INSERT INTO tbl_login_history (account_id, browser_name) VALUES (?, ?)");
            $stmt->bind_param("is", $fetchedUser["id"], $browserName);
            $stmt->execute();

            // Save user data to the session.
            $_SESSION["islogin"] = true;
            $_SESSION["data"] = $fetchedUser;
            $u_id = $fetchedUser["id"];
            $type = $fetchedUser["type"];
            // Send response to the browser.
            message(true,"Successful login.", $u_id, $type);
        }
        message(false,"Email and password is not valid, Please try again.", 0, 0);
    }else{
        message(false,"Email and password is not valid, Please try again.", 0, 0);
    }
}
?>

