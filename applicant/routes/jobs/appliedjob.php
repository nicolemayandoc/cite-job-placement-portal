<?php
require_once '../../../config.php';
header("Content-Type: application/json");

$userId = $_GET["id"];
$jobs = array();


$result_query = "SELECT
tbl_jobs.id,
tbl_company.c_name,
tbl_company.id as 'c_id',
tbl_company.c_address,
tbl_company.c_description,
tbl_company.c_cnum,
tbl_company.c_logo,
tbl_accounts.firstname,
tbl_accounts.lastname,
tbl_jobs.j_name,
tbl_jobs.j_age,
tbl_jobs.j_min,
tbl_jobs.j_max,
tbl_jobs.j_currency_symbol,
tbl_jobs.j_description,
tbl_jobs.j_highlights,
tbl_jobs.j_created_at
FROM tbl_applicants
INNER JOIN tbl_jobs
ON tbl_applicants.jobid = tbl_jobs.id
INNER JOIN tbl_company
ON tbl_company.userid = tbl_jobs.userid
INNER JOIN tbl_accounts
ON tbl_accounts.id = tbl_jobs.userid
WHERE tbl_applicants.applicantsid = ?";
$stmt = $con->prepare($result_query);
$stmt->bind_param("i", $userId);
$stmt->execute();
$result = $stmt->get_result();


while($row = mysqli_fetch_assoc($result)) {
$temp = array();
$temp["j_id"] = $row["id"];
$temp["j_name"] = $row["j_name"];
$temp["c_name"] = $row["c_name"];
$temp["c_id"] = $row["c_id"];
$temp["c_description"] = $row["c_description"];
$temp["c_logo"] = $row["c_logo"];
$temp["c_address"] = $row["c_address"];
$temp["j_min"] = $row["j_min"];
$temp["j_max"] = $row["j_max"];
$temp["j_description"] = $row["j_description"];
$temp["j_highlights"] = $row["j_highlights"];
$temp["j_currency_symbol"] = $row["j_currency_symbol"];
$temp["j_created_at"] = $row["j_created_at"];

array_push($jobs, $temp);  
}

echo json_encode($jobs);




//  $userId = $_GET["id"];
//  $query = "SELECT * FROM tbl_applicants
//  JOIN tbl_jobs
//  ON tbl_applicants.jobid = tbl_jobs.id
//  JOIN tbl_company
//  ON tbl_applicants.companyid = tbl_company.id
//  WHERE tbl_applicants.applicantsid = ?";
//  $stmt = $con->prepare($query);
//  $stmt->bind_param("i", $userId);
//  $stmt->execute();

//  $result = $stmt->get_result();
//  $jobs = array();

//  while ($row = $result->fetch_assoc())
//    array_push($jobs, $row);


//  echo json_encode([
//    //"message" => "Retrieved jobs applied by user.",
//    "jobs" => $jobs
//  ]);
?>