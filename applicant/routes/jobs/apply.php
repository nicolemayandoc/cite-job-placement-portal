<?php
session_start();
require_once '../../../config.php';
require_once '../../../functions.php';
require_once '../../../session.php';

$report = false;
$apply = false;

// if(form("id")){
//     if(!$islogin){
//         navigate("../auth/?a=already");
//     }

function message($status,$message){
    $result["message"] = $message;
    $result["status"] = $status;
    echo json_encode($result);
    //die();
}



if($_SERVER['REQUEST_METHOD'] === 'POST'){

    //$id = mysqli_value($con,"id");
    $mdata = $_POST;
    $id = $mdata["job_id"];
    $u_id = $mdata["u_id"];


    $load_data = mysqli_query($con,"SELECT * FROM `tbl_accounts` WHERE id = $u_id"); 

    $result_query = mysqli_query($con,"
    SELECT
        tbl_jobs.id,
        tbl_company.id as 'c_id',
        tbl_company.c_name,
        tbl_company.c_address,
        tbl_company.c_cnum,
        tbl_accounts.firstname,
        tbl_accounts.lastname,
        tbl_jobs.j_name,
        tbl_jobs.j_age,
        tbl_jobs.j_min,
        tbl_jobs.j_max,
        tbl_jobs.j_currency_symbol,
        tbl_jobs.j_description,
        tbl_jobs.j_created_at
    FROM
        tbl_jobs
    INNER JOIN tbl_company ON tbl_company.userid = tbl_jobs.userid
    INNER JOIN tbl_accounts ON tbl_accounts.id = tbl_jobs.userid

    WHERE
    tbl_jobs.id = $id
    ");

    if(hasResult($result_query)){
        $data = mysqli_fetch_assoc($result_query);
    }else{
        //navigate("./");
        message(false, "No table job query found");
    }

    if(hasResult($load_data)){
       $vdata = mysqli_fetch_assoc($load_data);
       $u_verification_state = $vdata["verification_state"];
    } else {
         message(false, "No table applicant query found");
    }

//    if(form("apply")){
        //check if the user is fully verified.
        $apply = true;
        if($u_verification_state == 2){
            $fully_verified = true;

            $job_id = $data["id"];
            $company_id = $data["c_id"];
            
            $check_application = mysqli_query($con,"SELECT * FROM `tbl_applicants` WHERE `companyid` = $company_id AND `applicantsid` = $u_id AND `jobid` = $job_id ");
            if(hasResult($check_application)){
                message(true, "We already submit your application for this position, The company will notify you.");
                $already_submited = true;
            }else{
                $submit_application = mysqli_query($con,"
                INSERT INTO `tbl_applicants`(
                    `companyid`,
                    `applicantsid`,
                    `jobid`
                )
                VALUES(
                    $company_id,
                    $u_id,
                    $job_id
                )
                ");
                $already_submited = false;
                message(true, "Your application was successfully submited, The company will notify you.");
            }
        }else{
            $fully_verified = false;
            message(false, "Not yet fully verified");
        }
    // }else{
    //     $apply = false;
    // }

// }elseif(form("report") && is_numeric(value("report"))){
//     $report = true;
//     $reported_company_id = value("report");
// }
// else{
//     navigate("./");
 }
?>