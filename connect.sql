-- MariaDB dump 10.19  Distrib 10.11.2-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: connect
-- ------------------------------------------------------
-- Server version	10.11.2-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_accounts`
--

DROP TABLE IF EXISTS `tbl_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_accounts` (
  `id` int(225) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `cnum` varchar(20) NOT NULL,
  `bday` date NOT NULL,
  `age` int(2) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `type` int(1) NOT NULL COMMENT 'admin=1 company=2 client=3',
  `verification_state` int(1) NOT NULL DEFAULT 0 COMMENT '0 = not verified\r\n1 = semi verified\r\n2 = verified',
  `avatar` varchar(50) NOT NULL DEFAULT 'avatar_default.png',
  `department` varchar(50) NOT NULL DEFAULT 'none',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `gender` enum('PREFER_NOT_TO_SAY','MALE','FEMALE') NOT NULL DEFAULT 'PREFER_NOT_TO_SAY',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_accounts`
--

LOCK TABLES `tbl_accounts` WRITE;
/*!40000 ALTER TABLE `tbl_accounts` DISABLE KEYS */;
INSERT INTO `tbl_accounts` VALUES
(6,'admin','admin','09093939708','1990-02-01',-1,'739 G. Araneta Avenue 1100','admin@connect.com','$2y$10$kwa2XKdoX5EduBf2EOIOvOMDuyHQBKs8eiwag2LUmzQSB7EZ5SWcC',1,0,'avatar_default.png','none','2022-08-03 18:15:03','PREFER_NOT_TO_SAY'),
(7,'company','company','09093939705','2022-09-10',25,'Rm. 202 Grace Building Ortigas Avenue Greenhills 1500','company@connect.com','$2y$10$19pQGZ/LC7TB1C8o3csNEOzN5z7Ux0mIhCEw0m.pIxrS3HQeZhqXC',2,0,'avatar_default.png','none','2022-08-03 18:33:47','PREFER_NOT_TO_SAY'),
(8,'client','client','09486502742','1990-01-01',-1,'P. Burgos Street corner Caseres Street','client@connect.com','$2y$10$cJLZ8ZbHrGW4B6kl2d76Oucn9zom3j5XWRC682ZjRt0FF4R0ivhNq',3,2,'avatar_default.png','I.T','2022-08-03 18:34:28','PREFER_NOT_TO_SAY'),
(9,'client1','client1','09093939705','2002-02-22',25,'#26 mBaguio First Hotel, Bonifacio Street','client1@connect.com','$2y$10$v4v0aqfYsE2/QMa6PSWXXuF/5it5U4IhZLBEEAmvhGkgxYwddid.i',3,0,'avatar_default.png','I.T','2022-08-03 18:34:28','PREFER_NOT_TO_SAY');
/*!40000 ALTER TABLE `tbl_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_applicants`
--

DROP TABLE IF EXISTS `tbl_applicants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_applicants` (
  `id` int(225) NOT NULL AUTO_INCREMENT,
  `companyid` int(225) NOT NULL,
  `applicantsid` int(225) NOT NULL,
  `jobid` int(225) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '1=pending\r\n2=hired\r\n3=decline',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_applicants`
--

LOCK TABLES `tbl_applicants` WRITE;
/*!40000 ALTER TABLE `tbl_applicants` DISABLE KEYS */;
INSERT INTO `tbl_applicants` VALUES
(1,4,8,3,1,'2022-08-12 14:00:44'),
(2,4,8,3,1,'2022-08-12 14:00:44'),
(3,5,8,3,2,'2022-08-12 14:00:44'),
(4,5,8,3,2,'2022-08-12 14:00:44'),
(5,4,8,3,1,'2022-09-02 14:31:42'),
(7,5,13,1,2,'2022-10-16 15:23:09'),
(8,5,8,1,1,'2022-11-25 22:57:38');
/*!40000 ALTER TABLE `tbl_applicants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_company`
--

DROP TABLE IF EXISTS `tbl_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_company` (
  `id` int(225) NOT NULL AUTO_INCREMENT,
  `userid` int(225) NOT NULL,
  `c_logo` varchar(70) NOT NULL DEFAULT 'company_logo_default.png',
  `c_banner` varchar(70) NOT NULL DEFAULT 'company_banner_default.png',
  `c_name` varchar(50) NOT NULL,
  `c_address` varchar(100) NOT NULL,
  `c_cnum` varchar(20) NOT NULL,
  `c_position` varchar(50) NOT NULL,
  `department` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `c_description` varchar(5000) DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `c_name` (`c_name`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_company`
--

LOCK TABLES `tbl_company` WRITE;
/*!40000 ALTER TABLE `tbl_company` DISABLE KEYS */;
INSERT INTO `tbl_company` VALUES
(4,6,'company_logo_default.png','company_banner_default.png','D Devs','Manila, Metro Manila','09093939708','I.T','I.T','2022-08-03 18:15:03',''),
(5,7,'company_logo_default.png','company_banner_default.png','Fux Devs','Manila, Metro Manila','09093939708','I.T','I.T','2022-08-03 18:33:47',''),
(6,8,'company_logo_default.png','company_banner_default.png','Metro Ui','Manila, Metro Manila','09093939708','Advertiser','I.T','2022-08-03 18:34:28','');
/*!40000 ALTER TABLE `tbl_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_company_reports`
--

DROP TABLE IF EXISTS `tbl_company_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_company_reports` (
  `id` int(225) NOT NULL AUTO_INCREMENT,
  `company_id` int(225) NOT NULL,
  `reported_by` int(225) NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_company_reports`
--

LOCK TABLES `tbl_company_reports` WRITE;
/*!40000 ALTER TABLE `tbl_company_reports` DISABLE KEYS */;
INSERT INTO `tbl_company_reports` VALUES
(1,4,8,'Miss leading information','2022-08-27 02:35:28'),
(2,4,8,'Miss leading information','2022-08-27 02:35:28'),
(3,4,8,'asdasdsa','2022-09-02 07:43:15');
/*!40000 ALTER TABLE `tbl_company_reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_jobs`
--

DROP TABLE IF EXISTS `tbl_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_jobs` (
  `id` int(225) NOT NULL AUTO_INCREMENT,
  `userid` int(225) NOT NULL,
  `j_name` varchar(50) NOT NULL,
  `j_age` int(2) NOT NULL,
  `j_min` decimal(50,0) NOT NULL,
  `j_max` decimal(50,0) NOT NULL,
  `j_currency_symbol` varchar(1) NOT NULL,
  `j_description` text NOT NULL,
  `j_gender` varchar(6) NOT NULL DEFAULT 'Both',
  `j_created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `j_age_max` int(11) DEFAULT NULL,
  `j_highlights` varchar(5000) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_jobs`
--

LOCK TABLES `tbl_jobs` WRITE;
/*!40000 ALTER TABLE `tbl_jobs` DISABLE KEYS */;
INSERT INTO `tbl_jobs` VALUES
(1,7,'I.T',18,15000,200000,'₱','Solary','Female','2022-08-05 00:46:17',60,''),
(2,7,'Social Media Avertiser',18,10000,200000,'₱','Salary','Both','2022-08-05 00:46:17',64,''),
(3,6,'Back End Developer',18,15000,20000,'₱','<p><strong>Bruh</strong></p>\r\n','Both','2022-08-12 05:22:23',64,''),
(4,7,'PHP Dev',21,15000,30000,'₱','<p>Hi</p>\r\n','Both','2022-09-14 18:23:22',64,'');
/*!40000 ALTER TABLE `tbl_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_login_history`
--

DROP TABLE IF EXISTS `tbl_login_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_login_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `login_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `browser_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  CONSTRAINT `tbl_login_history_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `tbl_accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_login_history`
--

LOCK TABLES `tbl_login_history` WRITE;
/*!40000 ALTER TABLE `tbl_login_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_login_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_resume`
--

DROP TABLE IF EXISTS `tbl_resume`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_resume` (
  `id` int(225) NOT NULL AUTO_INCREMENT,
  `userid` int(225) NOT NULL,
  `path` varchar(225) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_resume`
--

LOCK TABLES `tbl_resume` WRITE;
/*!40000 ALTER TABLE `tbl_resume` DISABLE KEYS */;
INSERT INTO `tbl_resume` VALUES
(1,6,'example1.png','2022-08-12 06:35:51'),
(2,7,'example2.jpg','2022-08-12 06:35:51'),
(3,8,'c9f0f895fb98ab9159f51fd0297e236d.png','2022-09-02 05:34:59'),
(4,13,'c51ce410c124a10e0db5e4b97fc2af39.pdf','2022-10-16 20:59:52');
/*!40000 ALTER TABLE `tbl_resume` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sms_logs`
--

DROP TABLE IF EXISTS `tbl_sms_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sms_logs` (
  `id` int(225) NOT NULL AUTO_INCREMENT,
  `receiverid` int(225) NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sms_logs`
--

LOCK TABLES `tbl_sms_logs` WRITE;
/*!40000 ALTER TABLE `tbl_sms_logs` DISABLE KEYS */;
INSERT INTO `tbl_sms_logs` VALUES
(4,8,'Connect \nVerification code: 484709','2022-08-03 18:15:03'),
(5,13,'Connect \nVerification code: 660188','2022-08-03 18:15:03'),
(6,7,'CONNECT \n Hello client, Fux Devs has new open job Barber.','2022-11-25 14:06:13'),
(7,7,'CONNECT \n Hello saddd, Fux Devs has new open job Barber.','2022-11-25 14:06:14'),
(8,6,'CONNECT \n Hello client,  has new open job Hana Barbero.','2022-11-25 15:05:58'),
(9,6,'CONNECT \n Hello saddd,  has new open job Hana Barbero.','2022-11-25 15:05:59'),
(10,6,'CONNECT \n Hello client,  has new open job Hana Barbero.','2022-11-25 15:06:00'),
(11,6,'CONNECT \n Hello saddd,  has new open job Hana Barbero.','2022-11-25 15:06:01');
/*!40000 ALTER TABLE `tbl_sms_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_verificationcode`
--

DROP TABLE IF EXISTS `tbl_verificationcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_verificationcode` (
  `id` int(225) NOT NULL AUTO_INCREMENT,
  `session` varchar(50) NOT NULL,
  `code` varchar(10) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0 COMMENT '0=not used\r\n1=used',
  `used_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_verificationcode`
--

LOCK TABLES `tbl_verificationcode` WRITE;
/*!40000 ALTER TABLE `tbl_verificationcode` DISABLE KEYS */;
INSERT INTO `tbl_verificationcode` VALUES
(17,'dd3d05d7de99af7a376db3aaf6a18dba','484709',1,'2022-09-02 05:34:41','2022-09-02 05:33:54'),
(18,'2db33b9255ddef48687f97f5503d5f50','660188',1,'2022-10-16 20:59:07','2022-10-16 20:58:32');
/*!40000 ALTER TABLE `tbl_verificationcode` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-03-09  1:49:37
